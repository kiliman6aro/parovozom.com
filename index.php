<?php
define('DS', DIRECTORY_SEPARATOR);
// change the following paths if necessary
$yii=dirname(__FILE__).'/../framework/yii.php';
$config=dirname(__FILE__).'/app/config/main.php';
$appClass = dirname(__FILE__) . '/app/components/WebApplication.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
require_once($appClass);

Yii::createApplication('WebApplication', $config)->run();
