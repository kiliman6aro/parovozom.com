﻿<!DOCTYPE html>
<html>
	<head>
		<title>Bad Browsers</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/ui_style/jquery-ui-1.10.3.custom.css"/>
		<link rel="stylesheet" href="css/styles.css"/>
		<link rel="stylesheet" href="css/select2.css"/>
		<link rel="stylesheet" href="css/jslider.css" type="text/css">
		<link rel="stylesheet" href="css/info_slider/animate.css">
		<link rel="stylesheet" href="css/info_slider/liquid-slider.css">
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie.css" type="text/css"/><![endif]-->
		<link rel="stylesheet" href="css/main.css"/>
		<script src="js/jquery-1.10.2.min.js"></script>
		<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="js/select2.js"></script>
		<script src="js/site.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/jquery.touchSwipe.min.js"></script>
		<script src="js/jquery.liquid-slider.min.js"></script>
		<script type="text/javascript" src="js/jshashtable-2.1_src.js"></script>
		<script type="text/javascript" src="js/jquery.numberformatter-1.2.3.js"></script>
		<script type="text/javascript" src="js/tmpl.js"></script>
		<script type="text/javascript" src="js/jquery.dependClass-0.1.js"></script>
		<script type="text/javascript" src="js/draggable-0.1.js"></script>
		<script type="text/javascript" src="js/jquery.slider.js"></script>
		<script>
			$(document).ready(function () {
				//select2 init
				$("#price_sort, #pp_sort").select2();
				$("#departure_time").select2({
					dropdownCssClass: "departure_time"
				});
				$('#main-slider').liquidSlider();
			});
		</script>
	</head>
	<body>
		<div class="bg_wrapper bad_page">
			<div class="header">
				<div class="container">
					<a href="index.html" class="logo"></a>
					<div class="slogan">
						Онлайн-продажа
						<b>Ж/Д билетов</b>
					</div>
					<div class="contact_us opener">
						Связаться с нами
					</div>
					
				</div>
			</div>
			<div class="content">
				<div class="container">
					<div class="info">
						<p>Ваш браузер устарел и не подходит для пользования нашим сервисом.</p>
						<p>Обновите браузер или воспользуйтесь одним из последних версий:</p>
					</div>
					<div class="browsewrs_list">
						<ul>
							<li>
								<p><a href=""><img src="images/browsers/img1.png" width="110" height="115" alt=""/></a></p>
								<p><a href="">Google Chrome</a></p>
							</li>
							<li>
								<p><a href=""><img src="images/browsers/img2.png" width="110" height="115" alt=""/></a></p>
								<p><a href="">Mozilla Firefox</a></p>
							</li>
							<li>
								<p><a href=""><img src="images/browsers/img3.png" width="110" height="115" alt=""/></a></p>
								<p><a href="">Opera</a></p>
							</li>
							<li>
								<p><a href=""><img src="images/browsers/img4.png" width="110" height="115" alt=""/></a></p>
								<p><a href="">Safari</a></p>
							</li>
							<li>
								<p><a href=""><img src="images/browsers/img5.png" width="110" height="115" alt=""/></a></p>
								<p><a href="">Internet Explorer</a></p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
			<div class="container">
				<span class="copyright">
					©2013.
					<b>Паровозом.</b>
				</span>
				<ul class="social_box">
					<li>
						<a target="_blank" href="" class="item tw"></a>
					</li>
					<li>
						<a target="_blank" href="" class="item in"></a>
					</li>
					<li>
						<a target="_blank" href="" class="item vk"></a>
					</li>
					<li>
						<a target="_blank" href="" class="item fcb"></a>
					</li>
				</ul>
				<ul class="bottom_nav">
					<li>
						<a href="">Условия</a>
					</li>
					<li>
						<a href="faq.html">Вопрос-ответ</a>
					</li>
					<li>
						<a href="contacts.html">Контакты</a>
					</li>
					<li class="contact_us opener">
						<a href="javascript:void(0)">
							<b>Связаться</b>
							с нами
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="dialog" class="support_pp">
			<h5>Связаться с нами</h5>
			<form action="">
				<div class="field">
					<select id="pp_sort" class="price_sort">
						<option value="">Выберитие тему вопроса</option>
						<option value="">Выберитие тему вопроса1</option>
						<option value="">Выберитие тему вопроса2</option>
						<option value="">Выберитие тему вопроса3</option>
					</select>
				</div>
				<div class="field">
					<textarea placeholder="Ваше сообщение" class="area"></textarea>
				</div>
				<div class="field column_layout contact_field">
					<div class="column1">
						<input class="inp" type="text" placeholder="Email"/>
					</div>
					<div class="column2">
						<input class="inp" type="text" placeholder="Имя"/>
					</div>
				</div>
				<div class="field btn_block">
					<a href="" class="btn send_btn">Отправить</a>
				</div>
			</form>
		</div>
	</body>
</html>