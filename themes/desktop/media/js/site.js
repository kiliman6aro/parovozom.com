$(document).ready(function () {
    (function () {

        //Advanced Search
        var advancedSearch = $('.advanced_search');
        var additionalSearch = $('.additional_search');
        var switchLinksBlock = $('.switch_links_block');


        switchLinksBlock.click(function () {
            if (additionalSearch.is(':hidden')) {
                additionalSearch.slideDown().addClass('open');
                advancedSearch.addClass('open');
                $(this).find('span.link_open').show();
                $(this).find('span.link').hide();
            }
            else {
                additionalSearch.removeClass('open').slideUp();
                advancedSearch.removeClass('open');
                $(this).find('span.link_open').hide();
                $(this).find('span.link').show();
            }
        });
    })();

    $('#luggage').click(function () {
        $('.additional').toggle();
    });

    $('.info_title').tooltip({
        show: null,
        position: {
            my: "left-20 top+8",
            at: "left bottom+1"
        },
        open: function (event, ui) {
            ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast");
        }
    });

    //jquery ui
    //ui date picker

    /*var child_datepicker = function (input) {
        $('#ui-datepicker-div').addClass('child_datepicker');
        setTimeout(function () {
            $('#ui-datepicker-div').prepend($("<div class='title'>Выберете дату рождения ребенка (6-14 лет):</div>"));
        });
    };
    $("#birthday").datepicker({
        changeMonth: true,
        changeYear: true,
        beforeShow: child_datepicker
    });*/




    //buttonset
    $("#radio").buttonset();
    $(".radio_btn_block").buttonset();
    //$(".radio_btn_block_1").buttonset();
    //$(".radio_btn_block_2").buttonset();

    $("#dialog1").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 500
        },
        hide: {
            effect: "explode",
            duration: 500
        },
        width: 679,
        resizable: false,
        modal: true,
        draggable: false
    });
    $("#confirm").click(function () {
        $("#dialog1").dialog("open");
    });
});