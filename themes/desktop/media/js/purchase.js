var hasLocalStorage = ('localStorage' in window) && window['localStorage'] !== null;
var frmSearch,
        trains,
        trainPopup,
        selectedPlaces;

var ModulePurchase = function() {
    frmSearch = new TFrmSearch();
};

var Helper = {
    settings: GV.page.settings,
    bonusCard: GV.data.bonus_card,
    placesAllowed: 0,
    placesMax: 0,
    currTrain: null,
    currCoach: null,
    limitError: function(resp, callback) {
        if (!resp.data || (resp.data != -3))
            return false;

        var time = resp.value;
        var popup = Common.alert('error', _(_ntr(time, 'ips_limit_msg'), time), {
            minWidth: 600,
            onclose: function() {
                window.clearInterval(timer);
            }
        });
        var cont = popup.elements.content.elem;
        var timer = window.setInterval(function() {
            cont.innerHTML = _(_ntr(--time, 'ips_limit_msg'), time);
            if (time <= 0) {
                popup.close();
                if (callback)
                    callback();
            }
        }, 1000);

        return true;
    }
};

var TDateTime = {
    _days: ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'],
    // Добавить 0 перед числом (до 2х знаков)
    _padLeftZero: function(n) {
        n += '';
        return n.length == 1 ? '0' + n : n;
    },
    // Формат даты
    dFormat: function(src_date) {
        src_date = src_date.split(' ')[0].split('-');
        var day = this._padLeftZero(src_date[2]),
                month = this._padLeftZero(src_date[1]),
                year = src_date[0];
        var date = new Date(year, month - 1, day);
        if (GV.page.lc.isEng)
            return _(this._days[date.getDay()]) + ', ' + month + '.' + day + '.' + year;
        return _(this._days[date.getDay()]) + ', ' + day + '.' + month + '.' + year;
    },
    tFormat: function(src_date) {
        var src_time = src_date.split(' ')[1].split(':'),
                hour = src_time[0],
                min = src_time[1];
        if (GV.page.lc.isEng) {
            var ampm = hour >= 12 ? 'pm' : 'am';
            hour = hour % 12;
            hour = hour ? hour : 12; // the hour '0' should be '12'
            return this._padLeftZero(hour) + ':' + this._padLeftZero(min) + '<sup>' + ampm + '</sup>';
        } else {
            return this._padLeftZero(hour) + ':' + this._padLeftZero(min);
        }
    },
    timeDiff: function(timestampStart, timestampEnd) {
        var timeDiff = Math.floor((timestampEnd - timestampStart) / 60);
        return Math.floor(timeDiff / 60) + ':' + this._padLeftZero(timeDiff % 60);
    }
};

var TFrmSearch = function() {
    var _this = this;

    this._form = document.train_search_form;
    this._suggests = {};

    // auto-suggest на поле ввода станций
    this._initAutoSuggest = function(dir) {
        this._form['station_id_' + dir].value = '';
        var b = $v('station_' + dir).$$('b');

        this._suggests[dir] = vTools.autoSuggest(
                _this._form['station_' + dir],
                {
                    url: GV.site.ht_host + '/' + GV.page.module + '/station/%s/',
                    loading: function(flag) {
                        if (flag) {
                            b.opacityFade(100);
                        } else {
                            b.opacityFade(0, {step: 20});
                        }
                    }
                },
        {
            dl: $v('stations_' + dir),
            dlCorrect: {top: +3, width: +35},
            firstItem: function(val) {
                return {
                    title: val,
                    station_id: ''
                };
            },
            onselect: function(item) {
                _this._form['station_id_' + dir].value = item.station_id;
            },
            lang: GV.page.lang
        }
        );
    };

    // Календарь
    this._initCalendar = function() {
        var min = new Date(),
                minTime = min.getTime(),
                max = new Date();
        max.setTime(minTime + 89 * 24 * 3600000);
        if (vTools.browser.ie)
            max.setTime(minTime + 89 * 24 * 3599999);

        Common.datePicker(this._form.date_dep, {
            minDate: min,
            maxDate: max,
            format: GV.page.lc.dff,
            hideOnPick: true,
            shortMode: true,
            firstDay: GV.page.lc.fdow,
            numberOfMonths: GV.data.month_diff
        }).on('change', function() {
            _this._dateToBtn();
        }).setDate(this._form.date_dep.value);
        $v(window).addEvent('resize', function() {
            Calendar.hideAll();
            $(_this._form.date_dep).blur();
        });
    };

    this._initEvents = function() {
        $v(this._form.date_dep)
                .typingRule(/^[\d\.]*$/)
                .addChange(function() {
                    _this._dateToBtn();
                });

        // обратное направление (стрелки)
        $v('reverse_dir').addClick(function() {
            _this._changeDirection();
        });

        // чекбокс "туда-обратно"
        $v(this._form.round_trip).addClick(function() {
            if ($('cart_table')) {
                $v('search_back').show(false);
                Cart.empty();
            }
        });

        // найти обратно
        $v('search_back').addClick(function() {
            _this._changeDirection();
            $v('ts_res').style('display', 'none');
        });

        this._form.onsubmit = this._onSubmit;
    };

    // Запись даты в кнопку поиска
    this._dateToBtn = function() {
        $v(this._form.search).$$('span', true).html(this._form.date_dep.value);
    };

    // Проверка поля странций при submit-е формы
    this._onSubmitCheckField = function(dir, handler) {
        var inp = this._form['station_' + dir];
        if (!inp.value) {
            handler.addError('uz_ts_empty_station_' + dir, inp);
        } else if (!this._form['station_id_' + dir].value) {
            handler.addError('uz_ts_empty_station_menu_' + dir, inp);

            $v(inp).addEvent('focus', function searchInputFocusHandler() {
                _this._suggests[dir].refresh();
                handler.fieldSuccess(inp);
                $v(inp).removeEvent('focus', searchInputFocusHandler);
            });
        }
    };

    // Submit формы поиска
    this._onSubmit = function() {
        var handler = new Common.formHandler(_this._form);
        handler.errors = [];

        _this._onSubmitCheckField('from', handler);
        _this._onSubmitCheckField('till', handler);

        var dateP = _this._form.date_dep.value.split('.'),
                dateD = GV.page.lang == 'en' ? dateP[1] : dateP[0],
                dateM = GV.page.lang == 'en' ? dateP[0] : dateP[1],
                dateY = dateP[2];
        var date = new Date(dateY, dateM - 1, dateD);
        var today = new Date();
        today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        if (date < today)
            handler.addError('wrong_date', _this._form.date_dep);

        if (!handler.showErrors()) {
            trains = new TTrains();
            Common.ajax(GV.page.module + '/search/', {form: _this._form}, function(resp) {
                if (resp.error) {
                    if (!Helper.limitError(resp, _this._onSubmit))
                        trains.error(resp.value);
                } else {
                    trains.show(resp.value);
                }
            });
        }
        return false;
    };

    // Откуда-куда поменять местами
    this._changeDirection = function() {
        var tmp = this._form.station_id_from.value;
        this._form.station_id_from.value = this._form.station_id_till.value;
        this._form.station_id_till.value = tmp;

        tmp = this._form.station_from.value;
        this._form.station_from.value = this._form.station_till.value;
        this._form.station_till.value = tmp;
    };

    this._onCartUpdate = function() {
        if (_this.isRoundTrip() && (Cart.placesCount == 1)) {
            $v('search_back').show(true);
        } else {
            $v('search_back').show(false);
        }
    };

    this.isRoundTrip = function() {
        return this._form.round_trip.checked;
    };

    this.isAnotherEc = function() {
        return this._form.another_ec.checked;
    };

    /*
     * Инициализация элементов
     */
    this._initAutoSuggest('from');
    this._initAutoSuggest('till');

    this._initCalendar();
    this._initEvents();

    this._form.station_from.focus();

    Cart.setOnCartUpdateCallback(this._onCartUpdate);
    Cart.initCartTable();

    if (GV.data.form_data) {
        this._form.station_id_from.value = GV.data.form_data.station_id_from ? GV.data.form_data.station_id_from : '';
        this._form.station_id_till.value = GV.data.form_data.station_id_till ? GV.data.form_data.station_id_till : '';
        this._form.station_from.value = GV.data.form_data.station_from ? GV.data.form_data.station_from : '';
        this._form.station_till.value = GV.data.form_data.station_till ? GV.data.form_data.station_till : '';
        if (GV.data.form_data.date_dep)
            this._form.date_dep.value = GV.data.form_data.date_dep;
        if (GV.data.form_data.time_dep)
            this._form.time_dep.value = GV.data.form_data.time_dep;
        if (GV.data.form_data.time_dep_till)
            this._form.time_dep_till.value = GV.data.form_data.time_dep_till;
        if (GV.data.form_data.round_trip == 1)
            this._form.round_trip.checked = true;
        if (GV.data.form_data.another_ec == 1)
            this._form.another_ec.checked = true;
        this._dateToBtn();

        if (!GV.data.form_data.skip_submit)
            this._form.onsubmit();
    }
};

// Данные одного поезда
var TTrain = function(params /* Controller response */) {

    this.num = params.num;
    this.model = params.model;
    this.category = params.category;
    this.from = {
        date: params.from.date,
        src_date: params.from.src_date,
        station: params.from.station,
        station_id: params.from.station_id
    };
    this.till = {
        date: params.till.date,
        src_date: params.till.src_date,
        station: params.till.station,
        station_id: params.till.station_id
    };

    this.types = [];
    for (var i = 0; i < params.types.length; i++) {
        this.types.push({
            letter: params.types[i].letter,
            title: params.types[i].title,
            placesCnt: params.types[i].places
        });
    }

    this.coaches = [];  /* TCoach[] */
};
TTrain.prototype.getCategoryName = function() {
    var map = {1: _('uz_train_intercity_p'), 2: _('uz_train_intercity')};
    return map[this.category];
};
TTrain.prototype.getNumFormat = function() {
    return this.num.substr(0, 3) + ' ' + this.num.substr(3, 1);
};
TTrain.prototype.setCoaches = function(srcCoaches /* Controller response */) {
    this.coaches = [];
    for (var i = 0; i < srcCoaches.length; i++)
        this.coaches.push(new TCoach(srcCoaches[i]));
};

// Данные одного вагона
var TCoach = function(params) {

    this.num = params.num;
    this.coachTypeId = params.coach_type_id;
    this.placesCnt = params.places_cnt;
    this.hasBedding = params.hasBedding;
    this.services = params.services;
    this.allowBonus = params.allow_bonus;

    this.prices = params.prices;
    this.reservePrice = params.reserve_price;

    this.isLoaded = false;
    this.places = {};
};
TCoach.prototype.hasPlace = function(num) {
    for (var k in this.places) {
        for (var i = 0; i < this.places[k].length; i++) {
            if (this.places[k][i] == num)
                return true;
        }
    }
    return false;
};
TCoach.prototype.getPlacePrice = function(num) {
    for (var k in this.places) {
        for (var i = 0; i < this.places[k].length; i++) {
            if (this.places[k][i] == num)
                return Number(this.prices[k]);
        }
    }
    for (var i in this.prices)
        return Number(this.prices[i]);

    return 0;
};
TCoach.prototype.getPlaceGroup = function(num) {
    for (var k in this.places) {
        for (var i = 0; i < this.places[k].length; i++) {
            if (this.places[k][i] == num)
                return k;
        }
    }
    return 0;
};

var TSelectedPlaces = function() {

    this._selected = {};

    this.set = function(num, coach /* TCoach */, guiRow) {
        if (!this._selected[coach.num])
            this._selected[coach.num] = {};

        this._selected[coach.num][num] = {
            num: num,
            coach: coach,
            guiRow: guiRow
        };
    };

    this.unset = function(num, coach /* TCoach */) {
        var place = this._selected[coach.num][num];
        delete this._selected[coach.num][num];
        place.guiRow.remove();
    };

    this.has = function(num, coach /* TCoach */) {
        if (!this._selected[coach.num])
            return false;
        return !!this._selected[coach.num][num];
    };

    this.get = function(num, coach /* TCoach */) {
        if (!this.has(num, coach))
            return false;
        return this._selected[coach.num][num];
    };

    this.getList = function(coach /* TCoach */) {
        var list = [];
        for (var c in this._selected) {
            if (coach && (coach.num != c))
                continue;

            for (var n in this._selected[c]) {
                list.push(this._selected[c][n]);
            }
        }
        return list;
    };
};

// Список поездов - результат поиска
var TTrains = function() {
    var _this = this;

    this._resultContainer = $v('ts_res');
    this._notFoundContainer = $v('ts_res_not_found');

    this._table = $v('ts_res_tbl').dataTable();
    this._tbody = this._table.table.$$('tbody', true);

    this._list = [];    /* TTrain[] */

    this.clear = function() {
        this._resultContainer.style('display', 'none');
        this._notFoundContainer.style('display', 'none');
    };

    this.error = function(message) {
        if (message)
            this._notFoundContainer.html(message);
        this._notFoundContainer.style('display', 'block');
    };

    this.show = function(srcTrains /* controller response */) {
        if (!srcTrains || !srcTrains.length) {
            this.error();
            return;
        }

        this._resultContainer.style('display', 'block');
        this._tbody.removeChilds();

        for (var i = 0; i < srcTrains.length; i++) {
            var train = new TTrain(srcTrains[i]);
            this._list.push(train);

            this._addRow(train);
        }

        this._table.init();
        this._table.sortInit();
    };

    this._onTrainNumClick = function(train /* TTrain */) {
        (new TTrainRoute(train)).show();
    };

    this._addWagonType = function(td, type, train /* TTrain */) {
        var div = td.add('div'),
                it = div.add('i');  // Title типа
        div.set('title', type.title);

        // Hover для типа
        it.html(type.letter).removeClass('show').style('marginLeft', 0);
        div.addMouseOut(function() {
            it.html(type.letter).removeClass('show').style('marginLeft', 0);
        });
        div.addMouseOver(function() {
            if (!it.classExists('show')) {
                var pw = it.bounds().fullWidth;
                it.html(type.title)
                        .style('whiteSpace', 'nowrap')
                        .addClass('show')
                        .style('marginLeft', (pw - it.bounds().fullWidth) + 'px');
            }
        });

        // create places count and choose button
        var btn = {
            innerHTML: _('uz_ts_res_choose'),
            disabled: type.placesCnt == 0,
            onclick: function() {
                _this._onWagonTypeClick(type, train);
                return false;
            }
        };
        div.add([
            ['b', type.placesCnt],
            ['button', btn]
        ]);
    };

    this._onWagonTypeClick = function(type, train /* TTrain */) {
        var params = {
            station_id_from: train.from.station_id,
            station_id_till: train.till.station_id,
            train: train.num,
            coach_type: type.letter,
            model: train.model,
            date_dep: train.from.date,
            round_trip: frmSearch.isRoundTrip(),
            another_ec: frmSearch.isAnotherEc()
        };
        Common.ajax(GV.page.module + '/coaches/', {params: params}, function(resp) {
            if (resp.error) {
                if (!Helper.limitError(resp, function() {
                    _this._onWagonTypeClick(type, train);
                })) {
                    if (resp.data == 400)     // system is overloaded
                        Common.pageInformation(resp.value);
                    else
                        Common.pageInformation('error', resp.value);
                }
                return;
            }
            if (!resp.value.content) {
                Common.pageInformation();
                return;
            }

            Helper.currTrain = train;
            Helper.placesAllowed = resp.value.places_allowed;
            Helper.placesMax = resp.value.places_max;
            train.setCoaches(resp.value.coaches);

            trainPopup = new TTrainPopup(resp.value);
            trainPopup.show();
        });
    };

    this._addRow = function(train /* TTrain */) {
        var tr = this._tbody.add('tr'),
                td = null,
                dfromSrc = train.from.src_date,
                dtillSrc = train.till.src_date;

        // Номер поезда
        td = tr.add('td', {className: 'num'});
        if (Helper.settings.train_route_on) {
            td.add([
                ['a', {
                        innerHTML: train.getNumFormat(),
                        onclick: function() {
                            _this._onTrainNumClick(train);
                            return false;
                        }
                    }]
            ]);
        } else {
            td.add([['span', train.getNumFormat()]]);
        }
        td.add([['div', train.getCategoryName()]]);

        // Откуда-куда
        td = tr.add('td', {className: 'stations'});
        td.html(train.from.station + '<br/>' + train.till.station);

        // Дата
        td = tr.add('td', {className: 'date'});
        td.add([
            ['div', _('uz_ts_res_departure') + ' <span>' + TDateTime.dFormat(dfromSrc) + '</span>'],
            ['p', {className: 'clear'}],
            ['div', _('uz_ts_res_arrival') + ' <span>' + TDateTime.dFormat(dtillSrc) + '</span>'],
            ['p', {className: 'clear'}]
        ]);

        // Время прибытия-отправления
        td = tr.add('td', {className: 'time'});
        td.html(TDateTime.tFormat(dfromSrc) + '<br/>' + TDateTime.tFormat(dtillSrc));

        // Время в пути
        td = tr.add('td', {className: 'dur'});
        td.html(TDateTime.timeDiff(train.from.date, train.till.date));

        // Места
        td = tr.add('td', {className: 'place'});
        for (var i = 0; i < train.types.length; i++)
            this._addWagonType(td, train.types[i], train);
    };

    this.clear();
};

var TTrainRoute = function(train /* TTRain */) {
    var _this = this;
    this._train = train;

    this.show = function() {
        var params = {
            station_id_from: this._train.from.station_id,
            station_id_till: this._train.till.station_id,
            train: this._train.num,
            date_dep: this._train.from.date
        };
        Common.ajax(GV.page.module + '/train_route/', {params: params}, function(resp) {
            if (resp.error) {
                if (!Helper.limitError(resp, function() {
                    _this.show();
                }))
                    Common.pageInformation('error', resp.value);
                return;
            }

            $v.alert(_('train_route'), resp.value);
            var mapLink = document.getElementById('train_on_map');
            if (mapLink) {
                mapLink.onclick = function() {
                    (new TTrainMap(_this._train)).show();
                    return false;
                };
            }
        });
    };
};

var TTrainMap = function(train) {
    var _this = this;

    this._train = train;
    this._map = null;
    this._stations = [];

    this.show = function() {
        var params = {
            station_id_from: this._train.from.station_id,
            station_id_till: this._train.till.station_id,
            train: this._train.num,
            date_dep: this._train.from.date
        };
        Common.ajax(GV.page.module + '/train_route_map/', {params: params}, function(resp) {
            if (resp.error) {
                if (!Helper.limitError(resp, function() {
                    _this.show();
                }))
                    Common.pageInformation('error', resp.value);
                return;
            }

            _this._stations = resp.data.stations;
            if (!_this._stations || (_this._stations.length == 0)) {
                Common.pageInformation('error', 'Error building map');
            } else {
                $v.alert(_('train_route_map'), resp.value);
                ymaps.ready(function() {
                    _this.init();
                });
            }
        });
    };

    this.init = function() {
        var st = this._stations;

        // Init map
        this._map = new ymaps.Map("train_route_map", {
            behaviors: ['default', 'scrollZoom'],
            center: [st[0]['coord']['lat'], st[0]['coord']['long']],
            zoom: 7
        });
        this._map.controls.add('mapTools').add('zoomControl').add('scaleLine');

        // Build route line
        var route = [];
        for (var i = 0; i < st.length; i++)
            route.push([st[i]['coord']['lat'], st[i]['coord']['long']]);
        var routePolyline = new ymaps.Polyline(route, {}, {
            strokeWidth: 7
        });
        this._map.geoObjects.add(routePolyline);

        // Build points
        var pointCollection = new ymaps.GeoObjectCollection({}, {
            preset: 'twirl#blueStretchyIcon'
        });
        for (var i = 0; i < st.length; i++) {
            pointCollection.add(new ymaps.Placemark([st[i]['coord']['lat'], st[i]['coord']['long']], {
                iconContent: st[i].name,
                hintContent: _('uz_ts_res_arrival') + ': ' + st[i].arrival_time,
                balloonContent: '<h4>' + st[i].name + '</h4><p><span>' + _('uz_ts_res_arrival') + ':</span>' + st[i].arrival_time + '</p><p><span>' + _('uz_ts_res_departure') + ':</span>' + st[i].departure_time + '</p><p><span>' + _('uz_distance') + ':</span>' + st[i].distance + '</p>'
            }));
        }
        this._map.geoObjects.add(pointCollection);
    };
};

var TTrainPopup = function(params) {
    var _this = this;

    this._params = params;

    this._popup = null;
    this._wagonTabs = null;

    this._tbl = null;
    this._dataTbl = null;
    this._tblRows = null;

    this._info = null;

    this._coachTooltip = $v.add('div', {className: 'ts_chs_coach_tooltip'});

    this._placeBtns = {};

    this._effects = [
        function(n) {
            return n * Math.ceil(700 / Helper.currCoach.placesCnt);
        },
        function(n) {
            return (Helper.currCoach.placesCnt - n) * Math.ceil(700 / Helper.currCoach.placesCnt);
        },
        function() {
            return $v.rand(0, Helper.currCoach.placesCnt) * Math.ceil(700 / Helper.currCoach.placesCnt);
        }
    ];

    this._initGuiVars = function() {
        this._tbl = $v('ts_chs_tbl');
        this._dataTbl = this._tbl.$$('table', true).dataTable();
        this._tblRows = this._dataTbl.table.get().rows;
        this._info = $v('ts_chs_info');

        this._wagonTabs = this._popup.elements.header.$$('span', true).add('span', {className: 'coaches'});
    };

    this.show = function() {
        selectedPlaces = new TSelectedPlaces();
        this._popup = $v.popup(
                '<span class="title">' + _('uz_ts_res_select_place', Helper.currTrain.getNumFormat()) + '</span>',
                this._params.content,
                {
                    aniScale: false,
                    className: 'coachScheme',
                    onopen: _this._onPopupOpen,
                    top: 100 + ($v.browser.webkit ? window.pageYOffset : document.documentElement.scrollTop),
                    onbeforeclose: _this._onBeforeClose
                }
        );

        var dfrom = TDateTime.dFormat(Helper.currTrain.from.src_date).split(', ')[1];
        this._popup.elements.header.add('div', {innerHTML: dfrom, className: 'date'});

        this._initGuiVars();
        for (var i = 0; i < Helper.currTrain.coaches.length; i++)
            this._addTab(Helper.currTrain.coaches[i]);

        this._ifAnotherEc();

        this._info.$$('span').addClick(function() {
            $v.alert(_('uz_ts_chs_info_ttl'), $('coach_chooser_help'));
        });
        this._tbl.$$('.complex_btn', true).addClick(function() {
            _this._onSubmit();
        });

        this._selectCoach(Helper.currTrain.coaches[0]);
    };

    this._onBeforeClose = function(eventReason) {
        if ((eventReason.reason != 'BG') && (eventReason.reason != 'KEY'))
            return true;

        if (selectedPlaces.getList().length == 0)
            return true;

        $v.confirm(null, _('close_confirm'), function() {
            _this._popup.close();
        });
        return false;
    };

    this._addTab = function(coach) {
        var tooltipText = _('uz_ts_chs_coach_number', coach.num, coach.placesCnt);

        var tab = this._wagonTabs.add('a', {
            innerHTML: coach.num,
            href: '#' + coach.num,
            onclick: function() {
                _this._selectCoach(coach);
                return false;
            },
            onmouseover: function() {
                var b = $v(this).bounds();
                _this._coachTooltip
                        .html(tooltipText)
                        .show(true, true)
                        .style({
                            left: (b.left) + 'px',
                            top: (b.top + b.height + 7) + 'px'
                        });
            },
            onmouseout: function() {
                _this._coachTooltip.show(false);
            }
        });
        tab.add('b').add('_text', coach.placesCnt);
    };

    this._onPopupOpen = function() {
        this.elements.main.style('height', 'auto');
        this.elements.content.style('height', 'auto');
    };

    this._ifAnotherEc = function() {
        if (!frmSearch.isAnotherEc())
            return;

        // кнопка добавления мест для другово ВЦ
        $$v('#chs_places_top').inputMask($v.parserNumeric(0, 1));
        $$v('#chs_places_bottom').inputMask($v.parserNumeric(0, 1));
        $$v('#chs_add_place').addClick(this._onAnotherEcAddClick);
    };

    this._setCurrentCoach = function(coach /* TCoach */) {
        Helper.currCoach = coach;
        var tabs = this._wagonTabs.elem.childNodes;
        for (var j = 0; j < tabs.length; j++) {
            var num = tabs[j].href.substr(tabs[j].href.indexOf('#') + 1);
            $v(tabs[j]).replaceClass('', 'active', num == coach.num);
        }
    };

    this._selectCoach = function(coach /* TCoach */) {
        if (Helper.currCoach == coach)
            return;

        // Другой ВЦ
        if (frmSearch.isAnotherEc()) {
            this._setCurrentCoach(coach);
            return;
        }

        // Данные вагона уже загружены
        if (coach.isLoaded && (Helper.currTrain.model == 0)) {
            this._showCoach(coach);
            return;
        }

        var changeScheme = !Helper.currCoach || (Helper.currCoach.coachTypeId != coach.coachTypeId);
        if (hasLocalStorage && changeScheme)
            changeScheme = localStorage.getItem('coach_scheme_v32_' + coach.coachTypeId) === null;
        var params = {
            station_id_from: Helper.currTrain.from.station_id,
            station_id_till: Helper.currTrain.till.station_id,
            train: Helper.currTrain.num,
            coach_num: coach.num,
            coach_type_id: coach.coachTypeId,
            date_dep: Helper.currTrain.from.date,
            change_scheme: changeScheme
        };

        Common.ajax(GV.page.module + '/coach/', {params: params}, function(resp) {
            if (resp.error) {
                if (!Helper.limitError(resp, function() {
                    _this._selectCoach(coach);
                })) {
                    if (resp.data == 400)     // система перегружена
                        Common.pageInformation(resp.value);
                    else
                        Common.pageInformation('error', resp.value);
                }

                return;
            }

            _this._onLoadCoach(coach, resp.value);
        });
    };

    this._onLoadCoach = function(coach /* TCoach */, data) {
        coach.isLoaded = true;
        coach.places = data.places;

        var changeScheme = !Helper.currCoach || (Helper.currCoach.coachTypeId != coach.coachTypeId);
        if (changeScheme) {    // поменялась схема вагона
            var coachScheme;
            if (hasLocalStorage) {
                if (localStorage.getItem('coach_scheme_v32_' + coach.coachTypeId) === null) {
                    localStorage.setItem('coach_scheme_v32_' + coach.coachTypeId, data.content);
                    coachScheme = data.content;
                } else {
                    coachScheme = localStorage.getItem('coach_scheme_v32_' + coach.coachTypeId);
                }
            } else {
                coachScheme = data.content;
            }

            if (this._placeBtns)
                this._reloadScheme(data.css, coachScheme);
        }

        this._showCoach(coach);
    };

    this._showCoach = function(coach /* TCoach */) {
        if (Helper.currCoach == coach)
            return;

        var first = !Helper.currCoach,
                back = Helper.currCoach && (Helper.currCoach.num > coach.num),
                coachNumCont = $$v('#ts_chs_scheme span b', true);

        this._setCurrentCoach(coach);

        if (!vTools.browser.ie) {
            // coach num effect
            var ceffl = -60, ceffr = 35, cefftout = {};

            coachNumCont.opacityFade(0, {step: 10}).styleTransform('marginLeft', (back ? ceffr : ceffl), false, 300, cefftout, 'fast', function() {
                coachNumCont.html('№<br/>' + coach.num);
                coachNumCont.style('marginLeft', (back ? ceffl : ceffr) + 'px');
                coachNumCont.opacityFade(100, {fromVal: 0}).styleTransform('marginLeft', 0, false, 500, cefftout, 'slow');
            });
        } else {
            coachNumCont.html('№<br/>' + coach.num);
        }

        this._showBtns(first);

        this._popup.setPosition(true);
    };

    this._reloadScheme = function(className, scheme) {
        var schemeObj = this._popup.elements.content.$$('#ts_chs_scheme', true);
        schemeObj.elem.className = 'coach_scheme ' + className;
        schemeObj.$$('#places', true).elem.innerHTML = scheme;
    };

    this._getANum = function(a) {
        return $v(a).$$('span', true).html().replace(/<[^>]+>/g, '');
    };

    this._showBtns = function(first) {
        this._placeBtns = {};
        $$v('#ts_chs_scheme a').each(function(a) {
            var num = _this._getANum(a);
            a.title = _('uz_ts_chs_btn_ttl', num);
            _this._placeBtns[num] = $v(a);
        });

        var effect = this._effects[$v.rand(0, 2)];
        for (var num in this._placeBtns) {
            var btn = this._placeBtns[num];
            if (first) {
                this._setBtnClasses(btn, num);
            } else {
                var enabled = Helper.currCoach.hasPlace(num);
                if (vTools.browser.ie) {
                    this._appearBtn(btn, num, first, enabled);
                } else {
                    this._showBtnDelay(effect, btn, num, first, enabled);
                }
            }
        }
    };

    this._showBtnDelay = function(effect, btn, num, first, enabled) {
        var delay = effect(num);
        setTimeout(function() {
            btn.opacityFade(0, {
                step: 20,
                oncomplete: function() {
                    _this._appearBtn(btn, num, first, enabled);
                }
            });
        }, delay);
    };

    this._setBtnClasses = function(btn, num) {
        btn.removeClass('free');
        btn.removeEvent('click', this._onBtnPlaceClick);
        btn.addEvent('click', this._onBtnPlaceClick);

        if (!Helper.currCoach.hasPlace(num)) {
            btn.replaceClass('active', '');
        } else {
            btn.addClass('free').removeClass('active');
            if (selectedPlaces.has(num, Helper.currCoach))
                btn.addClass('active');
        }
    };

    this._disableBtn = function(num, coach /* TCoach */) {
        if (Helper.currCoach != coach)
            return;
        if (!_this._placeBtns[num])
            return;
        _this._placeBtns[num].removeClass('free');
    };

    this._onBtnPlaceClick = function(event) {
        if (event.preventDefault)
            event.preventDefault();
        var eThis = this;
        if (!eThis.tagName || (eThis.tagName.toLowerCase() != 'a')) {   // Hack for IE
            eThis = event.srcElement;
            if (eThis.tagName.toLowerCase() != 'a')
                eThis = $v(eThis).parent('a').elem;
        }
        if (!$v(eThis).classExists('free'))
            return false;

        var num = _this._getANum(eThis);
        _this.togglePlace(num, Helper.currCoach);
        return false;
    };

    this._appearBtn = function(btn, num, first, enabled) {
        this._setBtnClasses(btn, num);
        if (!first && !vTools.browser.ie)
            btn.opacityFade(100, {step: 10});

        if (enabled && (Helper.currCoach.placesCnt <= 54)) {
            if (!btn._uz_ani_prev_font)
                btn._uz_ani_prev_font = btn.styleInt('font-size');
            if (!vTools.browser.ie)
                btn.styleTransform('font-size', btn._uz_ani_prev_font, btn._uz_ani_prev_font + 5, 100);
        }
    };

    this._onAnotherEcAddClick = function() {
        var isTableShowed = _this._tblRows.length > 1;
        if (Helper.placesAllowed <= 0) {
            setTimeout(function() {
                Common.pageInformation(frmSearch.isRoundTrip() ? _('uz_ts_chs_err_round_trip_max_places') :
                        _('uz_ts_chs_err_max_places', Helper.placesMax));
            }, 250);
            return;
        }

        if (selectedPlaces.getList(Helper.currCoach).length >= Helper.currCoach.placesCnt) {
            Common.pageInformation(_('not_enough_places', Helper.currCoach.num));
            return;
        }

        var tBody = _this._tbl.$$('table tbody', true),
                tr = tBody.add('tr'),
                tblTr = _this._dataTbl.tr(_this._tblRows.length - 1).init();

        var num = $v.uuid();
        var row = new TPlaceRow({
            tr: tr,
            tblTr: tblTr,
            placeNum: num,
            popup: this._popup
        });
        selectedPlaces.set(num, Helper.currCoach, row);
        row.show(_this._tblRows.length - 1);

        _this._tbl.style('display', '');
        _this._info.style('display', 'none');

        if (!isTableShowed && !vTools.browser.ie)
            _this._tbl.opacityFade(100, 0);
    };

    this._toggleMessagedShowed = false;
    this.togglePlace = function(num, coach /* TCoach */) {
        var prevShow = this._tblRows.length > 1,
                place = selectedPlaces.get(num, coach);

        if (!place && (Helper.placesAllowed <= 0)) {
            if (this._toggleMessagedShowed)
                return;
            this._toggleMessagedShowed = true;
            setTimeout(function() {
                _this._toggleMessagedShowed = false;
                Common.pageInformation(frmSearch.isRoundTrip() ? _('uz_ts_chs_err_round_trip_max_places') :
                        _('uz_ts_chs_err_max_places', Helper.placesMax));
            }, 250);
            return;
        }

        if (!place) {
            var tBody = this._tbl.$$('table tbody', true),
                    tr = tBody.add('tr'),
                    tblTr = this._dataTbl.tr(this._tblRows.length - 1).init();

            var row = new TPlaceRow({
                tr: tr,
                tblTr: tblTr,
                placeNum: num,
                popup: this._popup
            });
            selectedPlaces.set(num, coach, row);
            row.show(this._tblRows.length - 1);

        } else {
            selectedPlaces.unset(num, coach);
        }

        if (coach == Helper.currCoach && !frmSearch.isAnotherEc())
            this._placeBtns[num].replaceClass('', 'active', !place);

        var showTbl = this._tblRows.length > 1;
        this._tbl.style('display', showTbl ? '' : 'none');
        this._info.style('display', showTbl ? 'none' : '');

        if ((prevShow != showTbl) && !vTools.browser.ie) {
            if (showTbl) {
                this._tbl.opacityFade(100, 0);
            } else {
                if (this._info.elem)
                    this._info.opacityFade(100, 0);
            }
        }

        this._popup.setContentSize();
    };

    this.refreshButtonPrice = function() {
        var price = 0;
        var selected = selectedPlaces.getList();
        for (var i = 0; i < selected.length; i++)
            price += selected[i].guiRow.getPrice();

        this._tbl.$$('.complex_btn b', true).html(Common.nf(price) + ' ' + _('uah'));
    };

    this._onSubmit = function() {
        var params = {
            code_station_from: Helper.currTrain.from.station_id,
            code_station_to: Helper.currTrain.till.station_id,
            train: Helper.currTrain.num,
            date: Helper.currTrain.from.date,
            round_trip: frmSearch.isRoundTrip(),
            places: []
        },
        errors;

        var selected = selectedPlaces.getList();
        for (var i = 0; i < selected.length; i++) {
            var s = selected[i];
            if (!s.guiRow.isReserve() && s.guiRow.isChild() && !s.guiRow.getChildBirth().length) {
                errors = 'birthdate';
                break;
            }

            // проверка имени
            var names = [s.guiRow.getFirstName(), s.guiRow.getLastName()];
            for (var k = 0; k < names.length; ++k) {
                if (!Common.checkNamePattern(names[k])) {
                    errors = 'name';
                    break;
                }
            }

            var place = {
                ord: i,
                coach_num: s.coach.num,
                coach_type_id: s.coach.coachTypeId,
                place_num: s.num,
                firstname: s.guiRow.getFirstName(),
                lastname: s.guiRow.getLastName(),
                bedding: s.guiRow.hasBedding(),
                services: s.guiRow.getServices(),
                child: s.guiRow.isChild() ? s.guiRow.getChildBirth() : '',
                stud: s.guiRow.isStud() ? s.guiRow.getStudNumber() : '',
                transp: s.guiRow.getTransp(),
                reserve: s.guiRow.isReserve()
            };
            if (s.guiRow.isBonusAllowed())
                place.bonus = s.guiRow.hasBonus();

            params.places.push(place);
        }

        if (frmSearch.isAnotherEc()) {
            params.another_ec = {
                'count_top': $('chs_places_top').value,
                'count_lower': $('chs_places_bottom').value,
                'no_side': $('chs_no_side').checked,
                'one_coupe': $('chs_one_coupe').checked,
                'places_from': $('chs_places_from').value,
                'places_to': $('chs_places_to').value
            };

            var wantedCount = parseInt($('chs_places_top').value) + parseInt($('chs_places_bottom').value);
            if (wantedCount > selected.length)
                errors = 'desired';
        }

        if (errors) {
            Common.pageInformation('error_form', _('wrong_' + errors + '_empty'));
            return;
        }

        Cart.add(params, this._onCartAdd);
    };

    this._onCartAdd = function(resp) {
        if (resp.value.ignored_places && (resp.value.ignored_places.length > 0)) {
            var places = resp.value.ignored_places;
            var selected = selectedPlaces.getList();
            for (var i = 0; i < selected.length; i++) {
                if (!$v.inArray(places, i)) {
                    var s = selected[i];
                    trainPopup.togglePlace(s.num, s.coach);
                    _this._disableBtn(s.num, s.coach);
                }
            }

            return;
        }

        _this._popup.close();
    };
};

var TPlaceRow = function(params) {

    this._placeNum = params.placeNum;
    this._coach = Helper.currCoach;

    this._tr = params.tr;
    this._tblTr = params.tblTr;
    this._popup = params.popup;
    this._tdOptions = null;
    this._tdService = null;
    this._tdPrice = null;
};
TPlaceRowPrototype = function() {

    this.isReserve = function() {
        var elems = this._tdOptions.$$('.reserve input[type="radio"]', true).elem;
        if (elems.value == '0')
            return !elems.checked
        else
            return elems.checked
    };

    this.isFull = function() {
        var elems = this._tdOptions.$$('.kind input[type="radio"]').elem;
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].value === 'full')
                return elems[i].checked;
        }
        return false;
    };

    this.isChild = function() {
        var elems = this._tdOptions.$$('.kind input[type="radio"]').elem;
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].value === 'child')
                return elems[i].checked;
        }
        return false;
    };

    this.getChildBirth = function() {
        return this._tdOptions.$$('.kind .js_child .child_birthdate', true).elem.value;
    };

    this.isStud = function() {
        var elems = this._tdOptions.$$('.kind input[type="radio"]').elem;
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].value === 'stud')
                return elems[i].checked;
        }
        return false;
    };

    this.getStudNumber = function() {
        return this._tdOptions.$$('.kind .js_stud .stud_number', true).elem.value;
    };

    this.getTransp = function() {
        var elem = this._tr.$$('.transp_chk').elem[0];
        if (!elem.checked)
            return false;

        var elems = this._tdOptions.$$('.transp input').elem;
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].checked)
                return elems[i].value;
        }

        return false;
    };

    this.getLastName = function() {
        return this._tdOptions.$$('label .lastname', true).get().value;
    };

    this.getFirstName = function() {
        return this._tdOptions.$$('label .firstname', true).get().value;
    };

    this._showBonus = function(flag) {
        this._tdOptions.$$('.bonus').style('visibility', !flag ? 'hidden' : 'visible');
    };

    this.isBonusAllowed = function() {
        var inp = this._tdOptions.$$('.bonus', true);
        if (!inp || inp.isEmpty())
            return false;

        if ((this.getLastName() != Helper.bonusCard.last_name) || (this.getFirstName() != Helper.bonusCard.first_name))
            return false;
        if (!this.isFull())
            return false;
        if (this.isReserve())
            return false;

        return true;
    };

    this.hasBonus = function() {
        var inp = this._tdOptions.$$('.bonus input', true);
        if (!inp || inp.isEmpty())
            return false;
        return inp.elem.checked;
    };

    this.hasBedding = function() {
        var inp = this._tdService.$$('.bedding input', true);
        if (!inp || inp.isEmpty())
            return false;
        return inp.elem.checked;
    };

    this.getServices = function() {
        var list = [];
        var lbl = this._tdService.$$('label').elem;
        for (var i = 0; i < lbl.length; i++) {
            if ($v(lbl[i]).classExists('bedding'))
                continue;
            var inp = $v(lbl[i]).$$('input').elem[0];
            if (inp.checked)
                list.push(inp.value);
        }

        return list;
    };

    this.show = function(idx) {
        var _this = this,
                td;

        // № п.п.
        td = this._tr.add('td', {className: 'num'});
        td.html(idx);

        // Вагон, место
        td = this._tr.add('td', {className: 'place'});
        if (frmSearch.isAnotherEc())
            td.html(_('uz_ts_chs_td_place_title_ec', this._coach.num));
        else
            td.html(_('uz_ts_chs_td_place_title', this._coach.num, this._placeNum));

        // Опции
        this._tdOptions = this._tr.add('td', {className: 'option'});
        ;
        this._addOptionsCol();

        // Сервисы
        this._tdService = this._tr.add('td', {className: 'service'});
        ;
        this._addServices();

        // Цена
        this._tdPrice = this._tr.add('td', {className: 'price'});
        ;

        // Кнопка "отмена"
        td = this._tr.add('td', {className: 'cancel'});
        td.add('a', {
            href: '#',
            innerHTML: _('cancel'),
            onclick: function() {
                trainPopup.togglePlace(_this._placeNum, _this._coach);
                return false;
            }
        });

        if (!vTools.browser.ie) {
            // opacity appearance effect
            this._tr.$$('td').each(function(o, i) {
                $v(o).opacity(0);
                setTimeout(function() {
                    $v(o).opacityFade(100, {step: 7});
                }, i * 100);
            });
        }

        this._refreshPrice();
        this._onAddRemoveRow(true);
    };

    this._addOptionsCol = function() {
        this._addReserveControls();
        this._addKindControls();

        this._tdOptions.add('span', {className: 'clear'});
        this._addTransportationControls();

        this._tdOptions.add('span', {className: 'clear'});

        this._addNameControls();
        this._addBonusControls();
    };

    this._addReserveControls = function() {
        if (!this._coach.reservePrice)
            return;

        var div = this._tdOptions.add('div', {className: 'reserve'});
        div.style('width', '126px');

        var name = $v.uuid();

        this._addReserveBtn(0, div, name);
        if (!frmSearch.isRoundTrip()) {
            var lbl = this._addReserveBtn(1, div, name);
            lbl.add('img', {
                src: GV.site.ht_ihost + GV.site.path.images + 'ico/question.png',
                title: _('reserve_info')
            });
        }
    };

    this._addReserveBtn = function(v, div, name) {
        var _this = this,
                lbl = div.add('label', {title: _('uz_ts_chs_opt_buy' + v + '_promt')});
        lbl.add([
            ['input', {
                    name: name,
                    type: 'radio',
                    value: v,
                    checked: v == 0,
                    onclick: function() {
                        _this._onReserveClick();
                    }
                }],
            ['_text', _('uz_ts_chs_opt_buy' + v)]
        ]);
        return lbl;
    };

    this._onReserveClick = function() {
        var isReserve = this.isReserve(),
                visibility = !isReserve ? 'visible' : 'hidden';

        var list = this._tdService.get().childNodes;
        for (var i = 0; i < list.length; i++)
            list[i].style.visibility = visibility;

        this._tdOptions.$$('.transp', true).elem.style.visibility = visibility;
        this._tdOptions.$$('.js_transp', true).elem.style.visibility = visibility;

        this._tdOptions.$$('.js_child', true).elem.style.visibility = visibility;
        if (isReserve && this._tdOptions.$$('.js_child input[type="radio"]', true).elem.checked)
            this._tdOptions.$$('.js_full input', true).elem.checked = true;
        if (!Helper.settings.stud_on)
            this._tdOptions.$$('.kind', true).elem.style.visibility = visibility;

        this._refreshBonus();
        this._refreshPrice();
        trainPopup.refreshButtonPrice();
    };

    this._addKindControls = function() {
        var div = this._tdOptions.add('div', {className: 'kind'}).style('width', '210px'),
                name = $v.uuid();

        this._addKindBtn('full', div, name);

        // детский
        var divCh = this._addKindBtn('child', div, name);
        var container = divCh.add('span').style('display', 'none');
        container.add('input', {className: 'child_birthdate'}).typingRule(/^[\d\.]*$/);
        container.add('img', {
            src: GV.site.ht_ihost + GV.site.path.images + 'ico/question.png',
            title: _('child_birthdate')
        });

        var minDate = new Date(Helper.currTrain.from.date * 1000);
        minDate.setFullYear(minDate.getFullYear() - 14);
        minDate.setDate(minDate.getDate() + 1);

        var maxDate = null;
        if (vTools.browser.ie == 8) {
            maxDate = new Date();
            maxDate.setDate(maxDate.getDate() - 1);
        } else {
            maxDate = new Date(new Date().toDateString());
        }

        Common.datePicker(this._tdOptions.$$('.kind .js_child .child_birthdate', true).get(), {
            format: GV.page.lc.dff,
            hideOnPick: true,
            listYears: true,
            firstDay: GV.page.lc.fdow,
            minDate: minDate,
            maxDate: maxDate
        });

        // студенческий
        var divSt = this._addKindBtn('stud', div, name);
        var container = divSt.add('span').style('display', 'none');
        container.add('input', {
            className: 'stud_number',
            value: GV.site.user.stud_number || ''
        });
        container.add('img', {
            src: GV.site.ht_ihost + GV.site.path.images + 'ico/question.png',
            title: _('stud_number')
        });
        if (!Helper.settings.stud_on)
            divSt.style('display', 'none');
    };

    this._addKindBtn = function(v, div, name) {
        div = div.add('div', {className: 'js_' + v});

        var _this = this,
                lbl = div.add('label', {title: _('uz_ts_chs_opt_type_' + v + '_promt')});
        lbl.add([
            ['input', {
                    name: name,
                    type: 'radio',
                    value: v,
                    checked: v === 'full',
                    onclick: function() {
                        _this._onKindClick();
                    }
                }],
            ['_text', _('uz_ts_chs_opt_type_' + v)]
        ]);

        return div;
    };

    this._onKindClick = function() {
        this._tdOptions.$$('.kind .js_child span', true).style('display', this.isChild() ? '' : 'none');
        this._tdOptions.$$('.kind .js_stud span', true).style('display', this.isStud() ? '' : 'none');
        this._refreshBonus();
    };

    this._addTransportationControls = function() {
        if (!Helper.settings.transportation_on)
            return;
        var _this = this;

        var inpName = $v.uuid();
        this._tdOptions.add('label', {className: 'js_transp'}, [
            ['input', {
                    type: 'checkbox',
                    className: 'transp_chk',
                    onclick: function() {
                        _this._tdOptions.$$('.transp').style('display', _this.getTransp() === false ? 'none' : 'block');
                        _this._popup.setContentSize();
                    }
                }],
            ['_text', _('uz_ts_chs_opt_transp')],
            ['span', {className: 'transp-q'}, [
                    ['img', {
                            src: GV.site.ht_ihost + GV.site.path.images + 'ico/question.png'
                        }],
                    ['a', {
                            href: 'http://zakon2.rada.gov.ua/laws/show/z0310-07/page',
                            target: '_blank',
                            innerHTML: _('transportation_rules')
                        }]
                ]
            ]
        ]);

        var transpItems = [];
        if (Helper.settings.transp_animal_on) {
            transpItems.push(
                    ['label', {}, [
                            ['input', {
                                    type: 'radio',
                                    name: inpName,
                                    value: 'С'
                                }],
                            ['_text', _('uz_ts_chs_opt_transp0')]
                        ]]
                    );
        }

        transpItems.push(
                ['label', {}, [
                        ['input', {
                                type: 'radio',
                                name: inpName,
                                value: 'А'
                            }],
                        ['_text', _('uz_ts_chs_opt_transp1')]
                    ]]
                );
        transpItems.push(
                ['label', {}, [
                        ['input', {
                                type: 'radio',
                                name: inpName,
                                value: 'Б'
                            }],
                        ['_text', _('uz_ts_chs_opt_transp2')]
                    ]]
                );
        transpItems.push(
                ['img', {
                        src: GV.site.ht_ihost + GV.site.path.images + 'ico/question.png',
                        title: _('uz_ts_chs_opt_transp2_info')
                    }]
                );
        transpItems[0][2][0][1].checked = true;

        this._tdOptions.add('div', {className: 'transp'}, transpItems);
    };

    this._addNameControls = function() {
        var lastName_text = _('lastname'),
                fioCont = this._tdOptions.add('div', {className: 'fio'}),
        lastName_lbl = fioCont.add('label', {innerHTML: lastName_text}),
        firstName_text = _('firstname'),
                firstName_lbl = fioCont.add('label', {innerHTML: firstName_text});

        lastName_lbl.add('input', {
            className: 'lastname',
            display: 'block',
            title: lastName_text,
            value: GV.site.user.last_name || ''
        });

        firstName_lbl.add('input', {
            className: 'firstname',
            display: 'block',
            title: firstName_text,
            value: GV.site.user.first_name || ''
        });
    };

    this._addBonusControls = function() {
        if (!this._coach.allowBonus || frmSearch.isRoundTrip())
            return;
        if (!GV.site.user.user_id)
            return;
        var _this = this;

        this._tdOptions.$$('label .lastname').addEvent('keyup', function() {
            _this._refreshBonus();
        });
        this._tdOptions.$$('label .firstname').addEvent('keyup', function() {
            _this._refreshBonus();
        });

        this._tdOptions.add('label', {className: 'bonus'}, [
            ['input', {
                    type: 'checkbox'
                }],
            ['_text', _('uz_ts_chs_opt_bonus')]
        ]);
    };

    this._addServices = function() {
        // Постель
        if (this._coach.hasBedding) {
            this._tdService.add('div').add('label', {className: 'bedding'}, [
                ['input', {
                        type: 'checkbox',
                        checked: true
                    }],
                ['_text', _('uz_ts_chs_opt_bedding')]
            ]);
        }

        // добавление сервисов
        var services = this._coach.services;
        for (var i = 0, l = services.length; i < l; ++i) {
            this._tdService.add('div').add('label', false, [
                ['input', {
                        type: 'checkbox',
                        value: services[i]
                    }],
                ['_text', _('ticket_service_' + services[i])]
            ]);
        }
    };

    this.getPrice = function() {
        if (this.isReserve())
            return this._coach.reservePrice;

        return this._coach.getPlacePrice(this._placeNum);
    };

    this._refreshPrice = function() {
        var price = (this.getPrice() + '').split('.');
        this._tdPrice.html(Common.nf(price[0]));
    };

    this._onAddRemoveRow = function(flag) {
        this._refreshBonus();
        Helper.placesAllowed += (flag ? -1 : 1);
        trainPopup.refreshButtonPrice();
    };

    this._refreshBonus = function() {
        // Если находим где-то установленную галочку - пытаемся оставить ее
        var selected = selectedPlaces.getList();
        var hasCheck = false;
        for (var i = 0; i < selected.length; i++) {
            var s = selected[i];
            if (!s.coach.allowBonus)
                continue;

            if (s.guiRow.isBonusAllowed() && s.guiRow.hasBonus()) {
                hasCheck = true;
                break;
            }
        }
        if (!hasCheck) {
            // Показываем только первую галочку. Остальные прячем
            var showBonus = true;
            for (var i = 0; i < selected.length; i++) {
                var s = selected[i];
                if (!s.coach.allowBonus)
                    continue;

                if (s.guiRow._tdOptions.$$('.bonus').isEmpty())
                    continue;
                if (showBonus && s.guiRow.isBonusAllowed()) {
                    s.guiRow._showBonus(true);
                    showBonus = false;
                } else {
                    s.guiRow._showBonus(false);
                    s.guiRow._tdOptions.$$('.bonus input').elem[0].checked = false;
                }
            }
        } else {
            // Прячем все галочки, кроме активной
            for (var i = 0; i < selected.length; i++) {
                var s = selected[i];
                if (!s.coach.allowBonus)
                    continue;

                if (s.guiRow._tdOptions.$$('.bonus').isEmpty() || s.guiRow.hasBonus())
                    continue;
                s.guiRow._showBonus(false);
                s.guiRow._tdOptions.$$('.bonus input').elem[0].checked = false;
            }
        }
    };

    this.remove = function() {
        this._tblTr.remove();
        var rows = this._tblTr.dataTable.table.get().rows;
        for (var i = 1; i < rows.length; i++)
            rows[i].cells[0].innerHTML = i;

        this._onAddRemoveRow();
    };
};
TPlaceRow.prototype = new TPlaceRowPrototype();
