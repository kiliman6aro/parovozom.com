String.prototype.toPlaceNumber = function(){
    var n = parseInt(this, 10);
    var s = n.toString();
    n = (s.length < 2) ? '0'+s : n;
    return n;
};
/**
 * Преобразует число в формет 001 или 0001
 * в зависимости от заданной длины
 * @param {type} length заданная длина формата
 * @returns {String|Number.prototype.pad@call;toString|Number.prototype.pad.res}
 */
Number.prototype.pad = function(length) {
    var res = this.toString();
    while (res.length < length)
        res = '0' + res;
    return res;
};
var Purchase = (function(){
    /**
     * Корзина. Назначение корзины, отображение цены, формирование
     * пересчет итоговой стоимости, при добавлении или удалении
     * нового места в корзину
     * @param {type} wagon_number
     * @param {type} response_prices
     * @returns {_L7.Cart}
     */
    function Cart(wagon_number, response_prices, tmpls, train, wagon_type, wagon_class){
        
        var wagon_number = wagon_number;
        
        var train = train;
        
        var wagon_type = wagon_type;
        
        var wagon_class = wagon_class;
        
        var prices = response_prices;
        
        var templates = tmpls;
        
        var count = 0;
        
        var total = 0.00;
        
        var block = $(templates.tickets_block);
        
        var form = block.find('form');
        
        var items = {};
        
        var element_total = null;
        
        init();
        
        this.add = function(place_number, charline){
            if(count >= 4){
                return false;
            }
            if(!this.has(place_number) && prices.has(wagon_number, charline)){
                count++;
                var price = prices.get(wagon_number, charline);
                var e = generateItem(place_number, price.cost);
                items[place_number] = {price:price.cost, item:e};
                form.append(items[place_number].item);
                update();
                if(count === 1){
                    show();
                }                
                return true;
            } 
        };
        
        this.has = function(place_number){
            return items[place_number] ? true : false;
        };
        
        this.remove = function(place_number){
            if(this.has(place_number)){
                items[place_number].item.remove();
                delete items[place_number];
                count--;
                if(count <= 0){
                    hide();
                    return true;
                }
                update();
                return true;
            }
            return false;
        };
        /**
         * Проверяет правильность заполнения форм
         * 
         * @returns {Boolean}
         */
        this.validate = function(){
            var result = true;
            $('.ticket_item_container').each(function(){
               if(!_validate(this)){
                   result = false;
               }
            });
            return result;
        };
        /**
         * Уничтожить корзину
         * @returns {undefined}
         */
        this.desctruct = function(){
            block.remove();
        };
        /**
        * возвращает html форму заказа, которую можно отправить
        * @returns {jQuery}
        */
        this.toString = function(){
            return block;
        };
        /**
         * Отправка формы
         * @returns {undefined}
         */
        this.submit = function(){
            form.submit();  
        };
        /**
         * Инициализация корзины
         * @returns {undefined}
         */
        function init(){
            hide();
            element_total = $(templates.ticket_total.replace(/{total}/g, total));
            block.append(element_total);
        }
        /**
         * Скрыть корзину
         * @returns {undefined}
         */
        function hide(){
            block.hide();
        }
        /**
         * Отобразить страницу
         * @returns {undefined}
         */
        function show(){
            block.show();
        }
        /**
         * Генерирует элемент корзины
         * @param {type} p номер места
         * @param {type} price стоимость
         * @returns {string}
         */
        function generateItem(p, price){
            var item = templates.ticket_item
                    .replace(/{number}/g, wagon_number)
                    .replace(/{place}/g, p)
                    .replace(/{number_place}/g,p.toPlaceNumber())
                    .replace(/{price}/g, price);
            var container = $(item);
            
            container.find("#luggage_type").select2();
            container.find(".radio_btn_block_1").buttonset();
            container.find(".radio_btn_block_2").buttonset();
            //Закрытие подсказок валидации
            container.find('.error_message i').on('click', function(){
               $(this).parent().hide(); 
            });
            var child_datepicker = function () {
                $('#ui-datepicker-div').addClass('child_datepicker');
                setTimeout(function () {
                    $('#ui-datepicker-div').prepend($(templates.child_calendar_title));
                });
            };
            container.find(".birthday").datepicker({
                changeMonth: true,
                changeYear: true,
                beforeShow: child_datepicker
            });
            return container;
        }
        /**
         * Пересчитывает обзую сумму
         * @returns {undefined}
         */
        function update(){
            total = 0.00;
            for(var p in items){
                total += parseFloat(items[p].price);
            }
            element_total.find('.price span').html(total.toFixed(2));
        }
        function _validate(container){
            var result = true;
             $(container).find('.inp').not('.birthday,.luggage_weight input').each(function(){
                 if($(this).val() === ''){
                    $(this).siblings('.error_message').fadeIn(300).delay(4000).fadeOut(500);
                    result = false;
                 }
                
             });
             $(container).find(':checked').each(function(){
                 
                 var birthday = $(container).find('.birthday');
                 
                 if($(this).val() === 'child' && birthday.val() === ''){
                     birthday.siblings('.error_message').fadeIn(300).delay(4000).fadeOut(500);
                     result = false;
                 }
                 if($(this).attr('type') === 'checkbox'){
                     var weight = $(container).find('.luggage_weight input');
                     if($(container).find('select').val() === 'Б' && weight.val() === ''){
                         weight.siblings('.error_message').fadeIn(300).delay(4000).fadeOut(500);
                         result = false;
                     }
                 }
             });
             return result;
        }
    }
    /**
     * Элемент поезда на странице
     * @param {type} train
     * @param {type} wagon_type
     * @param {type} wagon_class
     * @returns {undefined}
     */
    function Item(train, wagon_type, wagon_class) {
        this.train = train;
        this.wagon_type = wagon_type;
        this.wagon_class = wagon_class;
        this.wagon_number = null;
        
        /**
         * Закрытый элмент: корзина
         * @type c
         */
        var cart = null;

        var selector = '.result_item[train=' + train + '][wagon_type=' + wagon_type + ']';
        if (wagon_class) {
            selector = selector + '[wagon_class=' + wagon_class + ']';
        }
        this.item = $(selector);

        /**
         * Загрузка данных
         * @param {string} act режим загруки
         * @returns {undefined}
         */
        this.load = function(e, act) {
            if (e === 'start') {
                switch (act) {
                    case 'wagons':
                        this.item.find('a.btn').addClass('load_btn');
                        break;
                    case 'places':
                        if (this.scheme) {
                            this.scheme.remove();
                            if(cart){
                                cart.desctruct();
                            }
                            delete this.scheme;
                        }
                        break;
                }

            }
            if (e === 'stop') {
                switch (act) {
                    case 'wagons':
                        this.item.find('a.btn').removeClass('load_btn');
                        break;
                    case 'places':
                        //ничего не делать
                        break;
                }

            }
        };
        /**
         * Делает текущий элемент открытым и помещает его 
         * в фокус, остальные элементы помещает за фокусом
         * @returns {undefined}
         */
        this.show = function() {
            this.item.find('.overlay').remove();
            this.item.append('<i class="close_icon" onclick="Purchase.close();"></i>');
            this.item.addClass('open');
            $('.result_item').not(this.item).addClass('hide_view');
            this.item.find('a.btn').addClass('disabled');
        };
        /**
         * Закрывает элмент, если он был открыт
         * @returns {undefined}
         */
        this.close = function() {
            this.item.find('.wagon').remove();
            this.item.find('.tickets').remove();
            this.item.removeClass('open');
            $('.result_item').not(this.item).removeClass('hide_view');
            this.item.find('i.close_icon').remove();
            this.item.append('<div class="overlay"></div>');
            //Сбросить кнопку
            this.item.find('a.btn').removeClass('disabled');
            this.wagons = null;
        };
        /**
         * Задает ценнник элемент. Отображает цену на элементе
         * @param {string} price
         * @returns {void}
         */
        this.setPrice = function(price) {
            var p = this.item.find('.price');
            p.empty();
            p.html('<span>'+price+'</span>');
            this.item.attr('price', price);
            this.price = price;
            p.parents('.info_element').show();
        };
        /**
         * Задает схему вагона
         * @param {string} s html схема вагона
         * @returns {bool}
         */
        this.setSheme = function(s) {
            if (this.wagons) {
                this.scheme = $(s);
                this.wagons.append(this.scheme);
                return true;
            }
            return false;
        };
        /**
         * Добавляет элмент корзину
         * @param {Cart} c
         * @returns {undefined}
         */
        this.setCart = function(c){
            if(cart){
                cart.desctruct();
            }
            cart = c;
            this.wagons.after(c.toString());
        };
        /**
         * Возвращает корзину
         * @returns {Boolean|Cart}
         */
        this.getCart = function(){
            if(cart){
                return cart;
            }
            return false;
        };
        /**
         * Проверят были ли уже получены вагон
         * @returns {Boolean}
         */
        this.hasWagons = function() {
            return this.wagons ? true : false;
        };
        /**
         * Задает html код списка вагонов к данному элементу
         * а так же посещает его на страницу, если элемент
         * ранее не был добавлен
         * @param {string} wagons html код
         * @returns {Boolean}
         */
        this.setWagons = function(wagons) {
            if (this.hasWagons()) {
                return false;
            }
            this.wagons = $(wagons);
            $(this.item).find('.general_info').after(this.wagons);
            return true;
        };
        /**
         * Список возможных цен элемента
         */
        this.prices = {
            list: {},
            add: function(number, charline, item) {
                if (this.has(number, charline)) {
                    return false;
                }
                if (this.list[number]) {
                    this.list[number][charline] = item;
                } else {
                    this.list[number] = {};
                    this.list[number][charline] = item;
                }
                return true;
            },
            get: function(number, charline) {
                if (!this.has(number, charline)) {
                    return false;
                }
                if (this.list[number][charline]) {
                    return this.list[number][charline];
                }
                return this.list[number];
            },
            has: function(number, charline) {
                if (!this.list[number]) {
                    return false;
                }
                if (!this.list[number][charline]) {
                    return false;
                }
                return true;
            }
        };
        /**
         * Список мест
         */
        this.places = {
            list: {},
            add: function(number, charline, places) {
                if (this.has(number, charline)) {
                    return false;
                }
                if (this.list[number]) {
                    this.list[number][charline] = places;
                } else {
                    this.list[number] = {};
                    this.list[number][charline] = places;
                }
                return true;
            },
            get: function(number, charline) {
                if (!this.has(number, charline)) {
                    return false;
                }
                if (this.list[number][charline]) {
                    return this.list[number][charline];
                }
                return this.list[number];
            },
            has: function(number, charline) {
                if (!this.list[number]) {
                    return false;
                }
                if (!this.list[number][charline]) {
                    return false;
                }
                return true;
            }
        };
        this.submit = function() {

        };
    };
    function Purchase(){
        this.items = 0;
        this.trains = {
            list: {},
            /**
             * Добавляет новый элемент Item в коллекцию
             * @param {Item} item
             * @returns {Boolean}
             */
            add: function(item /* Item */) {
                if (this.has(item.train, item.wagon_type, item.wagon_class)) {
                    return false;
                }
                //Если поезд и тип уже заданы, разница лишь в классе
                if (this.list[item.train] && this.list[item.train][item.wagon_type]) {
                    this.list[item.train][item.wagon_type][item.wagon_class] = item;
                    return true;
                }
                //Если задан только поезд
                if (this.list[item.train]) {
                    this.list[item.train][item.wagon_type] = item;
                    return true;
                }
                //Если такого поезда вообще нет в коллекции, то создаем все с нуля
                this.list[item.train] = {};
                this.list[item.train][item.wagon_type] = {};
                if (item.wagon_class) {
                    this.list[item.train][item.wagon_type][item.wagon_class] = item;
                    return true;
                }
                this.list[item.train][item.wagon_type] = item;
                return true;
            },
            /**
             * Проверяет наличие элемента Item в коллекции, по параметрам
             * @param {type} train номер поезда
             * @param {type} wagon_type тип вагона
             * @param {type} wagon_class класс вагона
             * @returns {Boolean}
             */
            has: function(train, wagon_type, wagon_class) {
                if (!train || !wagon_type || !this.list[train]) {
                    return false;
                }
                if (wagon_class && !this.list[train][wagon_type][wagon_class]) {
                    return false;
                }
                if (!this.list[train][wagon_type]) {
                    return false;
                }
                return true;
            },
            /**
             * Возвращает элемент Item из коллекции, по параметрам
             * @param {type} train номер поезда
             * @param {type} wagon_type тип вагона
             * @param {type} wagon_class класс вагонаы
             * @returns {Item}
             */
            get: function(train, wagon_type, wagon_class) {
                if (this.has(train, wagon_type, wagon_class)) {
                    if (wagon_class) {
                        return this.list[train][wagon_type][wagon_class];
                    }
                    return this.list[train][wagon_type];
                }
            }
        };
        setTimeout(function(){
            //перезагрузить страницу
            //window.location.reload();
        }, 60000);
    };
    /**
     * Отобразить список вагонов для пользователя
     * @param {type} train
     * @param {type} wagon_type
     * @param {type} wagon_class
     * @returns {undefined}
     */
    Purchase.prototype.open = function(train, wagon_type, wagon_class){
        var t;
        var $this = this;
        if (this.trains.has(train, wagon_type, wagon_class)) {
            t = this.trains.get(train, wagon_type, wagon_class);
        } else {
            t = new Item(train, wagon_type, wagon_class);
            this.trains.add(t);
        }
        this.item = t;
        if(t.hasWagons()){
            return false;
        }
        t.load('start', 'wagons');
        $.post('desktop/prices', {train: train, wagon_type: wagon_type, wagon_class:wagon_class}, function(response) {
            t.setPrice(response.items[0].cost);
            t.setWagons(response.html);
            t.show();
            t.load('stop', 'wagons');
            for(var i = 0; i < response.items.length; i++){
                for(charline in response.items[i].prices){
                    t.prices.add(response.items[i].number, charline, response.items[i].prices[charline]);
                }
            }
            $($this).trigger('show', [t.item]);
            return true;
        }, 'json');
        
    };
    /**
     * Закрывает открытый элемент
     * @returns {Boolean}
     */
    Purchase.prototype.close = function(){
        if(!this.item){return false;}
        this.item.close();
        delete this.item;
    };
    /**
     * Отобразить свободные места на схеме вагона
     * @returns {undefined}
     */
    Purchase.prototype.places = function(wagon_number){
        if(!this.item){return false;}
        var $this = this;
        t = this.item;
        t.load('start','places');
        $.post('desktop/places', {train: t.train, wagon_number:wagon_number, wagon_type: t.wagon_type, wagon_class:t.wagon_class}, function(response){
            t.load('stop', 'places');
            t.wagon_number = wagon_number;
            t.setSheme(response.scheme);
            t.setCart(new Cart(wagon_number, t.prices, response.tmpls, t.train, t.wagon_type, t.wagon_class));
            $($this).trigger('place', [response.scheme]);
        },'json');
    };
    /**
     * Добавить элемент в корзину
     * @returns {undefined}
     */
    Purchase.prototype.select = function(number, charline){
        if(!this.item){return false;}
        
        if(this.item.getCart().add(number, charline)){
            $(this).trigger('add_place_to_cart', [number, charline]);
            return true;
        }
        return false;
    };
    /**
     * Удалить место из корзины
     * @param {type} number
     * @param {type} charline
     * @returns {Boolean}
     */
    Purchase.prototype.unselect = function(number, charline){
        if(!this.item){return false;}
        if(this.item.getCart().remove(number)){
            $(this).trigger('remove_place_from_cart', [number, charline]);
            return true;
        }
        return false;
    };
    /**
     * Проверка заполнения полей в формах и отправка
     * запроса, на приобретение
     * @returns {Boolean}
     */
    Purchase.prototype.submit = function(){
        if(!this.item){return false;}
        if(this.item.getCart().validate()){
            this.item.getCart().submit();
        }
        return false;
    };
    
    Purchase.prototype.prices = function(trains){
        var count = 0;
        var $this = this;
        for(var i = 0; i < trains.length; i++){
            $.post('desktop/prices', {train: trains[i]}, function(response) {
                if(response.result !== 'OK'){
                    return false;
                }
                count++;
                for (var i = 0; i < response.items.length; i++) {
                    var wagon = response.items[i];
                    if(wagon.type_code == 'С'){
                        var t = new Item(wagon.train, wagon.type_code, wagon.class_code);
                        $this.trains.add(t);
                        t.setPrice(wagon.cost);
                        continue;
                    }
                    var t = new Item(wagon.train, wagon.type_code);
                    $this.trains.add(t);
                    t.setPrice(wagon.cost);
                }
                //Если получена последняя цена
                if (count === trains.length) {
                    $('#price_sort').prop('disabled', false);
                    $('#price_sort').trigger('change');
                }
            }, 'json');
        }
    };
    return new Purchase();
}());