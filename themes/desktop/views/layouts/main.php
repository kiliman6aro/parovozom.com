<!DOCTYPE html>
<html>
    <head>
        <title><?php echo CHtml::encode($this->getPageTitle()) ?></title>
        <noscript><meta http-equiv="refresh" content="0; URL=/badbrowser.php"></noscript>
        <meta charset="<?php echo Yii::app()->charset; ?>">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/media/css/ui_style/jquery-ui-1.10.3.custom.css"/>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/media/css/styles.css"/>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/media/css/select2.css"/>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/media/css/jslider.css" type="text/css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/media/css/info_slider/animate.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/media/css/info_slider/liquid-slider.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/media/css/main.css"/>
        <link rel="icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/media/images/big_logo.png" sizes="32x32">
        <!--[if lte IE 8]><link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/media/css/ie.css" type="text/css"/><![endif]-->
        <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/media/js/select2.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/media/js/site.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/media/js/jshashtable-2.1_src.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/media/js/jquery.numberformatter-1.2.3.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/media/js/tmpl.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/media/js/jquery.dependClass-0.1.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/media/js/draggable-0.1.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/media/js/jquery.slider.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/media/js/common.js"></script>
    </head>
    <body>
        <div class="bg_wrapper">
            <div class="header">
                <div class="container">
                    <a href="<?php echo Yii::app()->request->hostInfo; ?>" class="logo"></a>
                    <div class="slogan">
                        <?php echo Yii::t('app', 'Онлайн продажа <strong>Ж/Д билетов</strong>') ?>
                    </div>
                    <div class="contact_us opener">
                        <?php echo Yii::t('app', '<strong>Связаться</strong> с нами'); ?>
                    </div>
                    <div class="head_phone"><?php echo Settings::Get('phone', 'contact'); ?></div>
                </div>
            </div>
            <?php echo $content; ?>
        </div>
        <div class="footer">
            <div class="container">
                <span class="copyright">
                    &copy;<?php echo date('Y.'); ?>
                    <b>Паровозом.</b>
                </span>
                <!--<ul class="social_box">
                    <li>
                        <a target="_blank" href="" class="item tw"></a>
                    </li>
                    <li>
                        <a target="_blank" href="" class="item in"></a>
                    </li>
                    <li>
                        <a target="_blank" href="" class="item vk"></a>
                    </li>
                    <li>
                        <a target="_blank" href="" class="item fcb"></a>
                    </li>
                </ul>-->
                <ul class="bottom_nav">
                    <li>
                        <a href="<?php echo Yii::app()->createUrl('booking/default/terms'); ?>"><?php echo Yii::t('app', 'Оферта'); ?></a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->createUrl('faq'); ?>"><?php echo Yii::t('app', 'Вопрос-Ответ'); ?></a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->createUrl('contact'); ?>"><?php echo Yii::t('app', 'Контакты'); ?></a>
                    </li>
                    <li class="contact_us opener">
                        <a href="javascript:void(0)">
                            <?php echo Yii::t('app', '<strong>Связаться</strong> с нами'); ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <?php $this->widget('contact.widgets.feedback.FeedbackWidget'); ?>
        <?php 
        if(Settings::Get('tour')){
            $this->widget('widgets.tour.IntroJS');
        }
        ?>
    </body>
</html>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter32294809 = new Ya.Metrika({
                    id:32294809,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32294809" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->