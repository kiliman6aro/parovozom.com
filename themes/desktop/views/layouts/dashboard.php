<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content" class="content">
    <div class="container">
        <?php $this->widget('booking.widgets.search.SearchForm') ?>
        <?php $this->widget('widgets.advices.AdvicesWidget'); ?>
        <div class="content_item">
            <div class="tbl_layout">
                <div class="l_column user_content">
                    <?php echo $content; ?>
                </div>
                <div class="r_column question_list">
                    <?php $this->widget('faq.widgets.TopQuestions'); ?>
                </div>
            </div>
        </div>
    </div>
</div><!-- content -->
<?php $this->endContent(); ?>