<?php

return array(
    'id' => 'booking',
    'class' => 'application.modules.booking.BookingModule',
    'urlManagerRules' => array(
        'trains' => 'booking/default/index',
        'desktop/prices' => 'booking/default/prices',
        'desktop/places' => 'booking/default/places',
        'desktop/pay' => 'booking/default/pay',
        'booking/verify' => 'booking/default/verify',
        'booking/result/<id:\d+>' => 'booking/default/result',
        'booking/orders' => 'booking/default/orders',
        'terms' => 'booking/default/terms'
    ),
    'controllerMap' => array(
        'stations' => array(
            'class' => 'booking.widgets.search.StationsController'
        ),
        'payment' => array(
            'class' => 'booking.components.plategka.PaymentController'
        )
    ),
    'components' => array(
        'payment' => array(
            'class' => 'booking.components.plategka.PaymentComponent'
        )
    )
);