<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConfirmForm
 *
 * @author kilim
 */
class ConfirmForm extends CFormModel{
    
    public $email;
    
    public $agree;
    
    public function rules(){
        return array(
            array('email, agree', 'required'),
            array('agree', 'boolean'),
            array('email', 'email')
        );
    }
    
    public function attributeLabels(){
        return array(
            'email' => BookingModule::t('app', 'Email'),
            'agree' => BookingModule::t('app', 'Agree')
        );
    }
    public function getAttributeLabel($attribute) {
        return parent::getAttributeLabel($attribute) . ' <i onClick="$(this).parent(\'.error_message\').fadeOut(\'fast\')" class="close_icon"></i>';
    }
}
