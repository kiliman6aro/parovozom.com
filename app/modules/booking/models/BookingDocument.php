<?php


class BookingDocument extends TravelDocument {
    
    public $reserve = array();

    public static function modelByDom(\DOMDocument $dom, $classname = __CLASS__) {
        $document = parent::modelByDom($dom, $classname);
        $reserveNode = $dom->getElementsByTagName('reserve')->item(0);
        foreach ($reserveNode->childNodes as $child){
            if($child->nodeType != XML_ELEMENT_NODE){
                continue;
            }
            $document->reserve[$child->nodeName] = $child->nodeValue;
        }
        return $document;
    }

    public function getStringPrice() {
        $price = $this->prices[0];
        $strPrice = 'ВАРТ = ';
        $strPrice .= $price['cost'].' ГРН';
        $strPrice .= '(';
        $strPrice .= 'ПОСЛ. '.$price['fee'];
        $strPrice .= ' + ПДВ. '.$price['vat'];
        $strPrice .= ')';
        return $strPrice;
        //return 'ВАРТ = 17,00 ГРН (ПОСЛ. 14,17 + ПДВ. 2,83)';
    }

    public function getStringType() {
        return BookingModule::t('app', 'Документ на услугу');
    }
    /**
     * Возвращает путь к шаблону документа
     * исходя из его типа, подтипа
     * @return string полный путь к файлу шаблона
     */
    public function getLayoutPath(){
        $viewPath = self::VIEW_ROOT;
        $viewPath .= '/'.$this->type;
        return $viewPath;
    }

}
