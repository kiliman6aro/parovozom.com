<?php

/**
 * This is the model class for table "{{order}}".
 *
 * The followings are the available columns in table '{{order}}':
 * @property integer $id
 * @property string $direction
 * @property string $train
 * @property string $wagon
 * @property string $places
 * @property string $create_time
 * @property string $pay_time
 * @property string $total
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Product[] $products
 */
class BaseOrder extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return BOrder the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{order}}';
    }

    /**
     * @return array validation rules for model attributes. 
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that 
        // will receive user inputs. 
        return array(
            array('purchase_id, direction, train, wagon, places', 'required'),
            array('purchase_id', 'numerical', 'integerOnly' => true),
            array('total', 'length', 'max' => 10),
            array('status', 'length', 'max' => 8),
            array('create_time, pay_time', 'safe'),
            // The following rule is used by search(). 
            // Please remove those attributes that should not be searched. 
            array('id, purchase_id, direction, train, wagon, places, create_time, pay_time, total, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules. 
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related 
        // class name for the relations automatically generated below. 
        return array(
            'purchase' => array(self::BELONGS_TO, 'Purchase', 'purchase_id'),
            'products' => array(self::HAS_MANY, 'Product', 'order_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label) 
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'purchase_id' => 'Purchase',
            'direction' => 'Direction',
            'train' => 'Train',
            'wagon' => 'Wagon',
            'places' => 'Places',
            'create_time' => 'Create Time',
            'pay_time' => 'Pay Time',
            'total' => 'Total',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions. 
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that 
        // should not be searched. 

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('purchase_id', $this->purchase_id);
        $criteria->compare('direction', $this->direction, true);
        $criteria->compare('train', $this->train, true);
        $criteria->compare('wagon', $this->wagon, true);
        $criteria->compare('places', $this->places, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('pay_time', $this->pay_time, true);
        $criteria->compare('total', $this->total, true);
        $criteria->compare('status', $this->status, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
