<?php
/**
 * Description of Route
 *
 * @author Администратор
 */
class Direction {
    
    public $from_code;
    
    public $from_name;
    
    public $to_code;
    
    public $to_name;
    
    public $date1;
    
    public $departure_date;
    
    public $arrival_date;
    
    public $back_date;
    
    public $departure_time;
    
    
    /**
     * Генерирует модель на базе элемента DOM
     * @param DOMNode $node <detail></ditail>
     * @return \Direction
     */
    public static function modelByDOMNode(DOMNode $node){
        $xml = new DOMDocument;
        $xml->loadXML($node->ownerDocument->saveXML($node));
        
        $direction = new Direction;
        
        $statonFromNode = $xml->getElementsByTagName('station_from')->item(0);
        $stationToNode = $xml->getElementsByTagName('station_to')->item(0);
        
        $direction->date1 = $xml->getElementsByTagName('date')->item(0)->nodeValue;
        $direction->arrival_date = $xml->getElementsByTagName('arrival_date')->item(0)->nodeValue;
        $direction->departure_date = $xml->getElementsByTagName('departure_date')->item(0)->nodeValue;
        $direction->from_code = $statonFromNode->attributes->getNamedItem('code')->nodeValue;
        $direction->from_name = $statonFromNode->nodeValue;
        $direction->to_code = $stationToNode->attributes->getNamedItem('code')->nodeValue;
        $direction->to_name = $stationToNode->nodeValue;
        
        return $direction;
    }

        /**
     * Меняет направление маршрута 
     */
    public function reverse(){
        $from_code = $this->to_code;
        $from_name = $this->to_name;
        
        $to_code = $this->from_code;
        $to_name = $this->from_name;
        
        $this->from_name = $from_name;
        $this->from_code = $from_code;
        
        $this->to_code = $to_code;
        $this->to_name = $to_name;
    }
    /**
     * Увеличить рейтинг, выбранных станций
     */
    public function vote(){
        Yii::app()->db->createCommand("UPDATE {{stations}} SET rating = rating + 1 WHERE code IN('$this->from_code','$this->to_code')")
                ->execute();
    }
    
}
