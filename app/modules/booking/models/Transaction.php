<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transaction
 *
 * @author Администратор
 */
abstract class Transaction {
    
    public $action;
    
    public function __construct($action) {
        $this->action = $action;
    }


    abstract public function response(DOMDocument $dom);
    
    /**
     * Первичная обработка ответа
     * @param String $source
     * @return DOMDocument
     */
    public function handing($source){
        $dom = new DOMDocument;
        $dom->loadXML($source);
        return $dom;
    }
    
    public function save(){
        //Сохранение транзакции
    }
}
