<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Order
 *
 * @author Администратор
 */
class Order extends BaseOrder {
    
    
    private $_input;
    
    private $_storage;
    
    private $_transport;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterValidate() {
        $this->direction = serialize($this->direction);
        $this->train = serialize($this->train);
        $this->wagon = serialize($this->wagon);
        $this->places = serialize($this->places);
        parent::afterValidate();
    }
    public function afterFind() {
        parent::afterFind();
        $this->_unserialize();
    }

    public function afterSave() {
        parent::afterSave();
        foreach ($this->products as $product) {
            $product->order_id = $this->id;
            if(!$product->save()){
                throw new CDbException(BookingModule::t('errors', 'Не удалось сохранить заказ'), 500);
            }
        }
        $this->_unserialize();
    }

    public function setStorage(IStorageManager $storage){
        $this->_storage = $storage;
    }
    
    public function setTransport(ITransport $t){
        $this->_transport = $t;
    }

    /**
     * Добавить места к заказу
     */
    public function addPlaces($places) {
        $items = array();
        foreach ($places as $number => $place) {
            $p = new Place();
            $p->number = $number;
            $p->firstname = $place['firstname'];
            $p->lastname = $place['lastname'];
            $p->type = $place['type'];
            $p->kind = $place['kind'];
            if (isset($place['luggage'])) {
                $p->luggage = $place['luggage'];
            }
            $items[] = $p;
        }
        $this->places = $items;
    }
    
    /**
     * Получает место по номеру места
     * @param int $number
     * @return Place
     */
    public function getPlace($number){
        foreach ($this->places as $place) {
            if($place->number == $number){
                return $place;
            }
        }
        $place = new Place();
        $place->number = $number;
        return $place;
    }
    
    public function addProduct(Product $p){
        $products = empty($this->products) ? array() : $this->products;
        $products[] = $p;
        $this->total += $p->price;
        $this->products = $products;
    }
    public function addProducts($products){
        foreach ($products as $p) {
            $this->addProduct($p);
        }
    }

    /**
     * Увеличивает стоимость заказа
     * @param decimal $price насколько будет увеличина общая стоимость
     * @return decimal общая стоимость после изменения
     */
    public function addToPrice($price){
        return $this->total += $price;
    }
    
    /**
     * Выполняет оформление заказа используя другие транзакции.
     * reserve, booking, transportation
     */
    public function checkout(){
        foreach ($this->_input as $type => $place){
            $this->_storage->setParam('Places', $place);
            $transaction = TransactionsBuilder::build($type, $this->_storage, $this->_transport);
            $product = $transaction->sing()->submit();
            $this->addProduct($product);
            foreach ($transaction->transportations as $n) {
                $place = $this->getPlace($n);
                if (!$place->isLuggage()) {
                    continue;
                }
                $this->setParam('kind', $place->luggage['type']);
                $this->setParam('uid', $place->uid);
                if (isset($place->luggage['weight'])) {
                    $this->setParam('weight', $place->luggage['weight']);
                }
                $transaction = TransactionsBuilder::build('transportation', $this->_storage, $this->_transport);
                $product = $transaction->sign()->submit();

                $this->addProduct($product);
            }
        }
    }
    protected function _unserialize(){
        $this->direction = unserialize($this->direction);
        $this->train = unserialize($this->train);
        $this->wagon = unserialize($this->wagon);
        $this->places = unserialize($this->places);
    }
    /**
     * Осуществляет покупку заказа. Вызывает метод покупки для каждого
     * продукта в коллекции. 
     * @return Document[] возвращает массив документов
     */
    public function pay(){
        $documents = array();
        foreach($this->products as $product){
            $documents = array_merge($product->pay(), $documents);
        }
        $this->payBehavior->pay();
        return $documents;
    }

    public function defaultScope() {
        return array('condition' => 'status = "new"');
    }

    public function behaviors(){
        return array(
            //pay(), cancel(), error()
            'payBehavior' => array(
                'class' => 'booking.components.PayBehavior'
            )
        );
    }

}
