<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TransportationDocument
 *
 * @author Администратор
 */
class TransportationDocument extends Document{
    
    public $kind;
    
    public $linkUID;
    
    public static function modelByDom(\DOMDocument $dom, $classname = __CLASS__) {
        $document = parent::modelByDom($dom, $classname);
        foreach ($dom->getElementsByTagName('transportation')->item(0)->childNodes as $node) {
            if($node->nodeType != XML_ELEMENT_NODE){
                continue;
            }
            if($node->nodeName == 'kind'){
                $document->kind = $node->nodeValue;
            }
            if($node->nodeName == 'uid'){
                $document->linkUID = $node->nodeValue;
            }
        }
        return $document;
    }

    public function getStringPrice() {
        //$sPrice = 'ВАРТ=<b>7,91ГРН.</b>(ПР.ПЛ.0,76 + ПДВ 1,32 + КЗБ 5,83)';
        $price = $this->prices[0];
        $sPrice = 'ВАРТ=';
        $sPrice .= '<b>'.$price['cost'].'<b/>';
        $sPrice .= '(';
        $sPrice .= 'ПР.ПЛ.'.$price['reserved_seat'];
        $sPrice .= '+ ПДВ '.$price['vat'];
        $sPrice .= '+ КЗБ. '.$price['fee'];
        $sPrice .= ')';
        return $sPrice;
    }

    public function getStringType() {
        return BookingModule::t('app', 'Перевізний документ');
    }

}
