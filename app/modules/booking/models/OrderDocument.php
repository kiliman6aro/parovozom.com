<?php

/**
 * Документ заказа. Данный документ является лишь предзаказом,
 * проеездные документы по этому заказу можно получить в любой Ж/Д кассе
 * Украины. Данный документ оформляется сразу на несколько мест. То есть
 * для нескольких мест один ordernumber
 * @author Павел
 */
class OrderDocument extends Document{
    
    /**
     * @var Place[] список мест 
     */
    public $places = array();
    
    /* @var $type необходимо для автогенерации шаблона */
    public $type = 'travel';
    
    //Пересчитать стоимость всех документов и вывести ИТОГО
    public function getStringPrice() {
        $total = 0.00;
        foreach ($this->places as $p){
            $total += $p->total;
        }
        return $total;
    }

    public function getStringType() {
        return BookingModule::t('app', 'Бланк заказа');
    }
    
    /**
     * На базе переданных документов, создает места Place и добавляет их
     * в коллекцию. 
     * @see TravelDocument
     * @param Document[] $documents
     */
    public function setDocuments($documents){
        $this->train = $documents[0]->train;
        $this->direction = $documents[0]->direction;
        $this->wagon = $documents[0]->wagon;
        $this->arrival_date = $documents[0]->arrival_date;
        $this->departure_date = $documents[0]->departure_date;
        foreach ($documents as $document) {
            $place = new Place();
            $place->firstname = $document->firstname;
            $place->lastname = $document->lastname;
            $place->number = $document->place;
            $place->kind = $document->kind;
            $place->setPrices($document->prices);
            $place->services = $document->services;
            $place->uid = $document->uid;
            $this->setPlace($place);
        }
    }
    
    public function setPlace(Place $p){
        $this->places[] = $p;
    }
}
