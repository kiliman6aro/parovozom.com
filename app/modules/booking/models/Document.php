<?php

/**
 * Базовый класс для определения сущности документ
 * Определяет общий интерфейс для всех типов документов: reserve, travel, transportation
 * @package booking.models
 * @author kiliman6aro@gmail.com
 */
abstract class Document {
    
    const VIEW_ROOT = 'documents';
    
    /**
     * Размер скидки по умолчанию
     */
    const DISCOUNT_SIZE = 10;

    /* @var $electronic является ли данный документ электронным */
    public $electronic;
    
    /**
     * @var bool со скидкой документ? 
     */
    public $discount = false;
    
    /* @var $uid уникальный идентификационный номер */
    public $uid;
    
    /* @var $ficsal_info фиксальная информация */
    public $fiscal_info = array();
    
    /* @var $type тип документа: travel, transportation, reserve */
    public $type;
    
    /* @var $qr_image qr-код в base64 */
    public $qr_image;
    
    /* @var $barcode_image штрих-код в base64 */
    public $barcode_image;
    
    /* @var $ordernumber номер заказа */
    public $ordernumber;
    
    /* @var $paydeate дата и время покупки Y-m-d h:i */
    public $paydate;
    
    /* @var $prices список стоимостей для данного документа */
    public $prices = array();
    
    /* @var $train Train */
    public $train;
    
    /* @var $wagon Wagon */
    public $wagon;
    
    /* @var $direction Direction */
    public $direction;
    
    /* @var $paymentType тип системы оплаты, берется из тега transaction */
    public $paymentType;
    
    /* @var $departure_date Дата и время отправления */
    public $departure_date;
    
    
    /* @var $arrival_date Дата и время прибытия */
    public $arrival_date;
    
    /**
     * @var string Сокращенное название железной дороги
     */
    public $railway;

    /**
     * @return string Возвращает строковое представление цены
     */
    abstract public function getStringPrice();

    /**
     * @return string Возвращает строковое предсталвение имени документа
     */
    abstract public function getStringType();

    
    /**
     * @param string $uid uid в формате передаваемом API
     * @param bool $select выделять ли оставшиееся числа или нет
     * @return string возвращает правильно отформатированный uid документа. 
     */
    public static function UIDtoString($uid, $select = true){
        $strUid = str_replace('-', '', $uid);
        $first = substr($strUid, 0, 8); //первые 8 символов
        $lastCharacters = substr($strUid, 8);
        //последнюю часть строки перевожу в массив по 4 символа
        $second = str_split($lastCharacters, 4);
        
        //формируется необходимого формата страка и выводится
        if($select){
            return $first.'-'.'<span class="uid">'.implode('-', $second).'</span>';
        }
        return $first.'-'.implode('-', $second);
    }

    /**
     * Генерирует документа на базе элемента DOM <document></document>
     * @param DOMDocument $dom <document></document>
     * @param string $classname имя класса
     * @return \Document
     */
    public static function modelByDom(DOMDocument $dom, $classname = __CLASS__){
        
        $document= new $classname;
        $document->train = Train::TrainByDOMNode($dom->getElementsByTagName('train')->item(0));
        $document->wagon = Wagon::modelByDOMNode($dom->getElementsByTagName('wagon')->item(0));
        $document->direction = Direction::modelByDOMNode($dom->getElementsByTagName('detail')->item(0));
        $document->qr_image  = $dom->getElementsByTagName('qr_image')->item(0)->nodeValue;
        $document->barcode_image = $dom->getElementsByTagName('barcode_image')->item(0)->nodeValue;
        $document->type = $dom->getElementsByTagName('type')->item(0)->nodeValue;
        $document->uid = $dom->getElementsByTagName('uid')->item(0)->nodeValue;
        $document->departure_date = $dom->getElementsByTagName('departure_date')->item(0)->nodeValue;
        $document->arrival_date = $dom->getElementsByTagName('arrival_date')->item(0)->nodeValue;
        if($dom->getElementsByTagName('railway')->item(0)){
            $document->railway = $dom->getElementsByTagName('railway')->item(0)->nodeValue;
        }
        
        if($dom->getElementsByTagName('ordernumber')->item(0)){
            $document->ordernumber = $dom->getElementsByTagName('ordernumber')->item(0)->nodeValue;
        }
        
        $transactionNode = $dom->getElementsByTagName('transaction')->item(0);
        $document->paymentType = $transactionNode->attributes->getNamedItem('type')->nodeValue;
        $fiscalNode = $dom->getElementsByTagName('fiscal_info')->item(0);
        
        foreach ($fiscalNode->childNodes as $n) {
            if($n->nodeType != XML_ELEMENT_NODE){
                continue;
            }
            $document->fiscal_info[$n->nodeName] = $n->nodeValue;
        }
        $pricesNode = $dom->getElementsByTagName('costs')->item(0);
        foreach ($pricesNode->childNodes as $n) {
            if ($n->nodeType != XML_ELEMENT_NODE) {
                continue;
            }
            $prices = array();
            foreach ($n->attributes as $attr) {
                $prices[$attr->nodeName] = $attr->nodeValue;
            }
            if($dom->getElementsByTagName('text')->item(0)){
                $prices['text'] = $dom->getElementsByTagName('text')->item(0)->nodeValue;
            }
            $document->prices[] = $prices;
        }
        /**
         * Если документ со скидкой
         */
        if($pricesNode->hasAttributes() && $pricesNode->attributes->getNamedItem('discont')){
            $document->discount = true;
        }
        return $document;
    }
    
    /**
     * @return string дату и время отправления в необходимом формате
     */
    public function getStringDepartureDate(){
        return $this->_dateToTicketFormat($this->departure_date);
    }
    /**
     * @return string дату и время прибытия в необходимом формате
     */
    public function getStringArrivalDate(){
        return $this->_dateToTicketFormat($this->arrival_date);
    }

    /**
     * 
     * @return string дату и время покупки документа, в правильном формате
     */
    public function getStringPayDate(){
        return $this->_dateToTicketFormat($this->paydate);
    }
    /**
     * 
     * @return string Возвращает текстовое представление формы оплаты документа
     */
    public function getStringPaymentType(){
        return BookingModule::t('payment_types', $this->paymentType);
    }

    /**
     * Распечатывает полное название поезда. Берет данные так же из 
     * вагона. Например если он фирменный
     */
    public function getDescriptionTrain(){
        $string = $this->train->departure ? $this->train->departure : $this->train->number;
        if($this->wagon->firm){
            $string .= ' '.BookingModule::t('app', 'firm');
        }
        $string .= ' '.BookingModule::t('train_categories', $this->train->category);
        return $string;
    }
    
    /**
     * Выводит полное описание вагона, вместе с железной
     * дорогой
     */
    public function getDescriptionWagon(){
        $string = $this->wagon->getFullNumber();
        $string .= ' '.$this->railway;
        return $string;
    }
    
    /**
     * Возвращает путь к шаблону документа
     * исходя из его типа, подтипа
     * @return string полный путь к файлу шаблона
     */
    public function getLayoutPath(){
        $viewPath = self::VIEW_ROOT;
        $viewPath .= $this->electronic ? DS.'ppe' : DS.'ppd';
        $viewPath .= '/'.$this->type;
        return $viewPath;
    }

    /**
     * Преобразовывает дату и время в правильный формат
     * для электронного билета
     * @param type $datetime
     */
    private function _dateToTicketFormat($datetime){
        return date("d.m.Y H:i", strtotime($datetime));
    }
}
