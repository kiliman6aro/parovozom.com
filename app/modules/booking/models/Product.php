<?php

/**
 * This is the model class for table "{{product}}".
 *
 * The followings are the available columns in table '{{product}}':
 * @property integer $id
 * @property integer $order_id
 * @property string $type
 * @property string $transaction_id
 * @property string $ordernumber
 * @property string $uio
 * @property integer $electronic
 * @property integer $oldorder
 * @property string $price
 *
 * The followings are the available model relations:
 * @property Order $order
 */
class Product extends CActiveRecord  implements IStorageManager{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Product the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{product}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('order_id, type, transaction_id, uio, price', 'required'),
            array('order_id, oldorder', 'numerical', 'integerOnly' => true),
            array('type', 'length', 'max' => 50),
            array('transaction_id', 'length', 'max' => 32),
            array('ordernumber, uio', 'length', 'max' => 128),
            array('price', 'length', 'max' => 10),
            array('electronic', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, order_id, type, transaction_id, ordernumber, uio, electronic, oldorder, price', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'order_id' => 'Order',
            'type' => 'Type',
            'transaction_id' => 'Transaction',
            'ordernumber' => 'Ordernumber',
            'uio' => 'Uio',
            'electronic' => 'Electronic',
            'oldorder' => 'Oldorder',
            'price' => 'Price',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('order_id', $this->order_id);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('transaction_id', $this->transaction_id, true);
        $criteria->compare('ordernumber', $this->ordernumber, true);
        $criteria->compare('uio', $this->uio, true);
        $criteria->compare('electronic', $this->electronic);
        $criteria->compare('oldorder', $this->oldorder);
        $criteria->compare('price', $this->price, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function getParam($name, $cache = true) {
        if(isset($this->{$name})){
            return $this->{$name};
        }
        return Yii::app()->controller->getParam($name, $cache);
    }

    public function setParam($name, $value) {
        Yii::app()->controller->setParam($name, $value);
    }

    /**
     * Оформляет покупку транзакцией pay. В качестве менеджера данных
     * указывает самого себя, в качестве транспорта, указывает контроллер.
     * @todo Транзакция строится и ей в качестве делегата передается текущий контроллер. Но неизвестно,
     * что это за контроллер. И реализует ли он реально необходимые интерфейсы. Данная модель работоспособна
     * не с каждым контроллером.
     * @return Document[] $documents один продукт может возвращать множенство документов
     */
    public function pay(){
        $transaction = TransactionsBuilder::build('pay', $this, Yii::app()->controller);
        return $transaction->sign()->submit();
    }

}
