<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Train
 *
 * @author Администратор
 */
class Train {
    
    public $number;
    
    public $category;
    
    public $model;
    
    public $station_from_name;
    
    public $station_to_name;
    
    public $station_from_code;
    
    public $station_to_code;
    
    public $class;
    
    public $fasted;
    
    public $departure;
    
    public $departure_date;
    
    public $departure_date_array;
    
    public $arrival_date;
    
    public $arrival_date_array;
    
    public $travel_time;
    
    public $travel_time_array;
    
    
    public static function TrainByDOM(DOMDocument $dom){
        $train = new Train;

        $train->number = $dom->getElementsByTagName('number')->item(0)->nodeValue;
        $train->category = $dom->getElementsByTagName('category')->item(0)->nodeValue;
        $train->model = $dom->getElementsByTagName('model')->item(0)->nodeValue;
        
        $train->class = $dom->getElementsByTagName('class')->item(0)->attributes->getNamedItem('code')->nodeValue;
        $train->fasted = $dom->getElementsByTagName('fasted')->item(0)->attributes->getNamedItem('code')->nodeValue;
        $train->station_from_code = $dom->getElementsByTagName('station_from')->item(0)->attributes->getNamedItem('code')->nodeValue;
        $train->station_from_name = $dom->getElementsByTagName('station_from')->item(0)->nodeValue;
        $train->station_to_name = $dom->getElementsByTagName('station_to')->item(0)->nodeValue;
        $train->station_to_code = $dom->getElementsByTagName('station_to')->item(0)->attributes->getNamedItem('code')->nodeValue;
        $train->departure_date = $dom->getElementsByTagName('departure_date')->item(0)->nodeValue;
        $train->arrival_date = $dom->getElementsByTagName('arrival_date')->item(0)->nodeValue;
        $train->travel_time = $dom->getElementsByTagName('travel_time')->item(0)->nodeValue;
        
        $train->departure_date_array = ResponseDateTimeFormater::dateTimeToArray($train->departure_date);
        $train->arrival_date_array = ResponseDateTimeFormater::dateTimeToArray($train->arrival_date);
        $train->travel_time_array = ResponseDateTimeFormater::timeToArray($train->travel_time);
        return $train;
    }
    
    /**
     * Генериуер объект Train из DOOMNode, если параметры
     * поезда пришли в одному теге, а не нескольих
     * @param DOMNode $node
     * @return Train
     */
    public static function TrainByDOMNode(DOMNode $node){
        $attributes = $node->attributes;
        $train = new Train();
        $train->departure = $attributes->getNamedItem('departure') ? $attributes->getNamedItem('departure')->nodeValue : null;
        $train->number = $node->nodeValue;
        $train->category = $attributes->getNamedItem('category') ? $attributes->getNamedItem('category')->nodeValue : null;
        $train->model = $attributes->getNamedItem('model') ? $attributes->getNamedItem('model')->nodeValue : null;
        $train->class = $attributes->getNamedItem('class') ? $attributes->getNamedItem('class')->nodeValue : null;
        $train->fasted = $attributes->getNamedItem('class') ? $attributes->getNamedItem('fasted')->nodeValue : null;
        return $train;
    }

        /**
     * Проверяет можно ли оформлять предкахаз
     * на данный рейс. Для оформления предзаказа, 
     * отправление рейса должно быть за сутки, от текущей
     * даты
     * @return bool
     */
    public function isBooking() {
        $date1 = new DateTime($this->departure_date);
        $date2 = new DateTime('now');
        $interval = date_diff($date2, $date1);
        return $interval->days > 0 ? true : false;
    }

}
