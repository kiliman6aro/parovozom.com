<?php

/**
 * This is the model class for table "{{transactions_log}}".
 *
 * The followings are the available columns in table '{{transactions_log}}':
 * @property integer $id
 * @property string $transaction_id
 * @property string $url
 * @property string $action
 * @property string $request
 * @property string $response
 * @property string $status
 * @property string $create_time
 * @property string $error
 */
class TransactionsLog extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TransactionsLog the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    /**
     * Формирует и возвращает модель логирования
     * на базе транзакции
     * @param BaseTransaction $transaction
     * @return \TransactionsLog
     */
    public static function modelByTransaction(BaseTransaction $transaction){
        $log = new TransactionsLog;
        $log->transaction_id = $transaction->getID();
        $log->action = $transaction->getAction();
        $log->request = $transaction->getRequest();
        $log->response = $transaction->getResponse();
        if($transaction->hasError()){
            $log->status = 'error';
            $log->error = serialize($transaction->getError());
        }
        return $log;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{transactions_log}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('transaction_id, url, action, request', 'required'),
            array('transaction_id', 'length', 'max' => 32),
            array('url', 'length', 'max' => 255),
            array('action', 'length', 'max' => 50),
            array('status', 'length', 'max' => 7),
            array('response, create_time, error', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, transaction_id, url, action, request, response, status, create_time, error', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'transaction_id' => 'Transaction',
            'url' => 'Url',
            'action' => 'Action',
            'request' => 'Request',
            'response' => 'Response',
            'status' => 'Status',
            'create_time' => 'Create Time',
            'error' => 'Error',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('transaction_id', $this->transaction_id, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('action', $this->action, true);
        $criteria->compare('request', $this->request, true);
        $criteria->compare('response', $this->response, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('error', $this->error, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
