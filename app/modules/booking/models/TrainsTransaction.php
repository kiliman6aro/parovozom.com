<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TrainsTransaction
 *
 * @author Администратор
 */
class TrainsTransaction extends Transaction{
    
    public function response(DOMDocument $dom) {
        $routes = array();
        //Получить поезда
        foreach ($dom->getElementsByTagName('train') as $node) {
            $xml = new DOMDocument;
            $xml->loadXML($dom->saveXML($node));
            foreach ($xml->getElementsByTagName('wagon') as $wagonNode) {
                $route = new Route();
                $route->train = Train::TrainByDOM($xml);
                $route->place_count = $wagonNode->nodeValue;
                $route->wagon_type = $wagonNode->attributes->getNamedItem('type')->nodeValue;
                $routes[] = $route;
            }
        }
        return $routes;
    }

}
