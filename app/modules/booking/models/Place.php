<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Place
 *
 * @author Администратор
 */
class Place {

    public $number;
    public $firstname;
    public $lastname;
    public $type;
    public $kind;
    public $luggage;
    public $services = array();
    public $booking = array();
    public $total = 0.00;
    public $uid;
    
    /**
     *
     * @var Array Перечень всех цен, задается в момент покупки документа
     */
    public $prices = array();
    
    public $luggagePrice = 0.00;

    /**
     * Проверяет присутствие каких либо сервисов, на данный билет
     * @return bool
     */
    public function isServices() {
        return !empty($this->services);
    }
    
    /**
     * Проверяет не является ли данное местро
     * бронью
     * @return bool
     */
    public function isBooking(){
        return !empty($this->booking);
    }
    
    /**
     * Возвращает дату и время, до которого необходимо выкупить
     * бронь
     * @return string
     */
    public function getBookingDate(){
        return $this->booking['date'];
    }
    /**
     * Возвращает стоимость бронированного
     * билета
     * @return decimal
     */
    public function getBookingPrice(){
        return $this->booking['cost'];
    }

    /**
     * Проверяет присутствует ли заказ на транспортировку
     * @return bool
     */
    public function isLuggage() {
        return !empty($this->luggage);
    }
    /**
     * Задает общую стомость по всем сервисам
     * @param decimal $price
     */
    public function setServicesPrice($price){
        $this->serivicePrice = $price;
    }

    /**
     * Задает стомость транспортировки
     * @param decimal $price
     */
    public function setLuggagePrice($price){
        $this->total += $price;
        $this->luggagePrice = BookingModule::calculatePrice($price, 'luggage');
    }
    
    /**
     * 
     * @param Array $prices массив с ценами
     */
    public function setPrices($prices){
        foreach ($prices as $item) {
            $this->total += BookingModule::calculatePrice($item['cost']);
        }
        $this->prices = $prices;
    }

    /**
     * Возвращает описание багажа
     */
    public function getLuggage() {
        $note = BookingModule::t('kinds', $this->luggage['type']);
        return $note .= !empty($this->luggage['weight']) ? ' ('.$this->luggage['weight'].' кг.)' : '';
    }

    /**
     * Возвращает стоимость транспортировки
     * @return decimal
     */
    public function getLuggagePrice() {
        return $this->luggagePrice;
    }

    /**
     * Добавляет сервис в коллекцию
     * @param Service $s
     */
    public function addService(Service $s) {
        $this->services[] = $s;
    }

    /**
     * Добавляет сразу коллекцию сервисов
     * @param Service[] $services
     */
    public function addServices($services) {
        foreach ($services as $s) {
            $this->addService($s);
        }
    }

    /*
     * Возвращает список сервисов через запятую
     */

    public function getServices() {
        $names = array();
        foreach ($this->services as $s){
            $names[] = $s->name;
        }
        return implode('+', $names);
    }

    /**
     * Возвращает общую стоимость сервисов
     * @return type
     */
    public function getServicesPrice() {
        $price = 0.00;
        foreach ($this->services as $s) {
            $price += $s->price;
        }
        return $price;
    }
    
    /**
     * @return String полное имя
     */
    public function getName(){
        return $this->firstname.' '.$this->lastname;
    }


    /**
     * Возвращает строкове представление типа билета
     * может быть: детский, предзаказ, или полный билет
     * под выкуп
     * @return String
     */
    public function getType(){
        if($this->type == 'booking'){
            return BookingModule::t('app', 'Предзаказ');
        }
        if($this->kind == 'child'){
            return BookingModule::t('app', 'Детский');
        }
        return BookingModule::t('app', 'Полный');
    }

}
