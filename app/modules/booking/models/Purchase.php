<?php

/**
 * @todo данная модель может бы получена только, если с момента
 * оформления прошло менее 30 минут.
 *
 * @author Администратор
 */
class Purchase extends BasePurchase {
    
    public $documents = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            //render(), verify()
            'cartBehavior' => array(
                'class' => 'booking.components.plategka.CartBehavior',
                'keysPath' => Yii::getPathOfAlias(Settings::Get('data', 'booking')).DS.'keys'.DS.'plategka',
                'merchant_id' => Settings::Get('merchant_id', 'payment'),
                'encode_key' => Settings::Get('encode_key', 'payment'),
                'decode_key' => Settings::Get('decode_key', 'payment'),
                'url' => Settings::Get('uri', 'payment')
            ),
            //pay(), cancel(), error()
            'payBehavior' => array(
                'class' => 'booking.components.PayBehavior'
            )
        );
    }

    public function pay() {
        foreach ($this->orders as $order) {
            $this->addDocuments($order->pay());
        }
        $this->payBehavior->pay();
        $this->save();
        return $this->documents;
    }

    public function cancel() {
        foreach ($this->orders as $order) {
            $order->cancel();
        }
        $this->payBehavior->cancel();
    }

    public function error() {
        foreach ($this->orders as $order) {
            $order->error();
        }
        $this->payBehavior->error();
    }

    /**
     * Добавляет заказ в корзину
     * @param Order $o
     */
    public function addOrder(Order $o) {
        $this->total += $o->total;
        $orders = empty($this->orders) ? array() : $this->orders;
        $orders[] = $o;
        $this->orders = $orders;
    }
    
    /**
     * Добавляет массив заказов
     * @param Order[] $orders
     */
    public function addOrders($orders){
        foreach ($orders as $o) {
            $this->addOrder($o);
        }
    }
    
    /**
     * Добавляет документ в коллекцию
     * @param Document $doc
     */
    public function addDocument(Document $doc){
        $this->documents[] = $doc;
    }
    /**
     * Добалвяет сразу массив документов
     * @param Document[] $documents
     */
    public function addDocuments($documents){
        foreach ($documents as $document) {
            $this->addDocument($document);
        }
    }

    /**
     * Возвращает списоку документов. Если сериализованы
     * то рассерилизовывает и возращает массив документов
     * @return Document[] 
     */
    public function getDocuments(){
        if(!$this->documents)
            return null;
        
        if(is_array($this->documents))
            return $this->documents;
        
        return unserialize($this->documents);
    }

    public function scopes() {
        return array(
            'complete' => array('condition' => 'status = "complete"'),
            'new' => array('condition' => 'status = "new"')
        );
    }

    protected function beforeValidate() {
        $this->documents = serialize($this->documents);
        return parent::beforeValidate();
    }
    
    protected function afterFind() {
        parent::afterFind();
        $this->documents = unserialize($this->documents);
    }

    protected function afterSave() {
        parent::afterSave();
        foreach ($this->orders as $order) {
            $order->purchase_id = $this->id;
            if(!$order->save()){
                throw new CDbException(BookingModule::t('errors', 'Не удалось сохранить заказ'), 500);
            }
        }
        Yii::app()->session['purchase_id'] = $this->id;
    }

}
