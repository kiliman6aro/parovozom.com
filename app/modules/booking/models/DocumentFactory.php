<?php
/**
 * Фабрика документов, генерирующихся на базе элемента DOMDocument.
 * Генерирует документа на основании его типа и возвращает.
 */
class DocumentFactory {
    public static function load(DOMNode $node){
        $dom = new DOMDocument;
        $dom->loadXML($node->ownerDocument->saveXML($node));
        switch ($dom->getElementsByTagName('type')->item(0)->nodeValue){
            case 'travel':
                return TravelDocument::modelByDom($dom);
            case 'transportation':
                return TransportationDocument::modelByDom($dom);
            case 'reserve':
                return BookingDocument::modelByDom($dom);
        }
    }
}
