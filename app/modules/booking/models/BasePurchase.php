<?php

/**
 * This is the model class for table "{{purchase}}".
 *
 * The followings are the available columns in table '{{purchase}}':
 * @property integer $id
 * @property string $total
 * @property string $create_time
 * @property string $status
 * @property string $pay_time
 * @property integer $user_id
 * @property string $documents
 *
 * The followings are the available model relations:
 * @property Order[] $orders
 * @property Users $user
 */
class BasePurchase extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return BasePurchase the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{purchase}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id', 'numerical', 'integerOnly' => true),
            array('total', 'length', 'max' => 10),
            array('status', 'length', 'max' => 8),
            array('create_time, pay_time, documents', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, total, create_time, status, pay_time, user_id, documents', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'orders' => array(self::HAS_MANY, 'Order', 'purchase_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'total' => 'Total',
            'create_time' => 'Create Time',
            'status' => 'Status',
            'pay_time' => 'Pay Time',
            'user_id' => 'User',
            'documents' => 'Documents',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('total', $this->total, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('pay_time', $this->pay_time, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('documents', $this->documents, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
