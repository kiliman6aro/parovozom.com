<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Wagon
 *
 * @author Администратор
 */
class Wagon {
    
    public $number;
    
    public $train;
    
    public $class_code;
    
    public $class;
    
    public $type;
    
    public $type_code;
    
    public $places;

    public $cost;
    
    public $sitting = false;
    
    public $firm;


    public $currency;
    
    public $prices = array();
    
    public $services = array();
    
    
    /**
     * Генерирует модель Wagon на базе DOMNode
     * <wagon type="П" class="Б" firm="True">05</wagon>
     * @param DOMNode $node
     * @return \Wagon
     */
    public static function modelByDOMNode(DOMNode $node){
        //<wagon type="П" class="Б" firm="True">05</wagon>
        $wagon = new Wagon();
        $attributes = $node->attributes;
        $wagon->type_code = $attributes->getNamedItem('type') ? $attributes->getNamedItem('type')->nodeValue : null;
        $wagon->class_code = $attributes->getNamedItem('class') ? $attributes->getNamedItem('class')->nodeValue : null;
        if($attributes->getNamedItem('firm')){
            $wagon->firm = (String::toLower($attributes->getNamedItem('firm')->nodeValue) == 'true') ? true : false;
        }
        $wagon->number = $node->nodeValue;
        return $wagon;
    }

    /**
     * Возвращает массив доступных сервисов в данном вагоне.
     * Если вагоня не сидячий, то добавляет постельное белье
     * @return Array
     */
    public function getServices(){
        $items = array();
        if(!$this->sitting){
            $items['П'] = 'Постельное белье';
        }
        foreach ($this->services as $service) {
            $items[$service->code] = $service->name;
        }
        return $items;
    }
    
    public function getFullNumber(){
        $desc = $this->number.' '.$this->type_code;
        if($this->class_code){
            $desc .= '/'.$this->class_code;
        }
        return $desc;
    }
    
}
