<?php

/**
 * Description of TravelDocument
 *
 * @author Администратор
 */
class TravelDocument extends Document {

    public $firstname;
    public $lastname;
    public $kind;
    public $birthday;
    public $place;
    public $services = array();

    public function getStringPrice() {
        $price = $this->prices[0];
        $strPrice = 'ВАРТ=<b>'.$price['cost'].'ГРН</b>';
        $strPrice .= '(';
        $strPrice .= 'КВ.'.$price['ticket'];
        $strPrice .= '+ПЛ.'.$price['reserved_seat'];
        if(isset($price['service'])){
            $strPrice .= '+СП.'.$price['service'];
        }
        $strPrice .= '+ПДВ.'.$price['vat'];
        $strPrice .= '+СТР.'.$price['insurance'];
        $strPrice .= '+КЗБ.'.($price['commission'] + $price['fee']);
        $strPrice .= ')';
        $strPrice .= '<br/>';
        if(!empty($this->services)){
            $strPrice .= String::substr($price['text'], 'В Т.Ч.', ')').')</br>';
        }
        if($this->discount){
            $strPrice .= sprintf('УЗШК %s%% ЗНИЖКА<br/>',self::DISCOUNT_SIZE);
        }
        $strPrice .= '#'.String::substr($price['text'], 'СТР.ВІД');
        return $strPrice;
    }

    public function getStringTrain() {
        return $this->train->departure;
    }

    public function getStringType() {
        return BookingModule::t('app', 'Посадочный документ');
    }
    
    public function getName(){
        return $this->lastname.' '.$this->firstname;
    }
    
    public function getServices(){
        $names = array();
        foreach ($this->services as $s){
            $names[] = $s->name;
        }
        return implode(',', $names);
    }

    public static function modelByDom(\DOMDocument $dom, $classname = __CLASS__) {
        $document = parent::modelByDom($dom, $classname);
        $document->firstname = $dom->getElementsByTagName('firstname')->item(0)->nodeValue;
        $document->lastname = $dom->getElementsByTagName('lastname')->item(0)->nodeValue;
        $document->kind = $dom->getElementsByTagName('kind')->item(0)->nodeValue;
        $document->place = $dom->getElementsByTagName('places')->item(0)->nodeValue;
        $childs = $dom->getElementsByTagName('services')->item(0)->childNodes;
        foreach ($childs as $item) {
            if($item->nodeType != XML_ELEMENT_NODE){
                continue;
            }
            $s = new Service();
            $s->code = $item->attributes->getNamedItem('code')->nodeValue;
            $s->name = $item->nodeValue;
            $s->price = $item->attributes->getNamedItem('cost')->nodeValue;
            $document->services[] = $s;
        }
        return $document;
    }
}
