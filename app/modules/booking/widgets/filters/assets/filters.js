$('.section[name="trains"]').find('input').on('change', function () {
    var item = $(this);
    var train = item.val();
    if (item.prop('checked')) {
        $('.result_item[train="' + train + '"]').show();
    } else {
        $('.result_item[train="' + train + '"]').hide();
    }
    var thisSection = $('.section[name="trains"]').find('input');
    $('.section').find('input').not(thisSection).prop('checked', true);
});
$('.section[name="types"]').find('input').on('change', function () {
    var item = $(this);
    var type = item.val();
    if (item.prop('checked')) {
        $('.result_item[wagon_type="' + type + '"]').show();
    } else {
        $('.result_item[wagon_type="' + type + '"]').hide();
    }
    var thisSection = $('.section[name="types"]').find('input');
    $('.section').find('input').not(thisSection).prop('checked', true);
});
$('.section[name="shelf"]').find('input').on('change', function () {
    var thisSection = $('.section[name="shelf"]').find('input');
    $('.section').find('input').not(thisSection).prop('checked', true);
    var item = $(this);
    var type = item.val();
    if (item.prop('checked')) {
        $('.result_item[' + type + '=true]').show();
        $('.result_item[' + type + '=false]').hide();
    } else {
        $('.result_item[' + type + '=false]').hide();
        $('.result_item[' + type + '=true]').show();
    }

});

//ui range sliders
$("#Slider4").slider({
    from: 1,
    to: 48,
//        heterogeneity: ['50/5', '75/15'],
    scale: [1, 48],
    limits: false,
    step: 1,
    dimension: ' ч',
    onstatechange: function (value) {
        var all = $('.result_item').show();
        var times = value.split(';');
        var all = $('.result_item');
        var items = all.map(function(){
            var travel_time = $(this).attr('travel_time');
            if(travel_time <= parseInt(times[0], 10) || travel_time >= parseInt(times[1], 10)){
                return this;
            }
        });
        $(items).hide();
    }
});

$("#Slider5").slider({
    from: 0,
    to: 1440,
    step: 5,
    dimension: '',
    scale: ['0:00', '24:00'],
    limits: false, calculate: function (value) {
        var hours = Math.floor(value / 60);
        var mins = (value - hours * 60);
        return (hours < 10 ? "0" + hours : hours) + ":" + (mins == 0 ? "00" : mins);
    },
      onstatechange: function (value) {
        var times = value.split(';');
        var all = $('.result_item').show();
        var items = all.map(function(){
            var direction = $('.section[name=travel]').find('input:checked').val();
            var departure_time = $(this).attr(direction);
            if(departure_time <= parseInt(times[0], 10) || departure_time >= parseInt(times[1], 10)){
                return this;
            }
        });
        $(items).hide();
    }
});
//Кнопки показать все, или только один элемент
$('.section').on('click', 'a', function (ev) {
    var $clickedSection = $(ev.delegateTarget),
            $clickedInput = $(this).closest('.field').find('input');
    $clickedInput.prop('checked', true);
    $clickedSection.find('input').not($clickedInput).prop('checked', false);
    $clickedSection.find('input').trigger('change');
    return false;
});


$(".show_all").click(function () {
    var items = $(this).parent().parent().find("input:checkbox");
    items.prop('checked', true);
    items.trigger('change');
});