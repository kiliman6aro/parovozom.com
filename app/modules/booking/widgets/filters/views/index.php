<form action="">
    <fieldset class="section" name="trains">
        <h4>Поезда
            <span class="show_all">показать все</span>
        </h4>
        <?php foreach ($this->trains as $train): ?>
        <p class="field">
            <input type="checkbox" value="<?php echo $train; ?>" id="<?php echo $train; ?>" class="checkbox" checked="checked" name="type"/>
            <label for="<?php echo $train; ?>"><?php echo $train; ?></label>
            <a href="javascript:void(0);" class="filterOptionOnly">только</a>
        </p>
        <?php endforeach; ?>
    </fieldset>
    <fieldset class="section" name="types">
        <h4>Вагоны <span class="show_all">показать все</span></h4>
        <?php foreach ($this->types as $type): ?>
        <p class="field">
            <input type="checkbox" value="<?php echo $type ?>" id="<?php echo $type ?>" class="checkbox" checked="checked" name="type"/>
            <label for="<?php echo $type ?>"><?php echo BookingModule::t('wagon_types', $type); ?></label>
            <a href="javascript:void(0);" class="filterOptionOnly">только</a>
        </p>
        <?php endforeach; ?>
    </fieldset>
    <fieldset class="section">
        <h4>Полки <span class="show_all">показать все</span></h4>
        <p class="field">
            <input type="checkbox" value="" id="5" checked="checked" name="shelves"/>
            <label for="5">Верхние</label>
            <a href="javascript:void(0);" class="filterOptionOnly">только</a>
        </p>
        <p class="field">
            <input type="checkbox" value="" id="6" checked="checked" ame="shelves"/>
            <label for="6">Нижние</label>
            <a href="javascript:void(0);" class="filterOptionOnly">только</a>
        </p>
    </fieldset>
    <fieldset class="section" name="travel">
        <h4>Время</h4>
        <table class="choice_tbl_field">
            <tr>
                <td>
                    <p>
                        <input type="radio" value="departure_time" id="arrival" name="time" checked="checked"/>
                        <label for="arrival"><?php echo BookingModule::t('app', 'Отправления') ?></label>
                    </p>
                </td>
                <td>
                    <p>
                        <input type="radio" value="arrival_time" id="departure" name="time"/>
                        <label for="departure"><?php echo BookingModule::t('app', 'Прибытия') ?></label>
                    </p>
                </td>
            </tr>
        </table>
        <div class="layout-slider">
            <input id="Slider5" type="slider" name="area" value="0;1440"/>
        </div>
    </fieldset>
    <fieldset>
        <h4>Время в пути</h4>
        <div class="layout-slider">
            <input id="Slider4" type="slider" name="area" value="1;48"/> 
        </div>
    </fieldset>
</form>