<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Filter
 *
 * @author Администратор
 */
class Filter extends CWidget {

    public $routes = array();
    
    public $trains = array();
    
    public $types = array();
    
    public $assets;


    public function init() {
        $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets', false, -1, YII_DEBUG);
    }

    public function run() {
        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile($this->assets . '/filters.js', CClientScript::POS_END);
        foreach ($this->routes as $route) {
            $this->trains[] = $route->train->number;
            $this->types[] = $route->wagon_type;
            $this->types = array_unique($this->types);
            $this->trains = array_values(array_unique($this->trains));
        }
        if (Settings::Get('autoload_price', 'booking')) {
            $cs = Yii::app()->clientScript;
            $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/media/js/jquery.sortElements.js', CClientScript::POS_HEAD);
            $cs->registerScript('autoload', 'Purchase.prices(' . CJSON::encode($this->trains) . ');', CClientScript::POS_END);
            $cs->registerScript('sortble', "$('#price_sort').on('change', function() {
                var val = $(this).val();
                if (val === 'up') {
                    $('.result_item').sortElements(function(a, b) {
                        return parseFloat($(a).attr('price')) > parseFloat($(b).attr('price')) ? 1 : -1;
                    });
                    return;
                }
                $('.result_item').sortElements(function(a, b) {
                    return parseFloat($(a).attr('price')) < parseFloat($(b).attr('price')) ? 1 : -1;
                });
            });", CClientScript::POS_END);
        }
        $this->render('index');
    }

}
