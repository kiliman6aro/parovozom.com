<?php
/**
 * Description of StationsController
 *
 * @author Pavel Bhilick
 */
class StationsController extends CController{
    
    public function filters() {
        return array(
            'ajaxOnly + search, rating'
        );
    }
    
    /**
     * Возвращает список станций и сортирует их по
     * частоте использования
     * @param type $q
     */
    public function actionSearch($q){  
        $reader = Yii::app()->db->createCommand()
            ->select('s.code, s.'.$this->getFieldName().' AS name')
            ->from('{{stations}} s')
            ->where('name_uk LIKE :q OR name_ru LIKE :q OR name_lt LIKE :q', array(':q'=>$q.'%'))
            ->order('rating desc')
            ->limit(5);
        echo CJSON::encode($reader->queryAll());
    }

    /**
     * Возвращает имя поля, которое нужно вернуть, в зависимости
     * от языка системы
     * @return type
     */
    private function getFieldName(){
        $lang = !Yii::app()->language ? strtoupper(Settings::Get('lang', 'booking')) : strtoupper(Yii::app()->language);
        return'name_'.strtolower($lang);
    }
}
