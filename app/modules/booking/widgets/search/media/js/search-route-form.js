(function() {
    $(".place_inp").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "/stations/search",
                data: {q: request.term},
                dataType: "json",
                success: function(data) {
                    response($.map(data, function(item) {
                        return {
                            label: item.name,
                            value: item.name,
                            code: item.code
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            var select = $(this).attr('for');
            var code_station = $('#' + select);
            code_station.val(ui.item.code);
            code_station.trigger('change');
        }
    });
})();
var d = {
    closeText: 'Закрыть',
    prevText: '&#x3c;Пред',
    nextText: 'След&#x3e;',
    currentText: 'Сегодня',
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
        'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
        'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
    dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
    dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false
};
$.datepicker.setDefaults(d);
var showAdditionalButton2 = function(input) {
    setTimeout(function() {
        var buttonPanel = $(input).datepicker("widget").find(".ui-datepicker-buttonpane"),
                btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Сегодня</button>'),
                btn1 = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Завтра</button>'),
                btn2 = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Послезавтра</button>');

        btn.appendTo(buttonPanel);
        btn1.appendTo(buttonPanel);
        btn2.appendTo(buttonPanel);

        btn1.click(function() {
            var currentDate = new Date();
            var nextDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 1);
            $('#datepicker_2').datepicker("setDate", nextDate);
            $('#datepicker_2').datepicker("hide");
            $('.submit_btn').focus();
        });

        btn2.click(function() {
            var currentDate = new Date();
            var nextDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 2);
            $('#datepicker_2').datepicker("setDate", nextDate);
            $('#datepicker_2').datepicker("hide");
            $('.submit_btn').focus();
        });

        btn.click(function() {
            var currentDate = new Date();
            $('#datepicker_2').datepicker("setDate", currentDate);
            $('#datepicker_2').datepicker("hide");
            $('.submit_btn').focus();

        });
    }, 1);
};
$("#datepicker_2").datepicker({
    showButtonPanel: true,
    currentText: "",
    numberOfMonths: 2,
    beforeShow: showAdditionalButton2,
    onChangeMonthYear: showAdditionalButton
});
var showAdditionalButton = function(input) {
    setTimeout(function() {
        var buttonPanel = $(input).datepicker("widget").find(".ui-datepicker-buttonpane"),
                btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Сегодня</button>'),
                btn1 = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Завтра</button>'),
                btn2 = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Послезавтра</button>');

        btn.appendTo(buttonPanel);
        btn1.appendTo(buttonPanel);
        btn2.appendTo(buttonPanel);

        btn1.click(function() {
            var currentDate = new Date();
            var nextDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 1);
            $('#startDate').datepicker("setDate", nextDate);
            $('#startDate').datepicker("hide").next().focus();

        });

        btn2.click(function() {
            var currentDate = new Date();
            var nextDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 2);
            $('#startDate').datepicker("setDate", nextDate);
            $('#startDate').datepicker("hide").next().focus();
        });

        btn.click(function() {
            var currentDate = new Date();
            $('#startDate').datepicker("setDate", currentDate);
            $('#startDate').datepicker("hide").next().focus();

        });
    }, 1);
};
$("#startDate").datepicker({
    showButtonPanel: true,
    currentText: "",
    numberOfMonths: 2,
    beforeShow: showAdditionalButton,
    onSelect: function(dateText) {
        $('#startDate').trigger('change');
    },
    onChangeMonthYear: showAdditionalButton
});
function submitSearchRoute() {
    var form = $('form[name=trains]');
    var from = form.find('#from').val();
    var to = form.find('#to').val();
    var date = form.find('#startDate').val();
    if (from && to && date) {
        form.submit();
    }
}