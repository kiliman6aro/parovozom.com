<div class="switch_links_block">
    <span class="link" style=""><?php echo Yii::t('app', 'Advanced Search'); ?></span>
    <span class="link_open"><?php echo Yii::t('app', 'Hide advanced search'); ?></span>
</div>
<div class="additional_search">
    <div id="radio" class="choice_block">
        <input type="radio" id="radio1" name="radio"/>
        <label for="radio1"><?php echo Yii::t('app', 'Exact date'); ?></label>
        <input type="radio" id="radio2" name="radio" checked="checked"/>
        <label for="radio2" class="middle_btn">+ / - <?php echo Yii::t('app', '{n} days', array('{n}' => 3)); ?></label>
        <input type="radio" id="radio3" name="radio"/>
        <label for="radio3"><?php echo Yii::t('app', 'Holidays'); ?></label>
    </div>
    <div class="additional_block">
        <input type="text" id="datepicker_2" class="date_input" placeholder="<?php echo Yii::t('app', 'Departure Date'); ?>"/>
        <select id="departure_time" class="departure_time">
            <option>00:00</option>
            <?php for ($i = 1; $i <= 24; $i++): ?>
                <option><?php echo $i ?>:00</option>
            <?php endfor; ?>
        </select>
    </div>
</div>