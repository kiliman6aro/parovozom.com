<form action="<?php echo Yii::app()->createUrl('trains'); ?>" method="get" name="trains">
    <div class="main_search">
        <span class="wrap_inp">
            <input type="text" class="from_input place_inp" id="from_name" for="from" placeholder="<?php echo BookingModule::t('app', 'Откуда'); ?>" name="from_name" value="<?php echo !$this->direction->from_name ? '' : $this->direction->from_name; ?>" />
        </span>
        <span class="wrap_inp">
            <input type="text" class="in_input place_inp" id="to_name" for="to" placeholder="<?php echo BookingModule::t('app', 'Куда'); ?>" name="to_name" value="<?php echo !$this->direction->to_name ? '' : $this->direction->to_name; ?>" />
        </span>
        <input type="text" id="startDate" class="date_input" placeholder="<?php echo BookingModule::t('app', 'Дата отправления'); ?>" name="startDate" id="startDate" value="<?php echo !$this->direction->date1 ? '' : $this->direction->date1; ?>" />
        <a href="javascript:void(0);" onclick="submitSearchRoute();" class="submit_btn">
        </a>
        <input type="hidden" id="from" name="from" value="<?php echo !$this->direction->from_code ? '' : $this->direction->from_code; ?>"/>
        <input type="hidden" id="to" name="to" value="<?php echo !$this->direction->to_code ? '' : $this->direction->to_code; ?>"/>
    </div>
    <div class="advanced_search">
        <?php echo !$this->advanced ? '' : $this->render('advanced', null, true); ?>
    </div>
</form>