<?php
class SearchForm extends CWidget{
    
    /**
     * @var type детальный поиск: включен/выключен
     */
    public $advanced = false;
    
    public $direction;
    
    protected $assets;
    
    //@todo надо сделать так что бы получение и запись
    //direction была в едином стиле, как в контроллерах
    //так и в виджетах
    public function init() {
        $this->assets = Yii::app()->assetManager->publish(Yii::getPathOfAlias('booking.widgets.search.media.js'), false, -1, YII_DEBUG);
        $this->direction = new Direction;
        if(isset(Yii::app()->session['direction'])){
            $this->direction = Yii::app()->session['direction'];
        }
        
    }


    public function run(){
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerScriptFile($this->assets.'/search-route-form.js', CClientScript::POS_END);
        /*if(YII_DEBUG){
             Yii::app()->clientScript->registerScriptFile($this->assets.'/search-route-form.js', CClientScript::POS_END);
        }else{
             Yii::app()->clientScript->registerScriptFile($this->assets.'/search-route-form.min.js', CClientScript::POS_END);
        }*/
       
        $this->render('index');
    }
}

