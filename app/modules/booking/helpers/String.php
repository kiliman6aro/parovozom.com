<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MBString
 *
 * @author Павел
 */
class String {

    public static function mb_ucfirst($string, $enc = 'utf-8') {
        $str = mb_strtolower($string, $enc);
        return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc) . mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }
    public static function toUpper($string){
        return mb_convert_case($string, MB_CASE_UPPER, 'UTF-8');
    }
    public static function toLower($string){
        return mb_convert_case($string, MB_CASE_LOWER, 'UTF-8');
    }
    public static function toTitle($string){
        return mb_convert_case($string, MB_CASE_TITLE, 'UTF-8');
    }
    /**
     * Вытягивает подстроку, по символам, а не по номерам
     * Если параметр $end пустой, то строка будет вырезана
     * до самого конца
     * @param string $string Строка из которой будет вырезана подстрока
     * @param string $start подстрока с которой начинать искать
     * @param type $end подсктрока которой заканчивать
     * @return string подстрока, которая будет вырезана из общейстроки
     */
    public static function substr($string, $start, $end = null){
        if(!$end){
            return substr($string, strpos($string, $start));
        }
        $after = substr($string, strpos($string, $start));
        return substr($after, 0, strpos($after, $end));
    }
}
