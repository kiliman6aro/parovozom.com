<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ResponseDateTimeFormater
 *
 * @author Pavel Bhilick
 */
class ResponseDateTimeFormater {
    
    /**
     * Преобразует дату и время ответа, в массив
     * год, месяц, день, часы, минута и секунда
     * @param string $datetime 2014-07-15 00:01:00
     */
    public static function dateTimeToArray($datetime){
        $slice = explode(' ', $datetime);
        $date = explode('-', $slice[0]);
        $time = explode(':', $slice[1]);
        return array(
            'y' => $date[0],
            'm' => $date[1],
            'd' => $date[2],
            'h' => $time[0],
            'i' => $time[1],
            's' => $time[2]
        );
    }
    public static function timeToArray($time){
        $slice = explode(':', $time);
        return array(
            'h' => intval($slice[0]),
            'i' => $slice[1]
        );
    }
}
