<?xml version="1.0" encoding="utf-8"?>
<request>
    <datetime>2015-08-10 16:49:50</datetime>
    <merchant_id>10212</merchant_id>
    <language>ru</language>
    <xmlsign>gkcDhw7tuRq+goo/LPFTSgC7ZMuXUd6d5Tr/hF54Z5ENn4lxD3ceC58KwngRvMrtEj4BeFToGIEm+i7RWJshXYzfV/GMTxDaOOH3QRGy/8XrFtGyl/ZTWQH5zTOGSPSxCitXb8VkA+on6EIknKB3AE7P5XBw7CXAuLzG+O6Dwdo=</xmlsign>
    <transaction type="reserve" id="55c8a56e841ad">
        <code_station_from>2210700</code_station_from>
        <code_station_to>2200001</code_station_to>
        <date>2015-08-12</date>
        <train>733Ш</train>
        <wagon_type>С</wagon_type>
        <wagon_number>01</wagon_number>
        <wagon_class>2</wagon_class>
        <places>027</places>
        <documents>
            <document>
                <number>1</number>
                <kind>full</kind>
                <firstname>Павел</firstname>
                <lastname>Билык</lastname>
                <count_place>1</count_place>
            </document>
        </documents>
        <no_bedding>true</no_bedding>
    </transaction>
</request>
