<div class="col col_1">
    <div class="info_element">
        <div class="lbl"></div>
        <div class="info_txt"></div>
    </div>
</div>
<div class="col col_2">
    <div class="info_element">
        <div class="error_wrap">
            <div class="error_message" style="display:none;">
                <i class="close_icon"></i>
                <?php echo BookingModule::t('app', 'Вы не указали тип багажа') ?>
            </div>
            <select id="luggage_type" class="luggage_type" name="Places[<?php echo $place ?>][luggage][type]" onchange="$(this).val() == 'Б' ? $(this).parents('.ticket_item_container').find('.luggage_weight').show() : $(this).parents('.ticket_item_container').find('.luggage_weight').hide();">
                <option value="А"><?php echo BookingModule::t('app', 'Аппаратура') ?></option>
                <option value="С"><?php echo BookingModule::t('app', 'Птицы, животные') ?></option>
                <option value="Б"><?php echo BookingModule::t('app', 'Ручная кладь') ?></option>
            </select>
        </div>
    </div>
</div>
<div class="col col_3">
    <div class="info_element luggage_weight" style="display:none;">
        <div class="error_wrap">
            <div class="error_message" style="display:none;">
                <i class="close_icon"></i>
                <?php echo BookingModule::t('app', 'Вы не указали вес') ?>
            </div>
            <input type="text" placeholder="<?php echo BookingModule::t('app', 'Вес') ?>" class="inp" name="Places[<?php echo $place ?>][luggage][weight]">
        </div>
    </div>
</div>
<div class="col col_4"></div>
