<div class="tickets">
    <h3><?php echo BookingModule::t('app', 'Билеты'); ?></h3>
    <div class="attention_tooltip">
        <?php echo BookingModule::t('app', 'Обязательно введите фамилию и имя пассажира, который будет осуществлять поездку.'); ?>
        <i class="close_icon" onclick="$(this).parent().fadeOut('slow')"></i>
    </div>
    <form name="reserve" action="<?php echo $this->createUrl('reserve') ?>"method="POST">
        <!--<h4><?php echo BookingModule::t('app', 'Дополнительные сервисы:') ?> <i class="info_icon info_title" title="<?php echo BookingModule::t('app', 'Сервисы заказываются одновременно на все места.') ?>"></i></h4>-->
        <?php echo CHtml::checkBoxList('Services', 'П', $wagon->getServices(), array('container' => 'p', 'separator' => '&nbsp;')); ?>
    </form>
</div>