<div class="in_total">
    <a href="javascript:void(0);" class="cancel_btn" onClick="Purchase.close();"><?php echo BookingModule::t('app', 'Отмена') ?></a>
    <div class="btn_block">
        <span class="lbl"><?php echo BookingModule::t('app', 'Всего') ?>:</span>
        <span class="price">
            <span>{total}</span>
            <?php echo BookingModule::t('app', 'грн') ?>
        </span>
        <button class="btn confirm_btn" type="submit" onClick="Purchase.submit();"><?php echo BookingModule::t('app', 'Оформить') ?></button>
    </div>
</div>