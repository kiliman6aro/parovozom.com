<div class="ticket_item_container">
    <div class="ticket_item general_info">
        <div class="col col_1">
            <div class="info_element">
                <div class="lbl">{number}&nbsp;<?php echo BookingModule::t('app', 'вагон') ?></div>
                <div class="info_txt">{number_place}&nbsp;<?php echo BookingModule::t('app', 'место') ?></div>
            </div>
        </div>
        <div class="col col_2">
            <div class="info_element">
                <div class="radio_btn_block_1 choice_block" onchange="">
                    <?php echo CHtml::radioButton('Places[{place}][type]', true, array('id' => 'Places[{place}][type][reserve]', 'value' => 'reserve', 'onchange' => 'Ticket.changeType.call(this);')); ?>
                    <?php echo CHtml::label(BookingModule::t('app', 'Купить'), 'Places[{place}][type][reserve]'); ?>
                    <?php 
                        $options = array('id' => 'Places[{place}][type][booking]', 'value' => 'booking', 'onchange' => 'Ticket.changeType.call(this);');
                        if(!$train->isBooking()){
                            $options['disabled'] = 'disabled';
                        }
                    ?>
                    <?php echo CHtml::radioButton('Places[{place}][type]', false, $options); ?>
                    <?php echo CHtml::label(BookingModule::t('app', 'Бронировать'), 'Places[{place}][type][booking]', array('class' => 'middle_btn')); ?>
                </div>
            </div>
            <div class="info_element">
                <div class="radio_btn_block_2 choice_block">
                    <?php echo CHtml::radioButton('Places[{place}][kind]', true, array('id' => 'Places[{place}][kind][full]', 'value' => 'full', 'onchange' => 'Ticket.changeKind.call(this);')); ?>
                    <?php echo CHtml::label(BookingModule::t('app', 'Взрослый'), 'Places[{place}][kind][full]'); ?>
                    <?php echo CHtml::radioButton('Places[{place}][kind]', false, array('id' => 'Places[{place}][kind][child]', 'value' => 'child', 'onchange' => 'Ticket.changeKind.call(this);')); ?>
                    <?php echo CHtml::label(BookingModule::t('app', 'Детский'), 'Places[{place}][kind][child]', array('class' => 'middle_btn')); ?>
                </div>
            </div>

            <div class="info_element birthday_field" style="display:none;">
                <?php echo CHtml::label(BookingModule::t('app', 'Дата рождения') , 'Places[{place}][child]'); ?>
                <div class="error_wrap">
                    <div class="error_message" style="display:none;">
                        <i class="close_icon"></i>
                        <?php echo BookingModule::t('app', 'Вы не ввели дату рождения') ?>
                    </div>
                    <?php echo CHtml::textField('Places[{place}][child]', '', array('class' => 'inp birthday', 'id' => 'Places[{place}][child]')) ?>
                </div>
            </div>

            <div class="info_element luggage_field">
                <p>
                    <input type="checkbox" value="1" id="{place}_luggage" name="" onchange="Ticket.changeLuggage.call(this,'{place}')">
                    <label for="{place}_luggage"><?php echo BookingModule::t('app', 'Перевозка багажа') ?></label>
                    <i class="info_icon info_title" title=""></i>
                </p>
            </div>
        </div>
        <div class="col col_3">
            <div class="info_element">
                <div class="error_wrap">
                    <div class="error_message top_view" style="display:none;">
                        <i class="close_icon"></i>
                        <?php echo BookingModule::t('app', 'Вы не ввели имя') ?>
                    </div>
                    <input type="text" class="inp" name="Places[{place}][firstname]" placeholder="<?php echo BookingModule::t('app', 'Имя') ?>">
                </div>
            </div>
            <div class="info_element">
                <div class="error_wrap">
                    <div class="error_message" style="display:none;">
                        <i class="close_icon"></i>
                        <?php echo BookingModule::t('app', 'Вы не ввели фамилию') ?>
                    </div>
                    <input type="text" class="inp" name="Places[{place}][lastname]" placeholder="<?php echo BookingModule::t('app', 'Фамилия') ?>"/>
                </div>
            </div>
        </div>
        <div class="col col_4">
            <div class="info_element">
                <div class="price">
                    <span>{price}</span>
                    <?php echo BookingModule::t('app', 'грн') ?>
                </div>
            </div>
        </div>
        <i class="close_icon" onclick="Purchase.unselect('{place}')"></i>
    </div>
    <div class="ticket_item general_info additional">
        
    </div>
</div>

