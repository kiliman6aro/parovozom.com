<?php $this->beginContent('//layouts/main'); ?>
<div class="content inner_page">
    <div class="sticky-wrapper">
        <div class="sidebar" id="sidebar">
            <div class="container">
                <?php $this->widget('booking.widgets.search.SearchForm', array('direction' => $this->getParam('direction'))); ?>
                <?php $this->widget('widgets.advices.AdvicesWidget'); ?>
            </div>
        </div>
    </div>
    <div class="ticket_block">
        <div class="container">
            <div class="tbl_layout">
                <div class="l_column sort_block">
                    <?php $this->widget('booking.widgets.filters.Filter', array('routes' => $this->getParam('routes'))); ?>
                </div>
                <div class="r_column">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Yii::app()->clientScript->registerScript('sort-select2', '$("#price_sort").select2();', CClientScript::POS_END); ?>
<?php $this->endContent(); ?>