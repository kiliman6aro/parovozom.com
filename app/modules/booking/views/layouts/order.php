<?php $this->beginContent('//layouts/main'); ?>
<div class="content inner_page order_page">
    <div class="container">
        <h1><?php echo BookingModule::t('app', 'Ваш заказ'); ?></h1>
        <?php $this->widget('widgets.advices.AdvicesWidget'); ?>
        <?php echo $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>