<div class="text">
    <h3><?php echo $document->getStringType(); ?></h3>
</div>
<div class="blank electronic">
    <table class="part1">
        <tbody>
            <tr>
                <td rowspan="2">
                    <?php echo BookingModule::t('app', 'ТЕРМ.№{n}', array('{n}' => Settings::Get('mecrhant_id', 'booking'))); ?>
                </td>
                <td class="bold arial v-top"><?php echo $document->getStringType(); ?></td>
                <td class="bold"><?php echo Document::UIDtoString($document->uid); ?></td>
                <td class="bold arial w-right" rowspan="2">
                    <?php echo BookingModule::t('app', 'ПН:{tin}', array('{tin}' => $document->fiscal_info['tin'])); ?><br/>
                    <?php echo BookingModule::t('app', 'ФН:{rro}', array('{rro}' => $document->fiscal_info['rro'])); ?><br/>
                    <?php echo BookingModule::t('app', 'ЗН:{server}', array('{server}' => $document->fiscal_info['server'])); ?><br/>
                    <?php echo BookingModule::t('app', 'ФК:{id}', array('{id}' => $document->fiscal_info['id'])); ?>
                </td>
            </tr>
            <tr>
                <td class="address v-top nowrap">ДП«ГІОЦ Укрзалізниці»<br/> м.Київ, вул. І.Франка, буд.21</td>
                <td class="v-top"><b>#<?php echo $document->ordernumber ?><br/>#<?php echo Document::UIDtoString($document->linkUID, FALSE); ?></b></td>
            </tr>
            <tr>
                <td><?php echo $document->getStringPaymentType(); ?></td>
                <td class="bold arial center" colspan="2">
                    ЦЕЙ ПЕРЕВІЗНИЙ ДОКУМЕНТ Є ПІДСТАВОЮ ДЛЯ ПЕРЕВЕЗЕННЯ
                </td>
                <td class="align-right nowrap"><?php echo $document->getStringPayDate(); ?></td>
            </tr>
        </tbody>
    </table>
    <table class="part2">
        <tbody>
            <tr>
                <td class="nowrap w1">Відправлення</td>
                <td class="w1"><?php echo $document->direction->from_code; ?></td>
                <td class="upper ticket_info"><?php echo $document->direction->from_name; ?></td>
                <td>Поїзд</td>
                <td class="align-right ticket_info w-right"><?php echo $document->getDescriptionTrain(); ?></td>
            </tr>
            <tr>
                <td class="nowrap">Призначення</td>
                <td><?php echo $document->direction->to_code; ?></td>
                <td class="upper ticket_info"><?php echo $document->direction->to_name; ?></td>
                <td>Вагон</td>
                <td class="align-right ticket_info"><?php echo $document->getDescriptionWagon(); ?></td>
            </tr>
            <tr>
                <td colspan="2">Дата/час відпр.</td>
                <td class="ticket_info"><?php echo $document->getStringDepartureDate(); ?></td>
                <td>Тип</td>
                <td class="align-right"><?php echo BookingModule::t('kinds', $document->kind) ?></td>
            </tr>
            <tr>
                <td colspan="2">Дата/час приб.</td>
                <td class="ticket_info"><?php echo $document->getStringArrivalDate(); ?></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="5">
                    <?php echo $document->getStringPrice(); ?>
                    <!--ВАРТ=<b>7,91ГРН.</b>(ПР.ПЛ.0,76 + ПДВ 1,32 + КЗБ 5,83)-->
                </td>
            </tr>
            <tr>
                <td class="center" colspan="2">
                    <img alt="QR" src="data:image/gif;base64,<?php echo $document->qr_image ?>"/>
                </td>
                <td colspan="3">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>