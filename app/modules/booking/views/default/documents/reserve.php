<div class="text">
    <h3><?php echo $document->getStringType(); ?></h3>
</div>
<div class="blank electronic" data-doctype="reserve">
    <table class="part1">
        <tbody>
            <tr>
                <td class="nowrap" rowspan="2">
                    <?php echo BookingModule::t('app', 'ТЕРМ.№{n}', array('{n}' => Settings::Get('mecrhant_id', 'booking'))); ?>
                </td>
                <td class="bold arial v-top">
                    <?php echo $document->getStringType(); ?>
                </td>
                <td class="bold nowrap"><?php echo Document::UIDtoString($document->uid); ?></td>
                <td class="bold arial w-right" rowspan="2">
                    <?php echo BookingModule::t('app', 'ПН:{tin}', array('{tin}' => $document->fiscal_info['tin'])); ?><br/>
                    <?php echo BookingModule::t('app', 'ФН:{rro}', array('{rro}' => $document->fiscal_info['rro'])); ?><br/>
                    <?php echo BookingModule::t('app', 'ЗН:{server}',array('{server}' => $document->fiscal_info['server'])); ?><br/>
                    <?php echo BookingModule::t('app', 'ФК:{id}', array('{id}' => $document->fiscal_info['id'])); ?>
                </td>
            </tr>
            <tr>
                <td class="address v-top nowrap">ДП«ГІОЦ Укрзалізниці»<br/> м.Київ, вул. І.Франка, буд.21</td>
                <td class="v-top order_num">#<?php echo $document->ordernumber ?></td>
            </tr>
            <tr>
                <td>МПС</td>
                <td class="bold arial center" colspan="2">
                    ЗАМОВЛЕННЯ НЕОБХІДНО ВИКУПИТИ ДО 20:25 06.02.2015р.
                </td>
                <td class="align-right nowrap"><?php echo $document->getStringPayDate(); ?></td>
            </tr>
        </tbody>
    </table>
    <table class="part2">
        <tbody>
            <tr>
                <td colspan="3">Прізвище, Ім’я</td>
                <td><?php echo $document->getName(); ?></td>
                <td>Поїзд</td>
                <td class="align-right ticket_info w-right">
                    <?php echo $document->getDescriptionTrain(); ?>
                </td>
            </tr>
            <tr>
                <td class="nowrap w1">Відправлення</td>
                <td colspan="2" class="w1"><?php echo $document->direction->from_code; ?></td>
                <td class="upper ticket_info"><?php echo $document->direction->from_name; ?></td>
                <td>Вагон</td>
                <td class="align-right ticket_info"><?php echo $document->getDescriptionWagon(); ?></td>
            </tr>
            <tr>
                <td class="nowrap">Призначення</td>
                <td colspan="2"><?php echo $document->direction->to_code; ?></td>
                <td class="upper ticket_info"><?php echo $document->direction->to_name; ?></td>
                <td>Місце</td>
                <td class="align-right ticket_info"><?php echo $document->place; ?> <?php echo String::mb_ucfirst(BookingModule::t('ticket_types', $document->kind)); ?></td>
            </tr>
            <tr>
                <td colspan="3">Дата/час відпр.</td>
                <td class="ticket_info"><?php echo $document->getStringDepartureDate(); ?></td>
                <td>&nbsp;</td>
                <td class="align-right">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="ch-num-l">Дата/час приб.</td>
                <td class="ch-num">
                </td>
                <td class="ticket_info"><?php echo $document->getStringArrivalDate(); ?></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="6">
                    ПОСЛУГА:РЕЗЕРВУВАННЯ<br/>
                    <?php echo $document->getStringPrice(); ?>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="barcode">
        <img width="500" alt="barcode" src="data:image/gif;base64,<?php echo $document->barcode_image ?>"/>
    </div>
</div>