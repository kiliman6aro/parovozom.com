<div class="blank">
    <?php echo $this->renderPartial(Document::VIEW_ROOT.'/ppd/_header', array('document' => $document)); ?>
    <table class="places">
        <tr>
            <th><?php echo BookingModule::t('app', 'Номер поезда') ?>: <?php echo $document->getDescriptionTrain(); ?></th>
            <th><?php echo BookingModule::t('app', 'Вагон') ?>: <?php echo $document->getDescriptionWagon(); ?></th>
            <th><?php echo BookingModule::t('app', 'Тип') ?>: <?php echo BookingModule::t('kinds', $document->kind); ?></th>
        </tr>
    </table>
    <div class="prices">
        <h4><?php echo BookingModule::t('app', 'ИНФОРМАЦИЯ О СТОИМОСТИ ЗАКАЗА') ?>, грн.</h4>
        <table class="price_info">
            <tr class="center bold">
                <td><?php echo BookingModule::t('app', 'Количество мест') ?></td>
                <td><?php echo BookingModule::t('app', 'Стоимость проездного документа') ?>, грн. (<?php echo BookingModule::t('app', 'Тариф') ?>, <?php echo BookingModule::t('app', 'КСБ.') ?>, <?php echo BookingModule::t('app', 'СТР.') ?>, <?php echo BookingModule::t('app', 'НДС.') ?>)</td>
                <td><?php echo BookingModule::t('app', 'Сбор за оформление ППД без НДС') ?>, грн.</td>
            </tr>
            <tr class="center">
                <td>1</td>
                <td><?php echo $document->prices[0]['reserved_seat'] + $document->prices[0]['vat']; ?> грн</td>
                <td><?php echo $document->prices[0]['fee']; ?> грн</td>
            </tr>
            <tr>
                <td colspan="1" class="nowrap bold"><?php echo BookingModule::t('app', 'Общая стоимость') ?></td>
                <td class="center" colspan="2"><?php echo $document->prices[0]['cost']; ?> грн</td>
            </tr>
        </table>
    </div>

    <div><?php $this->renderPartial(Document::VIEW_ROOT.'/ppd/_footer'); ?></div>
</div>

