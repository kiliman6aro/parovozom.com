<div class="blank">
    <?php $this->renderPartial(DOcument::VIEW_ROOT.'/ppd/_header', array('document' => $document)) ?>
    <?php foreach($document->places as $place): ?>
    <table class="places">
        <tr>
            <th><?php echo BookingModule::t('app', 'Номер поезда') ?>: <?php echo $document->getDescriptionTrain(); ?></th>
            <th><?php echo BookingModule::t('app', 'Вагон') ?>: <?php echo $document->getDescriptionWagon(); ?></th>
            <th><?php echo BookingModule::t('app', 'Место') ?>: <?php echo $place->number; ?><span>, <?php echo String::toLower($place->getType()); ?>, <?php echo String::toLower($place->getServices()); ?></span></th>
        </tr>
        <tr>
            <th><?php echo $place->getName(); ?></th>
            <th colspan="2"><?php echo $place->uid; ?></th>
        </tr>
    </table>
    <?php endforeach; ?>
    <div class="prices">
        <h4><?php echo BookingModule::t('app', 'ИНФОРМАЦИЯ О СТОИМОСТИ ЗАКАЗА') ?>, грн.</h4>
        <table class="price_info">
            <tr class="center bold">
                <td colspan="4"><?php echo BookingModule::t('app', 'Проездные документы') ?></td>
            </tr>
            <tr class="center bold">
                <td><?php echo BookingModule::t('app', '№ доку-<br/>мента') ?></td>
                <td><?php echo BookingModule::t('app', 'Места') ?></td>
                <td><?php echo BookingModule::t('app', 'Стоимость проездного документа') ?>, грн. (<?php echo BookingModule::t('app', 'Тариф') ?>, <?php echo BookingModule::t('app', 'КСБ.') ?>, <?php echo BookingModule::t('app', 'СТР.') ?>, <?php echo BookingModule::t('app', 'НДС.') ?>)</td>
                <td><?php echo BookingModule::t('app', 'Сбор за оформление ППД без НДС') ?>, грн.</td>
            </tr>
            <?php foreach($document->places as $key => $place): ?>
            <tr class="center">
                <td><?php echo ++$key ?></td>
                <td><?php echo $place->number; ?></td>
                <td><?php echo (isset($place->prices[0]['service']) ? $place->prices[0]['service'] : 0.00)  + $place->prices[0]['ticket'] + $place->prices[0]['reserved_seat'] + $place->prices[0]['commission']  ?> + <?php echo $place->prices[0]['insurance']; ?> (страх. збiр) + <?php echo $place->prices[0]['vat'] ?> грн</td>
                <td><?php echo $place->prices[0]['fee']; ?> грн</td>
            </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="2" class="nowrap bold"><?php echo BookingModule::t('app', 'Общая стоимость') ?></td>
                <td class="center" colspan="2"><?php echo $document->getStringPrice(); ?> грн</td>
            </tr>
        </table>
    </div>
    <div><?php $this->renderPartial(Document::VIEW_ROOT.'/ppd/_footer'); ?></div>
</div>