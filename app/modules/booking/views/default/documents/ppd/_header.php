<div class="header_doc"><?php echo BookingModule::t('app', 'БЛАНК ЗАКАЗА'); ?></div>
<div class="orderDate">
    <?php echo BookingModule::t('app', 'Дата и номер заказа') ?>: <?php echo $document->getStringPayDate(); ?>
</div>
<div class="clear"></div>
<div class="barcode">
    <div class="header_doc"><?php echo $document->ordernumber; ?></div>
    <img width="500" alt="barcode" src="data:image/gif;base64,<?php echo $document->barcode_image; ?>"/>
</div>
<table class="info">
    <tr>
        <th><?php echo BookingModule::t('app', 'Станция отправления') ?>:</th>
        <td><?php echo $document->direction->from_code; ?></td><td><?php echo $document->direction->from_name; ?></td>
    </tr>
    <tr>
        <th ><?php echo BookingModule::t('app', 'Станция назначения') ?>:</th>
        <td><?php echo $document->direction->to_code; ?></td><td><?php echo $document->direction->to_name; ?></td>
    </tr>
    <tr>
        <th colspan="2"><?php echo BookingModule::t('app', 'Дата и время отправления') ?>:</th>
        <td><?php echo $document->getStringDepartureDate(); ?></td>
    </tr>
    <tr>
        <th class="ch-num-l"><?php echo BookingModule::t('app', 'Дата и время прибытия') ?>:</th>
        <td class="ch-num">
        </td>
        <td><?php echo $document->getStringArrivalDate(); ?></td>
    </tr>
</table>