<div class="wagon">
    <div class="hd column_layout">
        <div class="column1">
            <h3><?php echo BookingModule::t('app', 'Номер вагона'); ?></h3>
        </div>
        <div class="column2">
            <div class="wagon_choice">
                <?php if(isset($wagons) && is_array($wagons)): ?>
                <?php foreach ($wagons as $wagon): ?>
                <div class="info_element">
                    <input class="radio" onchange="Purchase.places('<?php echo $wagon->number ?>')" type="radio" value="<?php echo $wagon->number ?>" name="time" id="wagon_<?php echo $wagon->number ?>"count="<?php echo $wagon->places ?>"/>
                    <label for="wagon_<?php echo $wagon->number ?>">
						<?php echo $wagon->number ?>
						<span class="info_tooltip ">
							<span><?php echo $wagon->places ?></span>
							<?php echo BookingModule::t('app', 'места'); ?>
						</span>
					</label>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>