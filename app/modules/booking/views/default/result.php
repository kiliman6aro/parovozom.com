<?php
foreach ($documents as $document) {
    $this->renderPartial($document->getLayoutPath(), array('document' => $document));
}
