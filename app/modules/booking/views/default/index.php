<?php echo $this->setTitle($this->getParam('direction')->from_name.'-'.$this->getParam('direction')->to_name);?>
<div class="hd">
    <h1>
        <span class="title">Рейсы</span>
    </h1>
    <div class="sort_select" <?php echo (Settings::Get('autoload_price', 'booking')) ? '' : 'style="display:none;"' ?>>
        <label class="label"><?php echo BookingModule::t('app', 'Сортировать') ?>:</label>
        <select id="price_sort" class="price_sort">
            <option value="up"><?php echo BookingModule::t('app', 'Стоимость по возрастанию'); ?></option>
            <option value="down"><?php echo BookingModule::t('app', 'Стоимость по убыванию'); ?></option>
        </select>
    </div>
</div>
<div class="search_result">
    <?php foreach ($routes as $route): ?>
        <div class="result_item" train="<?php echo $route->train->number; ?>" 
             model="<?php echo $route->train->model;  ?>"
             price=""
             booking="<?php echo $route->train->isBooking() ? 'true' : 'false' ?>"
             wagon_type="<?php echo $route->wagon_type; ?>"
             travel_time="<?php echo $route->train->travel_time_array['h'] ?>"
             departure_time="<?php echo (floor($route->train->departure_date_array['h'] * 60) + $route->train->departure_date_array['i']) ?>"
             arrival_time="<?php echo (floor($route->train->arrival_date_array['h'] * 60) + $route->train->arrival_date_array['i']) ?>"
             <?php echo isset($route->wagon_class) ? 'wagon_class="' . $route->wagon_class . '"' : '' ?>>
            <div class="general_info">
                <div class="col col_1">
                    <div class="info_element" <?php echo (Settings::Get('autoload_price', 'booking')) ? '' : 'style="display:none;"' ?>>
                        <div class="lbl"><?php echo BookingModule::t('app', 'Цена от'); ?></div>
                        <div class="price">
                            <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/media/images/loader.gif" width="90" height="10" alt=""/></span>
                        </div>
                    </div>
                    <div class="info_element type_number">
                        <div class="lbl"><?php echo BookingModule::t('app', 'Тип'); ?></div>
                        <div class="info_txt"><?php echo BookingModule::t('wagon_types', $route->wagon_type); ?></div>
                        <?php if (isset($route->wagon_class)): ?>
                            <span><?php echo $route->wagon_class . BookingModule::t('app', '-ый класс'); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="info_element train_number">
                        <div class="lbl"><?php echo BookingModule::t('app', 'Поезд') ?></div>
                        <div class="info_txt"><a onclick="$('.section[name=trains]').find('#'+$(this).html()).siblings('a').trigger('click');" href="javascript:void(0);"><?php echo $route->train->number ?></a></div>
                    </div>
                    <div class="btn_block">
                        <a href="javascript:void(0);" onclick="Purchase.open('<?php echo $route->train->number  ?>', '<?php echo $route->wagon_type  ?>'<?php echo isset($route->wagon_class) ? ', ' . $route->wagon_class : null ?>);" class="btn"><?php echo BookingModule::t('app', 'Выбрать место'); ?></a>
                        <div class="info_tooltip">
                            <span><?php echo $route->place_count; ?></span>
                            <?php echo BookingModule::t('app', 'свободных'); ?>
                        </div>
                    </div>
                </div>
                <div class="col col_2 trip_name">
                    <div class="trip_title"><?php echo String::toTitle($route->train->station_from_name); ?> <b>&rarr;</b><?php echo String::toTitle($route->train->station_to_name); ?></div>
                    <div class="column_layout">
                        <div class="column1">
                            <div class="info_element">
                                <div class="lbl"><?php echo BookingModule::t('app', 'Время') ?></div>
                                <div class="info_txt">
                                    <?php echo $route->train->departure_date_array['h'] . ':' . $route->train->departure_date_array['i'] ?>
                                    -
                                    <?php echo $route->train->arrival_date_array['h'] . ':' . $route->train->arrival_date_array['i'] ?>
                                </div>
                            </div>
                        </div>
                        <div class="column2">
                            <div class="info_element">
                                <div class="lbl"><?php echo BookingModule::t('app', 'Время в пути') ?></div>
                                <div class="info_txt">
                                    <?php echo BookingModule::t('app', '{h} ч {i} м', array('{h}' => $route->train->travel_time_array['h'], '{i}' => $route->train->travel_time_array['i'])) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="overlay"></div>
        </div>
    <?php endforeach; ?>
</div>
<script type="text/javascript">
    Ticket = {
        changeType: function(){
            var parent = $(this).parents('.ticket_item_container');
            if($(this).val() == 'booking'){
                parent.find('input[value=child]').prop('disabled', true);
            }else{
                parent.find('input[value=child]').prop('disabled', false);
            }
            parent.find('.luggage_field').toggle();
            parent.find('.services').toggle();
            parent.find('.radio_btn_block_2').buttonset('refresh');
            
        },
        changeKind: function(){
            var parent = $(this).parents('.ticket_item_container');
            parent.find('.birthday_field').toggle();
        },
        changeLuggage: function(place){
            var parent = $(this).parents('.ticket_item_container');
            var additional = parent.find('.additional');
            if($(this).prop('checked')){
                additional.load('/booking/default/additional?place='+place, function(){
                    $(this).find('.luggage_type').select2();
                });
            }else{
                additional.empty();
            }
            additional.toggle();
        }
    };
</script>
