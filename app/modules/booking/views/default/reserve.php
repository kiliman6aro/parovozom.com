<?php foreach ($cart->orders as $order): ?>
<div class="checkout_content">
	<!-- Заголовок формы -->
	<div class="checkout_title">
		<a href="#" class="checkout_delete close_icon" title="Удалить заказ"></a>
		<!-- Поезд -->
		<div class="checkout_title_item inline_block checkout_train_info">
		<div class="checkout_title_inner">
			<div class="checkout_train_icon inline_block_middle">
				<span class="checkout_icon icon_train_white"></span>
				<span class="checkout_icon_text"><?php echo BookingModule::t('app', 'Поезд'); ?> <?php echo $order->train->number; ?></span>
			</div>
			<div class="checkout_title_value inline_block_middle">
			<?php echo String::mb_ucfirst($order->train->station_from_name); ?>
			<br>
			<?php echo String::mb_ucfirst($order->train->station_to_name); ?>
			</div>
		</div>
		</div>

		<!-- Отправление -->
		<div class="checkout_title_item inline_block checkout_from_name">
		<div class="checkout_title_inner">
			<span class="checkout_icon icon_clock_white"></span>
			<span class="checkout_title_value inline_block_middle">
				<?php echo BookingModule::t('app', 'Отправление') ?>: <?php echo String::mb_ucfirst($order->direction->from_name); ?><br>
				<?php echo Yii::app()->dateFormatter->format('dd MMMM y, EEEE в HH:mm', strtotime($order->train->departure_date)); ?>
			</span>
		</div>
		</div>
		
		<!-- Прибытие -->
		<div class="checkout_title_item inline_block checkout_to_name">
		<div class="checkout_title_inner">
			<span class="checkout_icon icon_clock_white"></span>
			<span class="checkout_title_value inline_block_middle">
				<?php echo BookingModule::t('app', 'Прибытие') ?>: <?php echo String::mb_ucfirst($order->direction->to_name); ?><br>
				<?php echo Yii::app()->dateFormatter->format('dd MMMM y, EEEE в HH:mm', strtotime($order->train->arrival_date)); ?>
			</span>
		</div>
		</div>
	</div>
	
	<?php foreach ($order->places as $place): ?>
		<!-- Контент билета -->
		<div class="checkout_item">
			
			<!-- Вагон -->
			<div class="checkout_wagon inline_block_middle">
				<div class="checkout_item_title"><?php echo $order->wagon->number; ?> вагон</div>
				<div class="checkout_item_value"><?php echo $place->number ?> место</div>
				<?php if ($place->type == 'booking' || $place->kind == 'child'): ?>
                    <div class="note"><?php echo $place->getType(); ?></div>
                <?php endif; ?>
			</div>
			
			<!-- Имя -->
			<div class="checkout_name inline_block_middle">
				<div class="checkout_first_name inline_block_middle">
					<div class="checkout_item_title">Имя</div>
					<div class="checkout_item_value"><?php echo $place->firstname ?></div>
				</div>
				<div class="checkout_last_name inline_block_middle">
					<div class="checkout_item_title">Фамилия</div>
					<div class="checkout_item_value"><?php echo $place->lastname ?></div>
				</div>
			</div>
			
			<!-- Цена -->
			<div class="checkout_price inline_block_middle">
				<?php echo Yii::app()->numberFormatter->formatDecimal($place->total); ?> грн
			</div>
			
			<!-- Выводим доп. инфо, если есть -->
			<?php if ($place->isBooking() || $place->isLuggage() || $place->isServices()): ?>
			<div class="checkout_service">
                <?php if ($place->isBooking()): ?>
                    <div class="checkout_service_item inline_block">
                        <?php echo BookingModule::t('app', 'Выкупить не позднее') ?>:
                        <?php echo Yii::app()->dateFormatter->format('dd MMMM y, EEEE в HH:mm', strtotime($place->getBookingDate())); ?>
                    </div>
                    <div class="checkout_service_item inline_block">
                        <?php echo BookingModule::t('app', 'Стоимость билета') ?>:
                        <?php echo $place->getBookingPrice(); ?> грн
                    </div>
                <?php else: ?>
                    <?php if ($place->isLuggage()): ?>
                        <div class="checkout_service_item inline_block">
                            <?php echo BookingModule::t('app', 'Багаж') ?>:
                            <?php echo $place->getLuggage(); ?>,
							<?php echo $place->getLuggagePrice(); ?> грн
                        </div>
                    <?php endif; ?>
                    <?php if ($place->isServices()): ?>
                        <div class="checkout_service_item inline_block">
                            Сервис:
                            <?php echo $place->getServices(); ?>,
							<?php echo $place->getServicesPrice(); ?> грн
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
			<?php endif; ?>
			
		</div>
	
	<?php endforeach; ?>
</div>
<?php endforeach; ?>
<!-- Итоговая цена -->
<div class="checkout_total">
    <a href="#" class="cancel_btn">Удалить все заказы</a>
    <span class="checkout_total_label inline_block_middle">Итого:</span>
    <span class="checkout_total_price inline_block_middle">
        <?php echo Yii::app()->numberFormatter->formatDecimal($cart->total); ?> грн
    </span>
    <button id="confirm" class="btn inline_block_middle" type="">Оплатить</button> 
</div>






<div class="train_info column_layout" style="margin-top: 200px; display: none;">
    <div class="column1">
        <div class="field">
            <span class="lbl"><?php echo BookingModule::t('app', 'Поезд'); ?> <?php echo $order->train->number; ?>:</span>
            <?php echo $order->train->station_from_name; ?> – <?php echo $order->train->station_to_name; ?>
        </div>
        <div class="field">
            <span class="lbl"><?php echo BookingModule::t('app', 'Направление') ?>:</span>
            <?php echo $order->direction->from_name; ?> – <?php echo $order->direction->to_name; ?>
        </div>
    </div>
    <div class="column2">
        <div class="field">
            <span class="lbl"><?php echo BookingModule::t('app', 'Отправление') ?>:</span>
            <?php echo Yii::app()->dateFormatter->format('dd MMMM y, EEEE в HH:mm', strtotime($order->train->departure_date)); ?>
        </div>
        <div class="field">
            <span class="lbl"><?php echo BookingModule::t('app', 'Прибытие') ?>:</span>
            <?php echo Yii::app()->dateFormatter->format('dd MMMM y, EEEE в HH:mm', strtotime($order->train->arrival_date)); ?>
        </div>
    </div>
</div>
<div class="result_item" style="display: none;">
    <div class="ticket_block">
        <div class="tickets">
            <div class="wrap">
                <?php foreach ($order->places as $place): ?>
                    <div class="ticket_item general_info">
                        <div class="col col_1">
                            <div class="info_element">
                                <div class="lbl"><?php echo $order->wagon->number; ?> вагон</div>
                                <div class="info_txt"><?php echo $place->number ?> место</div>
                                <?php if ($place->type == 'booking' || $place->kind == 'child'): ?>
                                    <div class="note"><?php echo $place->getType(); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col col_2">
                            <div class="name_field">
                                <div class="info_element">
                                    <div class="lbl"><?php echo BookingModule::t('app', 'Имя') ?></div>
                                    <div class="info_txt"><?php echo $place->firstname ?></div>
                                </div>
                                <div class="info_element">
                                    <div class="lbl"><?php echo BookingModule::t('app', 'Фамилия') ?></div>
                                    <div class="info_txt"><?php echo $place->lastname ?></div>
                                </div>
                            </div>
                            <div class="service_field">
                                <?php if ($place->isBooking()): ?>
                                    <div class="info_element">
                                        <div class="lbl"><?php echo BookingModule::t('app', 'Выкупить не позднее: ') ?></div>
                                        <div class="info_txt small_txt"><?php echo Yii::app()->dateFormatter->format('dd MMMM y, EEEE в HH:mm', strtotime($place->getBookingDate())); ?></div>
                                    </div>
                                    <div class="info_element width_guide">
                                        <div class="lbl"><?php echo BookingModule::t('app', 'Стоимость билета:') ?></div>
                                        <div class="info_txt small_txt"><?php echo $place->getBookingPrice(); ?> грн</div>
                                    </div>
                                <?php else: ?>
                                    <?php if ($place->isLuggage()): ?>
                                        <div class="info_element width_guide">
                                            <div class="lbl"><?php echo BookingModule::t('app', 'Багаж') ?></div>
                                            <div class="info_txt small_txt"><?php echo $place->getLuggage(); ?></div>
                                        </div>
                                        <div class="info_element">
                                            <div class="lbl"><?php echo BookingModule::t('app', 'Стоимость') ?></div>
                                            <div class="info_txt small_txt"><?php echo $place->getLuggagePrice(); ?> грн</div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($place->isServices()): ?>
                                        <div class="info_element width_guide">
                                            <div class="lbl">сервис</div>
                                            <div class="info_txt small_txt"><?php echo $place->getServices(); ?></div>
                                        </div>
                                        <div class="info_element">
                                            <div class="lbl"><?php echo BookingModule::t('app', 'Стоимость') ?></div>
                                            <div class="info_txt small_txt"><?php echo $place->getServicesPrice(); ?> грн</div>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col col_4">
                            <div class="info_element">
                                <div class="price">
                                    <span><?php echo $place->total; ?></span>
                                    грн
                                </div>
                            </div>
                            <!--<i class="close_icon"></i>-->
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="main_search reserve_search_container">
<!--                <div class="cancel_reserve">
                    <a href="javascript:void(0);" class="cancel_btn"><?php echo BookingModule::t('app', 'Отменить') ?></a>
                </div>
                <form action="">
                    <span class="wrap_inp">
                        <div class="error_wrap">
                            <div class="error_message">
                                <i class="close_icon"></i>
                                Ошибка ввода email адреса
                            </div>
                            <input type="text" class="mail_input error" placeholder="EMail"/>
                        </div>
                    </span>
                    <span class="wrap_inp">
                        <div class="error_wrap">
                            <div class="error_message">
                                <i class="close_icon"></i>
                                Пароль содержит только цифры
                            </div>
                            <input type="text" class="pas_input error" placeholder="пароль"/>
                        </div>
                    </span>
                    <span class="wrap_inp">
                        <div class="error_wrap">
                            <div class="error_message">
                                <i class="close_icon"></i>
                                Пароли не совпадают
                            </div>
                            <input type="text" class="pas_input error" placeholder="повторить пароль">
                        </div>
                    </span>
                </form>
                <form action="" class="reserve_form">
                    <div class="reserve_textfields">
                        <span class="wrap_inp">
                            <div class="error_wrap">
                                <div class="error_message">
                                    <i class="close_icon"></i>
                                    Ошибка ввода email адреса
                                </div>
                                <input type="text" class="mail_input error" placeholder="EMail"/>
                            </div>
                        </span>
                        <span class="wrap_inp">
                            <div class="error_wrap">
                                <div class="error_message">
                                    <i class="close_icon"></i>
                                    Пароль содержит только цифры
                                </div>
                                <input type="text" class="pas_input error" placeholder="пароль"/>
                            </div>
                        </span>
                    </div>
                    <a href="javascript:void(0);" class="confirm btn confirm_btn">Войти</a>
                </form>-->
            </div>
            
        </div>
    </div>
</div>
<?php echo $cart->render(); ?>
<div id="dialog1" class="support_pp">
    <h5>Внимание!</h5>
    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl('booking/default/pay'),
        'id' => 'confirm-form',
        'enableClientValidation'=>true,
	'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnChange' => false
	),
    )); ?>
        <div class="field">
            <p>После оплаты у Вас не будет возможности внести изменения в ваш заказ (имя, дату, маршрут и т.д.). Для совершения оплаты у Вас есть 30 минут. По истечении этого срока билеты будут снова в продаже.</p>
            <br>
        </div>
        <div class="send_email">
            <?php echo $form->textField($model, 'email', array('placeholder' => BookingModule::t('app', 'Введите ваш E-mail'), 'class' => 'mail_input')) ?>
            <div class="error_wrap">
                <?php echo $form->error($model, 'email', array('class' => 'error_message')); ?>
            </div>
        </div>
        <div class="field btn_block" style="margin-top: 30px;">
            <div class="agree_terms">
                <input id="agree_terms" class="checkbox" name="ConfirmForm[agree]" type="checkbox">
                <label class="checkbox_label" for="agree_terms">Я принимаю <a href="<?php echo Yii::app()->createUrl('site/terms'); ?>" target="_blank">правила</a> публичной оферты</label>
            </div>
            <a href="javascript:void(0);" class="btn" onclick="$('#confirm-form').submit();">Перейти к оплате</a>
        </div>
        <?php $this->endWidget(); ?>
</div>

<script>
    $(function() {
        $('.checkout_title_item').each(function(){
            if($(this).outerWidth() < $(this).find('.checkout_title_inner').outerWidth()) {
                $(this).addClass('long_text');
            }

        });
    }(jQuery));
        
</script>