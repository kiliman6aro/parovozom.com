<div class="legend">
	<span class="bottom_shelf">
		<i class="icon"></i>
		<?php echo Yii::t('app', 'Lower shelf'); ?>
	</span>
	<span class="top_shelf">
		<i class="icon"></i>
		<?php echo Yii::t('app', 'Top shelf'); ?>
	</span>
	<span class="current_shelf">
		<i class="icon"></i>
		<?php echo Yii::t('app', 'Selected Place'); ?>
	</span>
	<span class="back_dir">
		<i class="icon"></i>
		<?php echo Yii::t('app', 'Back to the direction'); ?>
	</span>
	<span class="forward_dir">
		<i class="icon"></i>
		<?php echo Yii::t('app', 'Forward to the direction'); ?>
	</span>
</div>
<div class="attention_tooltip" id="scheme_messages" style="display: none;">
    <?php echo Yii::t('app', 'For once, you can select up to {n} seats', array('{n}' => 4)); ?>
    <i class="close_icon" onClick="$(this).parent().fadeOut('slow')"></i>
</div>
<?php echo $scheme; ?>

<script type="text/javascript">
    $('.info_title').tooltip({
        show: null,
        position: {
            my: "left-20 top+8",
            at: "left bottom+1"
        },
        open: function (event, ui) {
            ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast");
        }
    });
    function unselected(n){
        var btn = $('.wagon_number[data-id='+n.toPlaceNumber()+']');
        btn.removeClass('active');
        //Если было деактиваровано последнее место
        if($('.wagon_number.active').length === 0){
            //Повторная инициализация схемы
            initializeScheme(window.places, true);
        }
    }
    /**
     * Обработчик события выбора места. Первым делом получает количество
     * уже активных элементов (с классом ative). Если место, по которому
     * был щёлчек, активно (повторный щёлчек по тому же месту), то место
     * сразу же деактивизируется (класс active удаляется) и выполняется
     * метод Purchase.unselect для удаления места из корзины. После этого,
     * выполняется провека, на количество активных мест, после деактивации
     * если место было последним, то выполнется переинициализация схемы.
     * Дальше управление передается методу Purchase.select, который помещает
     * место в корзину. Если метод возрвщает false, значит достигнут лимит
     * мест в корзине, необходиом вывести сообщение об этом.
     * 
     * @param {type} event.data.charline цепочка (тип места в вагоне)
     * @param {type} event.data.place номер места
     * @returns {undefined}
     */
    function selected(event){
        var btn = $(event.target);
        if(btn.hasClass('sold')){return false;}
        
        if(btn.hasClass('active')){
            if(Purchase.unselect(event.data.place, event.data.charline)){
                unselected(event.data.place);
                return true;
            }
            return false;
        }
        //Если элементов уже 4, то вернет false
        if(Purchase.select(event.data.place, event.data.charline)){
            btn.addClass('active');
            //Места другого charline получают статус sold
            //что бы пользователь не мог выбрать места из разных charline
            var exc = $('.wagon_number[charline='+event.data.charline+']');
            $('.wagon_number').not(exc).addClass('sold');
            return true;
        }else{
            //Отобразить ошибку, что достигнут лимит мест в корзине
            $('#scheme_messages').show();
            setTimeout(function(){
                $('#scheme_messages').fadeOut('slow');
            }, 2000);
        }

    }
    function initializeScheme(charlines, reset){       
        $('.wagon_number').addClass('sold');
        for(charline in charlines){
            var places = charlines[charline];
            for(var i = 0; i < places.length; i++){
                //Сервер передает места в формате 001, а в шаблоне используется формат 01
                //потому необходимо изменить формат, для поиска места
                //что бы пометить его как свободное
                var number = places[i].toPlaceNumber();
                var place = $('.wagon_number[data-id="'+number+'"]');
                place.attr('charline', charline);
                place.removeClass('sold');
                //Если reset, значит это повторная инициализация,
                //то обработчики уже заданы, их задавать не нужно
                if(!reset){
                   //передача параметров в обработчик события
                   place.on('click', {charline: charline, place:places[i]}, selected);
                  
                }
            }
        }
        if(!reset){
             $(Purchase).on('remove_place_from_cart', function(e, number){
                unselected(number);
            });
        }
    }
    $(document).ready(function(){        
        window.places = <?php echo CJavaScript::encode($places); ?>;
        initializeScheme(places);
    });
</script>