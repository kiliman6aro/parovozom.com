<?php
Yii::import('booking.classes.*');

class DefaultController extends RailController {

    public function actionIndex() {
        $direction = new Direction;
        $direction->from_code = Yii::app()->request->getParam('from');
        $direction->from_name = Yii::app()->request->getParam('from_name');
        $direction->to_code = Yii::app()->request->getParam('to');
        $direction->to_name = Yii::app()->request->getParam('to_name');
        $direction->date1 = Yii::app()->request->getParam('startDate');
        $direction->vote();
        $this->setParam('direction', $direction);
        $transaction = TransactionsBuilder::build('trains extended', $this, $this);
        $routes = $transaction->sign()->submit();
        $this->setParam('routes', $routes);
        $this->render('index', array('routes' => $routes));
    }
    
    public function actionPrices(){
        $transaction = TransactionsBuilder::build('prices', $this, $this);
        $wagons = $transaction->sign()->submit();
        $this->setParam('wagons', $wagons);
        $html = $this->renderPartial('wagon', array('wagons' => $wagons), true);
        echo CJSON::encode(array('result' => 'OK', 'html' => $html, 'items' => $wagons));
    }
    
    public function actionPlaces(){
        $transaction = TransactionsBuilder::build('places', $this, $this);
        $wagons = $transaction->sign()->submit();
        $scheme = BookingModule::GetScheme($this->getParam('wagon_number'), $this->getParam('wagon_type'), $this->getParam('wagon_class'));
        $train = $this->getTrainByNumber($this->getParam('train'));

        $tmpls = array(
            'ticket_item' => $this->renderPartial('/tmpls/ticket_item', array('train' => $train), true),
            'ticket_total' => $this->renderPartial('/tmpls/ticket_total', null, true),
            'tickets_block' => $this->renderPartial('/tmpls/ticket_block', array('wagon' => $this->getWagonByNumber($this->getParam('wagon_number'))), true),
            'tooltip' => '<div class="attention_tooltip">{message}<i class="close_icon" onClick="$(this).parent().fadeOut(\'slow\')"></i></div>',
            'child_calendar_title' => "<div class='title'>".  BookingModule::t('app', 'Выберете дату рождения ребенка (6-14 лет):').":</div>"
        );
        echo CJSON::encode(array(
            'result' => 'OK', 
            'scheme' => $this->renderPartial('scheme', array('scheme' => $scheme, 'places' => $wagons, 'wagon' => $this->getWagonByNumber($this->getParam('wagon_number'))), true), 
            'tmpls' => $tmpls,
            'places' => $wagons));
        Yii::app()->end();
    }
    public function actionReserve(){
        
        $order = $this->_loadOrder();
        foreach ($this->_getTypesPlaces() as $type => $places){
            $this->setParam('Places', $places);
            $transaction = TransactionsBuilder::build($type, $this, $this);
            $transaction->setOrder($order);
            $products = $transaction->sign()->submit();
            $order->addProducts($products);
        }
        $purchase = $this->_loadPurchase();
        $purchase->addOrder($order);
        if(!$purchase->save()){
            throw new CDbException(BookingModule::t('errors', 'Не удалось сохранить заказ'), 500);
        }
        $this->redirect('orders');
    }
    /**
     * Происходит проверка ответа от сервиса платежки на валидность.
     * Если ответ присутствует и он положительный, то осуществляется покупка.
     * Билеты высылаются пользователю на почту.
     * Если ответ присутствует и он отрицательный, то покупка не осуществляется. 
     * Заказу присваивается соответствующий статус.
     */
    public function actionVerify(){
        $order_id = (int)Yii::app()->request->getParam('order_id');
        $purchase = Purchase::model()->new()->findByPk($order_id);
        if(!$purchase){
            echo 'NO';
            Yii::app()->end();//Заказ не найден - вернуть деньги
        }
        if(!$purchase->verify($_POST)){
            $purchase->text = BookingModule::t('app', 'Ошибка со стороны платежного шлюза. Некорректная подпись данных');
            $purchase->error();
            $purchase->save();
            echo 'NO';
            Yii::app()->end();
        }
        try{
            if(Yii::app()->request->getParam('status') == 1){
                $purchase->pay();
                $user = Users::model()->findByPk($purchase->user_id);
                if($user){
                    $this->sendDocumentsToEmail($user->email, $purchase->getDocuments());
                }
            }
            if(Yii::app()->request->getParam('status') == 0){
                $purchase->error();
            }
            $purchase->pay_description = Yii::app()->request->getParam('text');
            $purchase->save();
            echo Yii::app()->createAbsoluteUrl('booking/default/result', array('id' => $order_id));
            Yii::app()->end();
        }  catch (Exception $e){
            echo 'NO';
            Yii::app()->end();
        }
        
    }
    /**
     * Отображается результат оплаты заказа. Заказ берется из сессии пользователя.
     * Выводятся билеты
     */
    public function actionResult($id){
        Yii::app()->language = 'uk';
        $purchase = Purchase::model()->complete()->findByPk($id);
        if($purchase){
            $this->layout = 'print';
            $this->render('result', array('documents' => $purchase->getDocuments()));
        }
    }

    /**
     * Отмена заказа
     * @param String $id идентификатор заказа
     * @throws CHttpException
     */
    public function actionCancel($id){
        $order = Order::model()->findByPk($id);
        if(!$order)
            throw new CHttpException(404, Yii::t('app', 'Страница не найдена'));
        
        
        foreach ($order->products as $product) {
            if($product->type == 'transportation')
                continue;
            try{
                $this->cancel($product->transaction_id);
            }  catch (Exception $e){
                continue;
            }
        }
        $purchase = $order->purchase;
        $order->cancel();
        $purchase->total -= $order->total;
        $purchase->save();
        $order->save();
        $this->redirect($this->createUrl('orders'));
        
    }
    /**
     * Обрабатывает форму с данными пользователя, генерирует форму
     * и тут же отправляет её на сервис оплаты
     */
    public function actionPay(){
        if(isset($_POST['ConfirmForm'])){
            $model = new ConfirmForm;
            $model->attributes = $_POST['ConfirmForm'];
            $purchase = Purchase::model()->findByPk(Yii::app()->session['purchase_id']);
            $user = Users::model()->find('email= :email', array(':email' => $model->email));
            if(!$user){
                $user = new Users();
                $user->email = $model->email;
                $user->save();
            }
            $purchase->user_id = $user->id;
            $purchase->save();
            $this->layout = '//layouts/column1';
            Yii::app()->clientScript->registerCoreScript('jquery');
            $this->render('pay', array('purchase' => $purchase));
        }
    }
    public function actionTerms(){
        $this->layout = '/layouts/terms';
        $this->render('terms');
    }

    /**
     * Подгружает дополнительный контент, для элемента корзины, 
     * в котором можно указать ручную кладь
     * @throws CHttpException
     */
    public function actionAdditional(){
        $this->renderPartial('/tmpls/_additional', array('place' => $this->getParam('place')));
    }
    
    /**
     * Отображает заказ
     * @param int $id идентификатор заказа
     */
    public function actionOrders(){
        $this->layout = '/layouts/order';
        $model = new ConfirmForm();
        $cart = $this->_loadPurchase();
        if(!$cart->orders){
            Yii::app()->request->redirect(Yii::app()->createUrl('site/index'));
        }
        
        $this->render('reserve', array('cart' => $cart, 'model' => $model));
    }
    /**
     * Отображает один заказ
     * @param int $id идентификатор заказа
     */
    public function actionOrder($id){
        $this->layout = '/layouts/order';
        $cart = Purchase::model()->new()->findByPk($id);
        if(!$cart || !$cart->orders){
            throw new CHttpException(404);
        }
        $model = new ConfirmForm();
        $this->render('reserve', array('cart' => $cart, 'model' => $model));
    }

    /**
     * Разбивает заказанные места
     * по типам: reserve, booking
     * @return Array
     */
    private function _getTypesPlaces(){
        $places = array();
        foreach ($this->getParam('Places') as $n => $p) {
            $places[$p['type']][$n] = $p;
        }
        return $places;
    }

    /**
     * Создает заказа на основании
     * списка мест, поезда и вагона сохранненного
     * в сессии
     * @return \Order
     */
    private function _loadOrder(){
        $order = new Order();
        $order->setStorage($this);
        $order->setTransport($this);
        $order->addPlaces($this->getParam('Places'));
        $order->direction = $this->getParam('direction');
        
        foreach ($this->getParam('routes') as $route){
            if($this->getParam('train') == $route->train->number){
                $order->train = $route->train;
                break;
            }
        }
        foreach ($this->getParam('wagons') as $wagon){
            if($this->getParam('wagon_number') == $wagon->number){
                $order->wagon = $wagon;
                break;
            }
        }
        return $order;
    }
    /**
     * Возвращает объект закупки
     * @return \Purchase
     */
    private function _loadPurchase(){
        $id =  Yii::app()->session['purchase_id'];
        if($purchase = Purchase::model()->new()->findByPk($id)){
            return $purchase;
        }
        return new Purchase();
    }
}
