<?php
Yii::import('booking.classes.*');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TestController
 *
 * @author kilim
 */
class TestController extends RailController{
    
    
    public function actionReserve(){
        $direction = new Direction;
        $direction->from_code = '2210700';
        $direction->to_code = '2200001';
        $direction->date1 = '2015-08-26';
        $this->setParam('direction', $direction);
        $this->setParam('train', '733Ш');
        $this->setParam('wagon_number', '05');
        $this->setParam('wagon_type', 'С');
        $this->setParam('wagon_class', '1');
        $places = array();
        $places['Places'] = array();
        $places['Places']['023'] = array('type' => 'reserve', 'kind' => 'full', 'firstname' => 'Иван', 'lastname' => 'Иванов');
        $_POST['Places'] = $places['Places'];
        $transaction = TransactionsBuilder::build('reserve', $this, $this);
        try{
            echo $this->request($transaction); exit;
        }  catch (Exception $e){
            echo $e->getMessage(); exit;
        }
    }
    public function actionRequest($source, $url = null){
	if(!file_exists(Yii::getPathOfAlias(Settings::Get('data', 'booking')).DS.'keys'.DS."10212.crt"))
            throw new CHttpException(400, 'File not found');
	
        $url =  $url == null ? Settings::Get('uri', 'booking') : $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: text/xml; charset="utf-8"'
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, "ООО Лекорн"); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $source);
        $response = curl_exec($ch);
        echo $response; exit;
        if (curl_errno($ch)) {
            echo curl_error($ch);
        }
        return $response;
    }
    public function actionVerify(){
        echo Yii::app()->request->userHostAddress;
        echo Yii::app()->request->requestUri;
        exit;
    }

    public function actionStatus(){
        $transaction = TransactionsBuilder::build('state', $this, $this);
        $result = $transaction->sign()->submit();
        echo $result;
    }
    
    public function actionCancel($id){
        $this->setParam('reserve_id', $id);
        $transaction = TransactionsBuilder::build('cancel', $this, $this);
        $result = $transaction->sign()->submit();
        echo implode(',', $result);
    }
}
