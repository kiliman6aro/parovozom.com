<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RailController
 *
 * @author Администратор
 */
class RailController extends Controller implements IStorageManager, ITransport{
    
    public $layout = '/layouts/column2';
    
    public $request;
    
    public $transport;
    
    public function init() {
        $this->setParam('merchant_id', Settings::Get('mecrhant_id', 'booking'));
        $this->setParam('datetime', date('Y-m-d H:i:s'));
        $this->setParam('language', Yii::app()->language);
        $this->setParam('key', Yii::getPathOfAlias(Settings::Get('data', 'booking')).DIRECTORY_SEPARATOR.'keys'.DIRECTORY_SEPARATOR.Settings::Get('key', 'booking'));
    }
    
    /**
     * Возвращает значение параметра из сессии
     * @todo переписать данный метод более логично
     * @param String $name имя параметра
     * @param bool $save возвращает переменную прямо из потока ввода
     * @param bool $rewrite перезаписывать значение переменной в кеше
     * @return String значение искомого параметра
     */
    public function getParam($name, $save = true, $rewrite = true) {
        if(!$save){
            return Yii::app()->request->getParam($name);
        }
        if($rewrite && Yii::app()->request->getParam($name)){
            $this->setParam($name, Yii::app()->request->getParam($name));
        }
        return Yii::app()->session[$name];
    }

    public function setParam($name, $value) {
        Yii::app()->session[$name] = $value;
    }
    
    public function getTrainByNumber($number) {
        foreach ($this->getParam('routes') as $route){
            if($route->train->number == $number){
                return $route->train;
            }
        }
    }

    public function getWagonByNumber($number) {
        foreach ($this->getParam('wagons') as $wagon) {
            if ($number == $wagon->number) {
                return $wagon;
            }
        }
    }

    public function request(BaseTransaction $transaction) {
        $url = Settings::Get('uri', 'booking');
        if($url == 'localhost'){
            $response = $this->_read($transaction->getAction());
        }else{
            $response = $this->_query($url, $transaction);
        }
        $result = $transaction->handling($response);
        $log  = new TransactionsLog;
        $log->action = $transaction->getAction();
        $log->url = $url;
        $log->transaction_id = $transaction->getID();
        $log->request = $transaction->getRequest();
        $log->response = $response;
        if($transaction->hasError()){
            $log->status = 'error';
            $log->error = $transaction->getError()->message;
            $log->save();
            throw new TransactionException($transaction->getError()->message, $transaction->getError()->code);
        }
        $log->save();
        return $result;
    }
    
    /**
     * Выполняет транзакцию транспортировки
     * @param type $uid идентификатор документа
     * @param type $type тип груза
     * @param type $weight вес
     * @return Product
     */
    public function transportation($uid, $type, $weight = null) {
        $this->setParam('kind', $type);
        $this->setParam('uid', $uid);
        if ($weight) {
            $this->setParam('weight', $weight);
        }
        $transaction = TransactionsBuilder::build('transportation', $this, $this);
        return $transaction->sign()->submit();
    }
    
    /**
     * Отмена заказа
     * @param string $reserve_id 
     * @return type
     */
    public function cancel($reserve_id){
        $this->setParam('reserve_id', $reserve_id);
        $transaction = TransactionsBuilder::build('revocation', $this, $this);
        return $transaction->sign()->submit();
    }

    /**
     * Отправить билеты на почту.
     * @param String $email на какой e-mail отправлять
     * @param String $output текстовое представление HTMl
     */
    protected function sendDocumentsToEmail($email, $documents){
        $this->layout = 'email';
        $html = $this->render('result', array('documents' => $documents), true);
        
        $mail = Yii::app()->smtpmail;
        $mail->SetFrom(Settings::Get('username', 'contact'));
        $mail->Subject = 'parovozom.com: Билеты';
        $mail->MsgHTML(BookingModule::t('app', 'Вы заказали и опалитили билеты, они во вложении, распечатайте их'));
        $mail->AddAddress($email);
        $mail->AddStringAttachment($html, 'tickets.html');
        $mail->send();
    }

    private function _read($action){
        $alias = Settings::Get('data', 'booking');
        $file = Yii::getPathOfAlias($alias).DS.'responses'.DS.$action.'.xml';
        return file_get_contents($file);
    }
    
    private function _query($url, BaseTransaction &$transaction){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: text/xml; charset="utf-8"'
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $transaction->getRequest());
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            $transaction->setError(500, curl_error($ch));
        }
        return $response;
    }
    
    
    
}
