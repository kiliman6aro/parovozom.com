<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pavel Bhilick
 */
interface IFormatterDelegate {
    
    public function formatterTrains($array);
    public function formatterPrices($array, IPriceFomatterDelegate $delegate);
    public function formatterPlaces($array);
    public function formatterReserve($array, IPriceFomatterDelegate $delegate);
    public function formatterTransportation($array, IPriceFomatterDelegate $delegate);
    public function formatterPay($array, IPriceFomatterDelegate $delegate);
    public function formatterCancel($array);
    public function formatterRevocation($array);
    public function formatterReserveRoundtrip($array, IPriceFomatterDelegate $delegate);
}
