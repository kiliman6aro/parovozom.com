<?php
/**
 * Интерфейс запросов. Описывает интерфейс
 * по полному циклу построения и обработки запросов, для 
 * дальнейшей передачи его вэб сервису. 
 * @package booking.interfaces
 * @author Билык Павел
 */
interface IRequest{
    
    public function build($action);

    public function signature($sign);
    
    public function handler($source);
    
    public function getSource();
    
    public function getAction();
    
    public function getId();
    
}
