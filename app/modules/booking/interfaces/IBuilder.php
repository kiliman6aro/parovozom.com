<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Администратор
 */
interface IBuilder {
    
    public function __construct(IStorageManager $store);


    public function build(DOMDocument $dom, $action, $id);
    
    public function parse(DOMDocument $dom);
}
