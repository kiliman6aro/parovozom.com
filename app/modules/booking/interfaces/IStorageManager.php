<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Администратор
 */
interface IStorageManager {
    
    public function getParam($name, $cache = true);
    
    public function setParam($name, $value);
    
}
