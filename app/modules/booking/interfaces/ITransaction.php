<?php

/**
 * Определяет интерфейс для любого типа транзакций. 
 * Хоть это будет XML, JSON, не имеет значения. Каждый
 * тип транзакций должен релазовать данный интерфейс
 * @author Pavel B.
 */
interface ITransaction {
    
    public function setStorage(IStorageManager $storage);
    
    public function setResponse($source);
    
    public function hasError();
    
    public function getError();
    
    public function setError($code, $message);

    public function getAction();
    
    public function getID();
    
    public function getRequest();
    
    public function getResponse();

    public function submit();
    
    public function build();
    
    public function sign();


    public function handling($source);
}
