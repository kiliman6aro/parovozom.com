<?php

class PaymentController extends CController {
    
    /**
     * @todo доделать проверку подписи при оплате
     */
    public function actionVerify(){
        $signature = $_POST['signature'];
        Yii::log('Signature: '.$signature);
        Yii::log(CJSON::encode($_POST));
        file_put_contents(Yii::getPathOfAlias('application.runtime').DIRECTORY_SEPARATOR.'log.txt', CJSON::encode($_POST));
        $purchase = Purchase::model()->new()->findByPk(Yii::app()->request->getParam('order_id'));
        $result = $purchase->verify(Yii::app()->request->getParam('signature'), $_POST);
        $purchase->pay();
        $purchase->save();
        /*if($result==1) {
            //$this->updateCart('pay');
            $purchase->pay();
            $purchase->save();
            Yii::log('Ok');
            echo 'ok';
        }
        elseif($result == 0){
            $purchase->error();
            $purchase->save();
            Yii::log('Bad!');
            file_put_contents(Yii::getPathOfAlias('application.runtime').DIRECTORY_SEPARATOR.'error.txt', openssl_error_string());
            echo 'bad';
            
        } 
        else {
            $purchase->error();
            $purchase->save();
            Yii::log('error checking signature!');
            file_put_contents(Yii::getPathOfAlias('application.runtime').DIRECTORY_SEPARATOR.'error.txt', 'error checking signature');
            echo 'error checking signature'; 
        }*/

    }
    public function actionPay($price, $description){
        echo Yii::app()->payment->generate(md5(microtime()), $price, $description);
        echo "<script>window.onload = function(){document.querySelector('#payment').submit();}</script>";
        Yii::app()->end();
    }
    /**
     * Обновляет статус записией в корзине
     * @param type $state
     * @return boolean
     */
    protected function updateCart($state) {
        return Cart::model()->updateAll(array('state' => $state), 'session_id = :session_id', array(':session_id' => Yii::app()->session->getSessionID()));
    }
}
