<?php

/**
 * Поведение использует компонент PaymentComponent. Подключается к модели корзины
 * с целью получать данные автоматически из ней, а не указывать на прямую
 * @author Администратор
 */
class CartBehavior extends CActiveRecordBehavior {
    
    /**
     *
     * @var type Поле в таблице суммы
     */
    public $priceField = 'total';
    
    /**
     *
     * @var type Поле идентификатора
     */
    public $idField = 'id';
    
    /**
     *
     * @var type Поле где хранятся данные транзакции оплаты
     */
    public $dataField = 'data';
    
    /**
     * @var string имя файла-ключа с расширением
     */
    public $encode_key = 'merchant_id.pem';
    
    /**
     *
     * @var string имя файла для расшифровки
     */ 
    public $decode_key;
    
    /**
     * @var string идентификатор клиента на сервере поставщика API
     */
    public $merchant_id = '';
    
    /**
     * @var string url для запроса
     */
    public $url = '';
    
    /**
     *
     * @var type Путь к каталогу с ключами
     */
    public $keysPath = '';
    
    /**
     *
     * @var type Версия протокола оплаты
     */
    public $version = '3.5';
    
    /**
     *
     * @var type режим мобильной версии, включен/выключен
     */
    public $mobile = 0;
    

    /**
     * Генерирует форму-корзину
     * @return string
     */
    public function render() {
        $post = array(
            'merchant_id' => $this->merchant_id,
            'order_id' => $this->owner->{$this->idField},
            'amount' => $this->owner->{$this->priceField}*100,
            'date' => date('Y-m-d H:i:s'),
            'description' => 'Оплата заказа: ' . $this->owner->{$this->idField},
            'sd' => '',
            'version' => $this->version,
        );
        $data = implode(';', $post).';';
        $post['signature'] = $this->signature($data, $this->getPathToKey($this->encode_key));
        $post['enable_mobile'] = $this->mobile;
        $html = '<form action="'.$this->url.'" method="post" id="payment">';
        foreach($post as $k=>$v){
            $html .= '<input type="hidden" name="'.$k.'" value="'.htmlspecialchars($v).'" />';
        }
        $html .= '</form>';
        return $html;
    }

    /**
     * Подписывает данные ключём
     * @param string $data данные для подписи
     * @param string $pem путь к файлу ключу
     * @return string подпись
     */
    protected function signature($data, $pem){
        $fp = fopen($pem, 'r');
        $priv_key = fread($fp, 8192);
        fclose($fp);
        $pkeyid = openssl_pkey_get_private($priv_key);
        openssl_sign($data, $signature, $pkeyid);
        openssl_free_key($pkeyid);
        return base64_encode($signature);
    }
    
    /**
     * Проверяет ответ
     */
    public function verify($data) {
        $sign = $data['signature'];
        unset($data['signature']);
        $signature = base64_decode($sign);
        // извлечь сертификат 
        $fp = fopen($this->getPathToKey($this->decode_key), "r");
        $cert = fread($fp, 8192);
        fclose($fp);

        $pubkeyid = openssl_get_publickey($cert);
        $data = $this->prepareResponse($data);
        // проверка подписи 
        $result = openssl_verify($data, $signature, $pubkeyid);
        openssl_free_key($pubkeyid);
        return $result;
    }

    /**
     * Возвращает файл ключа
     * @param type $filename имя файла с расширением
     * @return полный путь к файлу
     */
    protected function getPathToKey($filename){
        $path = $this->keysPath.DS.$filename;
        if (!file_exists($path)) {
            throw new CHttpException(500, BookingModule::t('app', 'Файл {path} не найден',array('{path}' => $path)));
        }
        return $path;
    }
    
    /**
     * Подготавливает строку для сверхи ответа
     * @param array $post данные из супер массива $_POST
     * @return string order_id;date;sd;status;text; 
     */
    protected function prepareResponse($post){
        return implode(';', $post).';';
    }
}
