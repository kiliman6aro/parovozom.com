<?php


class PaymentComponent extends CApplicationComponent {
    
    /**
     * @var string имя файла-ключа с расширением
     */
    public $encode_key = 'merchant_id.pem';
    
    /**
     *
     * @var string имя файла для расшифровки
     */ 
    public $decode_key;
    
    /**
     * @var string идентификатор клиента на сервере поставщика API
     */
    public $merchant_id = '';
    
    /**
     * @var array заголовки которые отправляются вместе с запросом на сервер
     * поставщика API
     */
    public $headers = array(
        'Content-type: application/x-www-form-urlencoded;'
    );
    
    /**
     * @var string url для запроса
     */
    public $url = '';
    
    /**
     * @var string полный путь к файлу-ключу. Создается при выполнении метода {@link BookingComponent::init()}
     */
    protected $key = '';
    
    /**
     *
     * @var string данные, на которые был сгенерирована подписб
     * Используется для проверки подписи 
     */
    protected $data;
    
    
    
    public function init() {
        $this->url = Settings::Get('uri', 'payment');
        $this->merchant_id = Settings::Get('merchant_id', 'payment');
        $this->encode_key = Settings::Get('encode_key', 'payment');
        $this->decode_key = Settings::Get('decode_key', 'payment');
        
        parent::init();
    }
    
    public function generate($id, $ammount, $description) {
        $post = array(
            'merchant_id' => $this->merchant_id,
            'order_id' => $id,
            'amount' => $ammount*100,
            'date' => date('Y-m-d H:i:s'),
            'description' => $description,
            'sd' => 'merchant_id;order_id;amount;date;description;sd;version;',
            'version' => '3.5',
        );
        $post['signature'] = $this->signature($post, $this->getPathToKey($this->encode_key));
        $post['enable_mobile'] = '1';
        $html = '<form action="'.$this->url.'" method="post" id="payment">';
        foreach($post as $k=>$v){
            $html .= '<input type="hidden" name="'.$k.'" value="'.htmlspecialchars($v).'" />';
        }
        $html .= '</form>';
        return $html;
    }
    /**
     * Проверка ответа
     * @param string $signature подпись
     * @param string $data строка данных, которая была подписана
     * @return int
     */
    public function verify($signature, $data){
        $signature = base64_decode($signature);
        // извлечь сертификат 
        $fp = fopen($this->getPathToKey($this->decode_key), "r"); 
        $cert = fread($fp, 8192); 
        fclose($fp); 

        $pubkeyid = openssl_get_publickey($cert); 
        // проверка подписи 
        $result = openssl_verify($data, $signature, $pubkeyid); 
        openssl_free_key($pubkeyid);
        return $result;
    }
    

    /**
     * Подписывает документ
     */
    protected function signature($post, $pem) {
        
        $this->data = '';
        foreach($post as $v){
           $this->data .= $v.';'; 
        }
        $fp = fopen($pem, 'r');
        $priv_key = fread($fp, 8192);
        fclose($fp);
        $pkeyid = openssl_pkey_get_private($priv_key);
        openssl_sign($this->data, $signature, $pkeyid);
        openssl_free_key($pkeyid);
        return base64_encode($signature);
    }
    protected function prepareResponse($params){
        $data = '';
        foreach ($params as $key => $value) {
            if($key != 'signature'){
                $data .=$value.';';
            }
        }
        return $data;
    }
    /**
     * Возвращает файл ключа
     * @param type $filename имя файла с расширением
     * @return полный путь к файлу
     */
    protected function getPathToKey($filename){
        $path = Yii::getPathOfAlias(Settings::Get('data', 'booking')).DS.'keys'.DS.'plategka'.DS.$filename;
        if (!file_exists($path)) {
            throw new CHttpException(500, BookingModule::t('app', 'Файл {path} не найден',array('{path}' => $path)));
        }
        return $path;
    }

}

