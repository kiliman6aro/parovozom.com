<?php

class BServiceModel {
    
    protected $service;
    
    protected $tbl_log = '{{booking_transactions_log}}';
    
    protected $tbl_shemes = '{{schemes}}';
    
    protected $logging = false;
    
    protected $lang = 'ru';
    
    protected $config = array();
    
    protected $delegateProcessing;
    
    protected $delegateFormatter;
    
    protected $deletagePriceFormatter;


    /**
     * Подключает API УЗ
     * @param type $uri http адресс запроса
     * @param type $merchant_id идентификатор агента
     * @param type $key имя ключа
     * @param type $logging ввести логи запросов к API
     * @param type $lang язык ответа от API
     */
    public function __construct($uri, $merchant_id, $key, $logging = true, $lang = 'RU') {
        
        $path = Yii::getPathOfAlias('booking.components.transport').DIRECTORY_SEPARATOR.'bootstrap.php';
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        require_once $path;
        spl_autoload_register(array('YiiBase', 'autoload'));
        
        $config = array(
            'mode' => $mode,
            'lang' => $lang,
            'processing' => array(
                'uri' => $uri,
                'merchant_id' => $merchant_id,
                'key' => $key
            )
        );
        $this->logging = $logging;
        $this->lang = strtolower($lang);
        $this->setConfig($config);
    }

    /**
     * Формирует запрос к API и выполняет его
     * @param type $from код станции откуда
     * @param type $to код станции куда
     * @param type $date дата отправления
     * @return type
     */
    public function trains($from, $to, $date){
        try{
           $this->getService()->trainsExtended($from, $to, $date);
        }catch(BBuilderException $e){
            throw new BBuilderException($e->getMessage());
        }
       return $this->delegateFormatter->formatterTrains($this->execute());
    }

    public function prices($from, $to, $date, $train){
        try{
           $this->getService()->prices($from, $to, $date, $train);
        }catch(BBuilderException $e){
            throw new BBuilderException($e->getMessage());
        }
        return $this->delegateFormatter->formatterPrices($this->execute(), $this->deletagePriceFormatter);
    }
    /**
     * 
     * @param type $from
     * @param type $to
     * @param type $date
     * @param type $train
     * @param type $wagon_number
     * @param type $wagon_type
     * @param type $wagon_class
     * @return type
     * @throws CHttpException
     */
    public function places($from, $to, $date, $train, $wagon_number, $wagon_type, $wagon_class = null) {
        try {
            $this->getService()->places($from, $to, $date, $train, $wagon_type, $wagon_number, $wagon_class);
            return $this->delegateFormatter->formatterPlaces($this->execute());
        } catch (BResultErrorHttpProcessingException $e) {
            //Несли при типе С и классе 1 появляются ошибки 25 или 60 то надо справку places c типом вагона Л
            //пробуем повторить транзакцию с типом Л. Если снова ошибка - то выбросить исключение
            if (($e->getCode() == 25 || $e->getCode() == 60) && ($wagon_type == 'С' && $wagon_class == '1')) {
                return $this->places($from, $to, $date, $train, $wagon_number, 'Л');
            }
            throw new CHttpException($e->getCode(), $e->getMessage());
        } catch (Exception $e) {
            throw new CHttpException(500, $e->getMessage());
        }
    }
    public function reserve($from, $to, $startDate, $train, $wagon_type, $wagon_number, $places, $documents, $wagon_class = null, $no_bedding = null, $services = null, $no_electronic = null) {
        try{
            $this->getService()->reserve($from, $to, $startDate, $train, $wagon_type, $wagon_number, $places, $documents, $wagon_class, $no_bedding, $services, $no_electronic);
            return $this->delegateFormatter->formatterReserve($this->execute(), $this->deletagePriceFormatter);
        }  catch (BBuilderException $e){
            throw new CHttpException(500, $e->getMessage());
        } catch(Exception $e){
            throw new CHttpException(500, $e->getMessage());
        }
    }
    public function pay($reserve_id, $transaction_type, $binary = null, $qrcode = null, $barcode = null) {
        try {
            $this->getService()->pay($reserve_id, $transaction_type, $binary, $qrcode, $barcode);
            return $this->delegateFormatter->formatterPay($this->execute(), $this->deletagePriceFormatter);
        } catch (BBuilderException $e) {
            throw new CHttpException(500, $e->getMessage());
        } catch (Exception $e) {
            throw new CHttpException(500, $e->getMessage());
        }
    }
    public function transportation($uid, $kind, $weight = null){
        try{
            $this->getService()->transportation($uid, $kind, $weight);
            return $this->delegateFormatter->formatterTransportation($this->execute(), $this->deletagePriceFormatter);
        }catch (Exception $e){
            throw new CHttpException(500, $e->getMessage());
        }
        
    }
    public function revocation($reserve_id){
        try{
            $this->getService()->revocation($reserve_id);
            return $this->delegateFormatter->formatterRevocation($this->execute());
        }catch (Exception $e){
            throw new CHttpException(500, $e->getMessage());
        }
    }
    public function cancel($reserve_id){
        try{
            $this->getService()->cancel($reserve_id);
            return $this->delegateFormatter->formatterCancel($this->execute());
        }catch (Exception $e){
            throw new CHttpException(500, $e->getMessage());
        }
    }
    public function booking($from, $to, $startDate, $train, $wagon_type, $wagon_number, $places, $documents, $wagon_class = null){
        try{
            $this->getService()->booking($from, $to, $startDate, $train, $wagon_type, $wagon_number, $places, $documents, $wagon_class);
            return $this->execute();
        }catch (Exception $e){
            throw new CHttpException(500, $e->getMessage());
        }
    }

    /**
     * Выполняет запрос к API УЗ, обрабатывает исключения, ведет логи
     * @return type
     * @throws BResultErrorHttpProcessingException API УЗ ответил, но вернул в своем ответе ошибку.
     * @throws CHttpException исключения связанные с доступом к API
     */
    public function execute(){
        $service = $this->getService();
        try{
            $service->query();
            $this->log($service->getID(), $service->getRealTransactionName(), $service->getRequest(), $service->getResponse());
        }catch(BResultErrorHttpProcessingException $e){            
            $this->log($service->getID(), $service->getRealTransactionName(), $e->getRequest(), $e->getResponse(), 'warning', $e->getCode(), $e->getMessage());
            throw $e;
        }catch(BHttpProcessingException $e){
            $this->log($service->getID(), $service->getRealTransactionName(), $service->getRequest(), null, 'error');
            throw new CHttpException(500, $e->getMessage());
        }catch(BFileExistException $e){
            $this->log($service->getID(), $service->getRealTransactionName(), $service->getRequest(), null, 'error');
            throw new CHttpException(500, $e->getMessage());
        }
        return $this->delegateProcessing->handing($service->getRealTransactionName(), $service->getResponse());
    }
    /**
     * Возвращает схему вагона, исходя из переданных параметров
     * @param type $type
     * @param type $class
     * @return type
     */
    public function getScheme($number, $type, $class = null){
        $where = array('and', "type = '".$type."'");
        if ($class) {
            $where[] = array("class = '".$class."'");
        }
        $scheme = Yii::app()->db->createCommand()
                ->select('scheme')
                ->from($this->tbl_shemes)
                ->where($where)
                ->queryScalar();
        if(!$scheme){
            //бросить исключение
        }
        return str_replace('{number}', $number, $scheme);
    }

    /**
     * Генерирует шаблоны для билетов с переводом
     * @return mixed
     */
    public function getTemplates(){
        $ticket_item = array(
            'mask' => array(
                    '%coach%', '%place%', '%Buy%', '%Booking%', '%Full%', '%Child%', '%Birthday%', 
                    '%Transportation baggage%', '%USD%', '%Apparatus%', '%Animals%', '%Luggage%',
                    '%Enter the date of birth%','%You must enter a name%','%You must enter a surname%',
                    '%You have not selected the type of baggage%','%You have not entered weight%', '%Weight%',
                    '%First Name%', '%Last Name%'
                ),
            'values' => array(
                    Yii::t('app', 'coach'), Yii::t('app', 'place'), Yii::t('app', 'Buy'), Yii::t('app', 'Booking'), Yii::t('app', 'Full'), 
                    Yii::t('app', 'Child'), Yii::t('app','Birthday'), Yii::t('app', 'Transportation baggage'), Yii::t('app', 'USD'), 
                    Yii::t('app', 'Apparatus'),Yii::t('app', 'Animals'),Yii::t('app', 'Luggage'), Yii::t('app', 'Enter the date of birth'),
                    Yii::t('app', 'You must enter a name'), Yii::t('app', 'You must enter a surname'), Yii::t('app', 'You have not selected the type of baggage'),
                    Yii::t('app', 'You have not entered weight'), Yii::t('app', 'Weight'), Yii::t('app', 'First Name'), Yii::t('app', 'Last Name')
                ), 
        );
        $ticket_total = array(
            'mask' => array('%Cancel%', '%Total%', '%USD%', '%Reserve%'),
            'values' => array(Yii::t('app', 'Cancel'), Yii::t('app', 'Total'), Yii::t('app', 'USD'), Yii::t('app', 'Reserve'))
        );
        $tickets_block = array(
            'mask' => array('%Tickets%', '%Be sure to enter your name and the name of the passenger, who will carry out the train%'),
            'values' => array(Yii::t('app', 'Tickets'), Yii::t('app', 'Be sure to enter your name and the name of the passenger, who will carry out the train'))
        );
        $tmpls = array(
            'ticket_item' => str_replace($ticket_item['mask'], $ticket_item['values'], Yii::app()->config->get('BOOKING.TICKET_ITEM')),
            'ticket_total' => str_replace($ticket_total['mask'], $ticket_total['values'], Yii::app()->config->get('BOOKING.TOTAL')),
            'tickets_block' => str_replace($tickets_block['mask'], $tickets_block['values'], Yii::app()->config->get('BOOKING.TICKETS_BLOCK')),
            'tooltip' => Yii::app()->config->get('BOOKING.ATTENTION_TOOLTIP'),
            'child_calendar_title' => "<div class='title'>".Yii::t('app', 'Select the date of birth of the child ({start}-{end} years)', array('{start}' => 6, '{end}' => 14)).":</div>"
        );
        return $tmpls;
    }

    public function setDelegateProcessing(IResultDelegate $delegate){
        $this->delegateProcessing = $delegate;
    }
    public function setDelegatePriceFormatter(IPriceFomatterDelegate $delegate){
        $this->deletagePriceFormatter = $delegate;
    }


    public function setDelegateFormatter(IFormatterDelegate $delegate){
        $this->delegateFormatter = $delegate;
    }

    /**
     * Возвращает идентификатор транзакции
     */
    public function getID(){
        return $this->getService()->getID();
    }
    /**
     * Возвращает имя транзакции
     * @return type
     */
    public function getRealTransactionName(){
        return $this->getService()->getRealTransactionName();
    }

    protected function getService(){
        return !$this->service ? $this->service = new BAPIService($this->config) : $this->service;
    }
    
    protected function setConfig($config){
        $this->config = $config;
    }
    protected function log($id, $name, $request, $response = null, $type = 'success', $error_code = null, $error_message = null){
        /*if(!$this->logging){
            return false;
        }
        Yii::app()->db->createCommand()
                ->insert($this->tbl_log, array(
                    'uid' => $id,
                    'name' => $name,
                    'type' => $type,
                    'request' => $request,
                    'response' => $response,
                    'error_code' => $error_code,
                    'error_message' => $error_message
                ));
        return true;*/
    }
}
