<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XMLPricesTransactionBuild
 *
 * @author Администратор
 */
class XMLPricesTransactionBuild implements IBuilder{
    
    private $_store;
    
    public function __construct(\IStorageManager $store) {
        $this->_store = $store;
    }

    public function build(\DOMDocument $dom, $action, $id) {
        $transaction = $dom->createElement('transaction');
        $transaction->setAttribute('id', $id);
        $transaction->setAttribute('type', $action);
        
        $direction = $this->_store->getParam('direction');
        $station_to = $dom->createElement('code_station_to', $direction->to_code);
        $station_from = $dom->createElement('code_station_from', $direction->from_code);
        $date = $dom->createElement('date', $direction->date1);
        $number = $dom->createElement('train', $this->_store->getParam('train'));
        
        $transaction->appendChild($station_from);
        $transaction->appendChild($station_to);
        $transaction->appendChild($date);
        $transaction->appendChild($number);
        return $transaction;
    }

    public function parse(\DOMDocument $dom) {
        $items = array();
        $wagon_type = $this->_store->getParam('wagon_type');
        $wagon_class = $this->_store->getParam('wagon_class');
        foreach ($dom->getElementsByTagName('wagon') as $node){
            $xml = new DOMDocument;
            $xml->loadXML($node->ownerDocument->saveXML($node));
            $number = $xml->getElementsByTagName('number')->item(0)->nodeValue;
            if(isset($items[$number])){
                $wagon = $items[$number];
                
                $wagon->prices[$xml->getElementsByTagName('charline')->item(0)->nodeValue] = array(
                    'cost' => $xml->getElementsByTagName('cost')->item(0)->nodeValue,
                    'costreserve' => $xml->getElementsByTagName('costreserve')->item(0)->nodeValue,
                    'currency' => $wagon->currency
                );
                
                $lower = $xml->getElementsByTagName('lower')->item(0)->nodeValue;
                $lower_side = $xml->getElementsByTagName('lower')->item(0)->attributes->getNamedItem('side')->nodeValue;
                $top = $xml->getElementsByTagName('top')->item(0)->nodeValue;
                $top_side = $xml->getElementsByTagName('top')->item(0)->attributes->getNamedItem('side')->nodeValue;
                $wagon->places += $lower += $lower_side += $top += $top_side;
                
                $wagon->cost = $wagon->cost <= $xml->getElementsByTagName('cost')->item(0)->nodeValue ? $wagon->cost : $xml->getElementsByTagName('cost')->item(0)->nodeValue;
                continue;
            }
            $wagon = new Wagon();
            $wagon->number = $number;
            if($xml->getElementsByTagName('class')->length > 0){
                $wagon->class = $xml->getElementsByTagName('class')->item(0)->nodeValue;
                $wagon->class_code = $xml->getElementsByTagName('class')->item(0)->attributes->getNamedItem('code')->nodeValue;
            }
            $wagon->type = $xml->getElementsByTagName('type')->item(0)->nodeValue;
            $wagon->type_code = $xml->getElementsByTagName('type')->item(0)->attributes->getNamedItem('code')->nodeValue;
            $wagon->cost = $xml->getElementsByTagName('cost')->item(0)->nodeValue;
            $wagon->currency = $xml->getElementsByTagName('cost')->item(0)->attributes->getNamedItem('currency')->nodeValue;
            
            $lower = $xml->getElementsByTagName('lower')->item(0)->nodeValue;
            $lower_side = $xml->getElementsByTagName('lower')->item(0)->attributes->getNamedItem('side')->nodeValue;
            $top = $xml->getElementsByTagName('top')->item(0)->nodeValue;
            $top_side = $xml->getElementsByTagName('top')->item(0)->attributes->getNamedItem('side')->nodeValue;
            $wagon->places += $lower += $lower_side += $top += $top_side;
            
            $wagon->prices[$xml->getElementsByTagName('charline')->item(0)->nodeValue] = array(
                'cost' => $wagon->cost,
                'costreserve' => $xml->getElementsByTagName('costreserve')->item(0)->nodeValue,
                'currency' => $wagon->currency
            );
            //Добавлять только те которые соответсвутют параметрам
            if($wagon_class && $wagon_class != $wagon->class){
                continue;
            }
            if($wagon_type && $wagon_type != $wagon->type_code){
                continue;
            }
            $items[$wagon->number] = $wagon;
        }
        return array_values($items);
    }

}
