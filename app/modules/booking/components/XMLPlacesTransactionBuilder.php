<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XMLPlacesTransactionBuilder
 *
 * @author Администратор
 */
class XMLPlacesTransactionBuilder implements IBuilder{
    
    private $_storage;
    
    public function __construct(\IStorageManager $store) {
        $this->_storage = $store;
    }

    public function build(\DOMDocument $dom, $action, $id) {
        $transaction = $dom->createElement('transaction');
        $transaction->setAttribute('id', $id);
        $transaction->setAttribute('type', $action);
        
        $direction = $this->_store->getParam('direction');
        $station_to = $dom->createElement('code_station_to', $direction->to_code);
        $station_from = $dom->createElement('code_station_from', $direction->from_code);
        $date = $dom->createElement('date', $direction->date1);
        $number = $dom->createElement('train', $this->_store->getParam('train'));
        $type = $dom->createElement('wagon_type', $this->_storage->getParam('wagon_type'));
        $wagon_number = $dom->createElement('wagon_number', $this->_storage->getParam('wagon_number'));
        $transaction->appendChild($station_from);
        $transaction->appendChild($station_to);
        $transaction->appendChild($date);
        $transaction->appendChild($number);
        $transaction->appendChild($type);
        $transaction->appendChild($wagon_number);
        if($this->_storage->getParam('wagon_type') == 'С'){
            $wagon_class = $dom->createElement($this->_storage->getParam('wagon_class'));
            $transaction->appendChild($wagon_class);
        }
        return $transaction;
    }

    public function parse(\DOMDocument $dom) {
        
    }

}
