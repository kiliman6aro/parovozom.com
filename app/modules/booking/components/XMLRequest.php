<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XMLRequest
 *
 * @author Администратор
 */
class XMLRequest implements IRequest {

    public $response;
    protected $dom;
    protected $request;
    private $_builders = array();
    private $_store;
    private $_action;
    private $_id;

    public function __construct(IStorageManager $store) {
        $this->dom = new DOMDocument('1.0', 'utf-8');
        $datetime = $this->dom->createElement('datetime', $store->getParam('datetime'));
        $this->request = $this->dom->createElement('request');
        $merchant = $this->dom->createElement('merchant_id', $store->getParam('merchant_id'));
        $language = $this->dom->createElement('language', $store->getParam('language'));
        $signature = $this->dom->createElement('xmlsign', ' ');
        $this->request->appendChild($datetime);
        $this->request->appendChild($datetime);
        $this->request->appendChild($merchant);
        $this->request->appendChild($language);
        $this->request->appendChild($signature);

        $trains = new XMLTrainsTransactionBuilder($store);
        $this->_builders = array(
            'trains extended' => $trains,
            'trains' => $trains,
            'prices' => new XMLPricesTransactionBuild($store)
        );

        $this->_id = uniqid();
        $this->_store = $store;
    }

    public function build($action) {
        $this->_action = $action;
        $t = $this->_builders[$action]->build($this->dom, $action, $this->getId());
        $this->request->appendChild($t);
        $this->dom->appendChild($this->request);
    }

    public function handler($source) {
        $this->response = $source;
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->loadXML($source);
        return $this->_builders[$this->_action]->parse($dom);
    }

    public function getSource() {
        return $this->dom->saveXML();
    }

    public function getAction() {
        return $this->_action;
    }

    public function getId() {
        return $this->_id;
    }

    public function signature($key, $pass = '') {
        $fp = fopen($key, 'r');
        $priv_key = fread($fp, 8192);
        fclose($fp);
        $pkeyid = openssl_pkey_get_private($priv_key, $pass);

        openssl_sign($this->dom->saveXML(), $signature, $pkeyid);
        openssl_free_key($pkeyid);
        $sign = base64_encode($signature);
        $this->dom->getElementsByTagName('xmlsign')->item(0)->nodeValue = $sign;
        return $this;
    }

}
