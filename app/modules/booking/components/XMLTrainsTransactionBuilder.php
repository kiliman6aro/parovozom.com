<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XMLTrainsBuilder
 *
 * @author Администратор
 */
class XMLTrainsTransactionBuilder implements IBuilder{
    
    private $_store;
    
    public function __construct(\IStorageManager $store) {
        $this->_store = $store;
    }

    
    public function build(DOMDocument $dom, $action, $id) {
        $transaction = $dom->createElement('transaction');
        $transaction->setAttribute('type', $action);
        $transaction->setAttribute('id', $id);
        $direction = $this->_store->getParam('direction');
        $station_to = $dom->createElement('code_station_to', $direction->to_code);
        $station_from = $dom->createElement('code_station_from', $direction->from_code);
        $date = $dom->createElement('date1', $direction->date1);
        
        $transaction->appendChild($station_from);
        $transaction->appendChild($station_to);
        $transaction->appendChild($date);
        return $transaction;
    }
    
    public function parse(DOMDocument $dom) {
        $routes = array();
        //Получить поезда
        foreach ($dom->getElementsByTagName('train') as $node) {
            $xml = new DOMDocument;
            $xml->loadXML($dom->saveXML($node));
            foreach ($xml->getElementsByTagName('wagon') as $wagonNode) {
                $route = new Route();
                $route->train = Train::TrainByDOM($xml);
                $route->place_count = $wagonNode->nodeValue;
                $route->wagon_type = $wagonNode->attributes->getNamedItem('type')->nodeValue;
                if($route->wagon_type == 'С'){
                    $route->wagon_class = $wagonNode->attributes->getNamedItem('class')->nodeValue;
                }
                $routes[] = $route;
            }
        }
        return $routes;
    }

}
