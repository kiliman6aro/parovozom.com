<?php
/**
 * Поведение для изменения статуса модели, в зависимости от 
 * сосотояния покупки.
 * @package booking.components.PayBehavior
 * @author kiliman6aro@gmail.com
 */
class PayBehavior extends CActiveRecordBehavior{
    
    public $statusField = 'status';
    
    public $payTimeField = 'pay_time';
    
    /**
     * Помечает статус заказа как купленный
     */
    public function pay(){
        $this->owner->{$this->statusField} = 'complete';
        $this->owner->{$this->payTimeField} = date('Y-m-d H:i:s',time());
    }
    
    /**
     * Если покупка была отменена
     */
    public function cancel(){
        $this->owner->{$this->statusField} = 'canceled';
        $this->owner->{$this->payTimeField} = date('Y-m-d H:i:s',time());
    }
    
    /**
     * Покупка привела к ошибке
     */
    public function error(){
        $this->owner->{$this->statusField} = 'error';
        $this->owner->{$this->payTimeField} = date('Y-m-d H:i:s',time());
    }
}
