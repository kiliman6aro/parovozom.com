<?php

interface IBProcessing {

    public function prepare($source, $uri, $key);

    public function execute();
}
