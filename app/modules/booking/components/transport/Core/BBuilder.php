<?php
class BBuilder {
    
    protected $id;
    
    protected $merchant_id;
    
    protected $date;
    
    protected $lang;


    public function __construct($id, $merchant_id, $lang = 'RU') {
        $this->id = $id;
        $this->merchant_id = $merchant_id;
        $this->lang = $lang;
        $this->date = date('Y-m-d H:i:s');
    }
}
