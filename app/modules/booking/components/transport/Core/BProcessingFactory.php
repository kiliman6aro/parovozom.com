<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BProcessingFactory
 *
 * @author Павел
 */
class BProcessingFactory {
    public static function factory($type) {
        switch ($type) {
            case 'localhost':
                return new BFileProcessing();
            default:
                return new BHttpProcessing();
        }
    }

}
