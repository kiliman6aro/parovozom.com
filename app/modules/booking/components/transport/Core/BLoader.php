<?php

class BLoader {

    public static function autoload($class){
        $directories = array(
            'Common',
            'Builders',
            'Exception',
            'Core'
        );
        foreach ($directories as $dir) {
            if(file_exists(BOOKING_BASE_PATH.DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR.$class.'.php')){
                include BOOKING_BASE_PATH.DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR.$class.'.php';
                break;
            }
        }
    }
}
