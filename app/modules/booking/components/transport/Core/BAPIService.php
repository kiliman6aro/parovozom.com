<?php

class BAPIService {
    
    protected $mode = 'live';


    protected $lang = 'RU';
    
    protected $processing_params = array(
        'uri' => '',
        'merchant_id' => '',
        'key' => ''
    );

    /**
     *
     * @var type имя транзакции
     */
    protected $transaction_name = '';
    
    /**
     *
     * @var type идентификатор запроса. Генерируется уникально в конструкторе
     */
    protected $id;
    
    /**
     *
     * @var type сформированный запрос, в данном случае XML
     */
    protected $request;
    
    /**
     *
     * @var type ответ от серфера, в данном случае XML
     */
    protected $response;


    public function __construct($config) {
        $this->mode = isset($config['mode']) ? $config['mode'] : 'live';
        $this->lang = isset($config['lang']) ? $config['lang'] : 'RU';
        $this->processing_params = $config['processing'];
    }
    
    public function __call($transaction, $params){
        $this->id = uniqid();
        $builder = $this->getBuilder();
        if(method_exists($builder, $transaction)){
            $this->request = call_user_func_array(array($builder, $transaction), $params);
            if($this->uri != 'localhost'){
                $sign = new BSSLXMLSignature($this->request, $this->getPathToKey());
                $this->request = $sign->signature();
            }
            $this->transaction_name = $builder->getRealTransactionName();
            return $this;
        }
        throw new BBuilderException("Unknown transaction: $transaction");
    }
    public function query(){
        $this->response = BProcessingFactory::factory($this->mode)->prepare($this->getRequest(), $this->getURI(), $this->getPathToKey())->execute();
        return $this;
    }
    
    public function getRequest(){
        return $this->request;
    }
    public function getID(){
        return $this->id;
    }

    public function getResponse(){
        return $this->response;
    }

    public function getRealTransactionName(){
        return $this->transaction_name;
    }

    protected function getURI(){
        if(isset($this->processing_params['uri'])){
            return $this->processing_params['uri'];
        }
        throw new ConfigParamsException('Not correct params for processing mode');
    }
    
    protected function getMerchantId(){
        if(isset($this->processing_params['merchant_id'])){
            return $this->processing_params['merchant_id'];
        }
        throw new ConfigParamsException('Not correct params for processing mode');
    }
    protected function getPathToKey(){
        if(isset($this->processing_params['key'])){
            $path = BOOKING_BASE_PATH.DIRECTORY_SEPARATOR.'Keys'.DIRECTORY_SEPARATOR.$this->mode.DIRECTORY_SEPARATOR.$this->processing_params['key'];
            if(!file_exists($path) && $this->uri != 'localhost'){
                throw new BFileExistException("Key file not exists on path: $path");
            }
            return $path;
        }
        throw new ConfigParamsException('Not correct params for processing mode');
    }

    protected function getBuilder(){
        return new BXMLBuilder($this->id, $this->getMerchantId(), $this->lang);
    }
}
