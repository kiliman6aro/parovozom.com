<?php

interface IBBuilder {

    /**
     * 
     * @param type $q начало имени станции
     * @return type
     */
    public function stations($q);

    /**
     * 
     * @param type $date
     */
    public function stationsList($date = null);

    /**
     * 
     * @param type $from код станции откуда
     * @param type $to код станции куда
     * @param type $startDate дата отправления YYYY-MM-DD
     */
    public function trains($from, $to, $startDate);

    /**
     * 
     * @param type $from код станции откуда
     * @param type $to код станции куда
     * @param type $startDate дата отправления YYYY-MM-DD
     */
    public function trainsExtended($from, $to, $startDate);

    /**
     * 
     * @param type $from
     * @param type $to
     * @param type $startDate
     * @param type $train
     */
    public function prices($from, $to, $startDate, $train);

    /**
     * 
     * @param type $from
     * @param type $to
     * @param type $startDate
     * @param type $train
     * @param type $wagon_type
     * @param type $wagon_number
     */
    public function places($from, $to, $startDate, $train, $wagon_type, $wagon_number, $wagon_class = null);

    /**
     * 
     * @param type $from
     * @param type $to
     * @param type $startDate
     * @param type $train
     * @param type $wagon_type
     * @param type $wagon_number
     * @param type $places
     * @param type $documents
     * @param type $wagon_class
     * @param type $no_bedding
     * @param type $services
     * @param type $no_electronic
     * @return boolean
     */
    public function reserve($from, $to, $startDate, $train, $wagon_type, $wagon_number, $places, $documents, $wagon_class = null, $no_bedding = null, $services = null, $no_electronic = null);

    /**
     * 
     * @param type $uio
     * @param type $from
     * @param type $to
     * @param type $startDate
     * @param type $train
     * @param type $train_class
     * @param type $train_model
     * @param type $train_fasted
     * @param type $train_category
     * @param type $wagon_type
     * @param type $wagon_number
     * @param type $places
     * @param type $documents
     * @param type $wagon_class
     * @param type $no_bedding
     * @param type $services
     * @param type $no_electronic
     * @return type
     * @throws CException
     */
    public function reserveRoundtrip($uio, $from, $to, $startDate, $train, $train_class, $train_model, $train_fasted, $train_category, $wagon_type, $wagon_number, $places, $documents, $wagon_class = null, $no_bedding = null, $services = null, $no_electronic = null);

    /**
     * 
     * @param type $from
     * @param type $to
     * @param type $startDate
     * @param type $train
     * @param type $wagon_type
     * @param type $wagon_number
     * @param type $places
     * @param type $documents
     * @param type $wagon_class
     * @return boolean
     */
    public function booking($from, $to, $startDate, $train, $wagon_type, $wagon_number, $places, $documents, $wagon_class = null);

    /**
     * 
     * @param type $uid
     * @param type $kind
     * @param type $weight
     * @return type
     * @throws CException
     */
    public function transportation($uid, $kind, $weight = null);

    /**
     * 
     * @param type $reserve_id
     * @param type $transaction_type
     * @param type $binary
     * @param type $qrcode
     * @param type $barcode
     */
    public function pay($reserve_id, $transaction_type, $binary = null, $qrcode = null, $barcode = null);

    /**
     * 
     * @param type $id
     * @return type
     */
    public function response($id);

    /**
     * 
     * @param type $reserve_id
     * @return type
     */
    public function revocation($reserve_id);

    /**
     * Является близнецом транзакции revocation. Потому вызывается
     * шаблон тот же, параметры те же, лишь передаетс название транзакции
     * другое
     * @param type $reserve_id
     * @return type
     */
    public function cancel($reserve_id);

    /**
     * 
     * @param type $uid
     * @return type
     */
    public function getEDoc($uid, $firstname, $lastname, $fiscal_id = null);

    /**
     * 
     * @param type $uid
     * @return type
     */
    public function getDoc($uid);

    /**
     * 
     * @param type $uid
     * @param type $firstname
     * @param type $lastname
     * @param type $fiscal_id
     * @return type
     */
    public function returnDoc($uid, $firstname, $lastname, $fiscal_id);

    /**
     * 
     * @param type $uio
     * @param type $binary
     * @return type
     */
    public function confirmDoc($uio, $binary = null);

    /**
     * 
     * @param type $uio
     * @param type $uid
     * @throws CException
     */
    public function statusPay($uio = null, $uid = null);

    /**
     * 
     * @param type $from
     * @param type $to
     * @param type $startDate
     * @param type $train
     */
    public function trainRoute($from, $to, $startDate, $train);

    /**
     * 
     * @param type $start
     * @param type $end
     * @param type $change
     * @return type
     */
    public function documentsList($start, $end, $change = false);

    /**
     * 
     * @param type $code
     * @param type $date
     */
    public function trainsFormStation($code, $date);

    /**
     * Баланс агента
     */
    public function state();

    /**
     * Отчет по транзакциям что были успешно олачены и погашены
     * @param type $date_from начало дипазона в YYYY-MM-DD H:i:s
     * @param type $date_to конец дипазона в YYYY-MM-DD H:i:s
     * @param type $type тип отчета
     */
    public function report($date_from, $date_to, $type = 'payment');
    
    public function getRealTransactionName();
}
