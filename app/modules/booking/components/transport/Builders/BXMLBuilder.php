<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Builder
 *
 * @author Павел
 */
class BXMLBuilder extends BBuilder implements IBBuilder {
    
    protected $templates_dir;
    
    protected $source;
    
    protected $params = array();
    
    protected $transaction_name = 'unknown transaction';


    public function __construct($id, $merchant_id, $lang = 'RU') {
        $this->templates_dir = dirname(__FILE__).DIRECTORY_SEPARATOR.'XML'.DIRECTORY_SEPARATOR.'templates';
        parent::__construct($id, $merchant_id, $lang);
    }
    
    public function getRealTransactionName() {
        return $this->transaction_name;
    }

    public function booking($from, $to, $startDate, $train, $wagon_type, $wagon_number, $places, $documents, $wagon_class = null) {
        if(count($documents) > 4){
            throw new InvalidArgumentException('Maximal count documents is 4');
        }
        if($wagon_type == 'С' && !isset($wagon_class)){
            throw new InvalidArgumentException('Parameter wagon_class not exists');
        }
        $this->setParamsRoute($from, $to, $startDate);
        $this->setParamsTrainNumber($train);
        $this->setParamsWagon($wagon_type, $wagon_number, $wagon_class);
        $this->setPlaces($places);
        $this->params['documents'] = $documents;
        return $this->generate('booking', $this->params);
    }

    public function cancel($reserve_id) {
        return $this->generate('revocation', array('reserve_id' => $reserve_id), 'cancel');
    }

    public function confirmDoc($uio, $binary = null) {
        return $this->generate('return_doc_confirm', array('uio' => $uio, 'binary' => $binary));
    }

    public function documentsList($start, $end, $change = false) {
        return $this->generate('documents_list', array('start' => $start, 'end' => $end, 'change' => $change));
    }

    public function getDoc($uid) {
        return $this->generate('getdoc', array('uid' => $uid));
    }

    public function getEDoc($uid, $firstname, $lastname, $fiscal_id = null) {
         return $this->generate('getedoc', array('uid' => $uid, 'firstname' => $firstname, 'lastname' => $lastname, 'fiscal_id' => $fiscal_id));
    }

    public function pay($reserve_id, $transaction_type, $binary = null, $qrcode = null, $barcode = null) {
        return $this->generate('pay', array('reserve_id' => $reserve_id, 'transaction_type' => $transaction_type, 'binary' => $binary, 'qrcode' => $qrcode, 'barcode' => $barcode));
    }

    public function places($from, $to, $startDate, $train, $wagon_type, $wagon_number, $wagon_class = null) {
        $this->setParamsRoute($from, $to, $startDate);
        $this->setParamsTrainNumber($train);
        $this->setParamsWagon($wagon_type, $wagon_number, $wagon_class);
        return $this->generate('places', $this->params);
    }

    public function prices($from, $to, $startDate, $train) {
        $this->setParamsRoute($from, $to, $startDate);
        $this->setParamsTrainNumber($train);
        return $this->generate('prices', $this->params);
    }

    public function report($date_from, $date_to, $type = 'payment') {
        return $this->generate('report', array('date_form' => $date_from, 'date_to' => $date_to, 'type' => $type));
    }

    public function reserve($from, $to, $startDate, $train, $wagon_type, $wagon_number, $places, $documents, $wagon_class = null, $no_bedding = null, $services = null, $no_electronic = null) {
        $this->setParamsRoute($from, $to, $startDate);
        $this->setParamsTrainNumber($train);
        $this->setParamsWagon($wagon_type, $wagon_number, $wagon_class);
        $this->setPlaces($places);
        $this->setReserve($documents, $no_bedding, $no_electronic, $services);
        return $this->generate('reserve', $this->params);
    }

    public function reserveRoundtrip($uio, $from, $to, $startDate, $train, $train_class, $train_model, $train_fasted, $train_category, $wagon_type, $wagon_number, $places, $documents, $wagon_class = null, $no_bedding = null, $services = null, $no_electronic = null) {
        $this->setParamsRoute($from, $to, $startDate);
        $this->setParamsTrainNumber($train);
        $this->setParamsWagon($wagon_type, $wagon_number, $wagon_class);
        $this->setPlaces($places);
        $this->setReserve($documents, $no_bedding, $no_electronic, $services);
        $this->params['uio'] = $uio;
        $this->params['train_class'] = $train_class;
        $this->params['train_model'] = $train_model;
        $this->params['train_fasted'] = $train_fasted;
        $this->params['train_category'] = $train_category;
        return $this->generate('reserve_roundtrip', $this->params);
    }

    public function response($src_id) {
        return $this->generate('response', array('src_id' => $src_id), 'get response');
    }

    public function returnDoc($uid, $firstname, $lastname, $fiscal_id) {
        return $this->generate('return_doc', array('uid' => $uid, 'firstname' => $firstname, 'lastname' => $lastname, 'fiscal_id' => $fiscal_id));
    }

    public function revocation($reserve_id) {
        return $this->generate('revocation', array('reserve_id' => $reserve_id));
    }

    public function state() {
        return $this->output('', 'state');
    }

    public function stations($q) {
        return $this->generate('stations', array('q' => $q));
    }

    public function stationsList($date = null) {
        return $this->generate('stations_list', array('date' => $date), 'stations list');
    }

    public function statusPay($uio = null, $uid = null) {
        if(!isset($uid) && !isset($uio)){
            throw new InvalidArgumentException('Do not pass parameters');
        }
        if(isset($uid) && isset($uio)){
            throw new InvalidArgumentException('Only one parameter: uid or uio');
        }
        return $this->generate('pay_status', array('uio' => $uio, 'uid' => $uid));
    }

    public function trainRoute($from, $to, $startDate, $train) {
        $this->setParamsRoute($from, $to, $startDate);
        $this->setParamsTrainNumber($train);
        return $this->generate('prices', $this->params, 'train_route');
    }

    public function trains($from, $to, $startDate) {
        $this->setParamsRoute($from, $to, $startDate);
        return $this->generate('trains', $this->params);
    }

    public function trainsExtended($from, $to, $startDate) {
        $this->setParamsRoute($from, $to, $startDate);
        return $this->generate('trains', $this->params, 'trains extended');
    }

    public function trainsFormStation($code, $date) {
        return $this->generate('trains_from_station', array('code' => $code, 'date' => $date));
    }

    public function transportation($uid, $kind, $weight = null) {
        return $this->generate('transportation', array('uid' => $uid, 'kind' => $kind, 'weight' => $weight));
    }
    public function source(){
        return $this->source;
    }
    
    public function xml(){
        return $this->source ? new BXMLDOMDocument($this->source) : null;
    }

    protected function setParamsRoute($from, $to, $date){
        $this->params['from'] = $from;
        $this->params['to'] = $to;
        $this->params['date'] = $date;
        return $this->params;
    }
    
    protected function setParamsTrainNumber($train){
        return $this->params['train'] = $train;
    }
    
    protected function setParamsWagon($wagon_type, $wagon_number, $wagon_class){
        $this->params['wagon_type'] = $wagon_type;
        $this->params['wagon_number'] = $wagon_number;
        $this->params['wagon_class'] = $wagon_class;
        return $this->params;
    }
    
    protected function setPlaces($places){
        if(is_array($places)){
            $this->params['count_lower'] = $places['count_lower'];
            $this->params['count_top'] = $places['count_top'];
            $this->params['places'] = $places['places'];
            $this->params['no_side'] = $places['no_side'];
            $this->params['one_coupe'] = $places['one_coupe'];
            $this->params['needs'] = true;
        }else{
            $this->params['places'] = $places;
            $this->params['needs'] = false;
        }
    }
    protected function setReserve($documents, $no_bedding = null, $no_electronic = null, $services = null){
        $this->params['documents'] = $documents;
        $this->params['no_bedding'] = $no_bedding;
        $this->params['no_electronic'] = $no_electronic;
        $this->params['services'] = $services;
    }

    protected function generate($fileName, $params = array(), $transaction_name = null){
        extract($params);
        ob_start();
        require $this->templates_dir.DIRECTORY_SEPARATOR.$fileName.'.php';
        $name = !$transaction_name ? $fileName : $transaction_name;
        $this->source = $this->output(ob_get_clean(), $name);
        $this->reset();
        return $this->source();
    }
    protected function output($content, $transaction_name){
        $this->transaction_name = $transaction_name;
        ob_start();
        require $this->templates_dir.DIRECTORY_SEPARATOR.'transaction.php';
        return ob_get_clean();
    }
    protected function reset(){
        $this->params = array();
    }

}
