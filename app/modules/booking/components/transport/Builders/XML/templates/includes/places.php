<?php if($needs): ?>
<needs>
    <count_lower><?php echo $count_lower ?></count_lower>
    <count_top><?php echo $count_top ?></count_top>
    <places><?php echo $places ?></places>
    <no_side><?php echo $no_side ?></no_side>
    <one_coupe><?php echo $one_coupe ?></one_coupe>
</needs>
<?php else: ?>
<places><?php echo $places ?></places>
<?php endif; ?>
<documents>
    <?php foreach ($documents as $document): ?>
    <document>
        <number><?php echo $document['number'] ?></number>
        <kind><?php echo $document['kind'] ?></kind>
        <count_place><?php echo $document['count_place'] ?></count_place>
        <firstname><?php echo $document['firstname'] ?></firstname>
        <lastname><?php echo $document['lastname'] ?></lastname>
        <?php if(isset($document['child'])): ?>
        <child><?php echo $document['child']; ?></child>
        <?php elseif (isset($document['privilege'])): ?>
        <passport><?php echo $document['passport']; ?></passport>
        <?php endif; ?>
    </document>
    <?php endforeach; ?>
</documents>