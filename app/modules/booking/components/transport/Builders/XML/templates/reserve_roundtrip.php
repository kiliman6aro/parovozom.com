<link_uio><?php echo $uio; ?></link_uio>
<?php include $this->templates_dir.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'route.php'; ?>
<train class="<?php echo $train_class; ?>" fasted="<?php echo $train_fasted; ?>" model="<?php echo $train_model; ?>" category="<?php echo $train_category; ?>"><?php echo $train; ?></train>
<wagon_type><?php echo $wagon_type; ?></wagon_type>
<wagon_number><?php echo $wagon_number; ?></wagon_number>
<?php  if(isset($wagon_class)): ?>
<wagon_class><?php echo $wagon_class; ?></wagon_class>
<?php endif; ?>
<?php if(isset($no_bedding)): ?>
<no_bedding><?php echo $no_bedding ?></no_bedding>
<?php endif; ?>
<?php if(isset($services)): ?>
<services><?php echo $services ?></services>
<?php endif; ?>
<?php if(isset($no_electronic)): ?>
<no_electronic><?php echo $no_electronic ?></no_electronic>
<?php endif; ?>
<?php include $this->templates_dir.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'places.php'; ?>