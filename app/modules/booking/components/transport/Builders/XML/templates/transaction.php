<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<request version="1.0">
    <merchant_id><?php echo $this->merchant_id; ?></merchant_id>
    <xmlsign><?php echo " "; ?></xmlsign>
    <language><?php echo $this->lang; ?></language>
    <datetime><?php echo $this->date; ?></datetime>
    <transaction type="<?php echo $transaction_name ?>" id="<?php echo $this->id ?>">
        <?php echo $content; ?>
    </transaction>
</request>
