<reserve_id><?php echo $reserve_id; ?></reserve_id>
<transaction_type><?php echo $transaction_type; ?></transaction_type>
<?php if (isset($qrcode)): ?>
<qrcode>image</qrcode>
<?php endif; ?>
<?php if (isset($barcode)): ?>
<barcode>image</barcode>
<?php endif; ?>
<?php if (isset($binary)): ?>
<binary type="printer"><?php echo $binary; ?></binary>
<?php endif; ?>