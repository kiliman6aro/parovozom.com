<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BSSLSignature
 *
 * @author Павел
 */
class BSSLXMLSignature {
    
    protected $key;
    
    protected $key_pass;
    
    protected $source;
    
    protected $signature;
    
    protected $dom;


    public function __construct($source, $pem, $pem_pass = '') {
        if (!file_exists($pem)) {
            throw new BFileExistException("Key file not exists on path: $pem");
        }
        $this->key = $pem;
        $this->key_pass = $pem_pass;
        $this->source = $source;
    }
    
    public function signature(){
        $fp = fopen($this->key, 'r');
        $priv_key = fread($fp, 8192);
        fclose($fp);
        $pkeyid = openssl_pkey_get_private($priv_key, $this->key_pass);
        $xml = new BXMLDOMDocument($this->source);
        
        openssl_sign($xml->saveXML(), $signature, $pkeyid);
        openssl_free_key($pkeyid);
        $this->signature = base64_encode($signature);
        return $this->signatureDOM($this->signature);
    }
    private function signatureDOM($signature){
        $this->dom = new BXMLDOMDocument($this->source);
        $this->dom->getElementsByTagName('xmlsign')->item(0)->nodeValue = $signature;
        return $this->dom->saveXML();
    }
}
