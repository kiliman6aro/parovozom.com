<?php

class BHttpProcessing implements IBProcessing {

    protected $headers = array(
        'Content-type: text/xml; charset="utf-8"'
    );
    
    protected $request;
    
    protected $response;

    public function prepare($source, $uri, $key) {
        $this->request = $source;
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $uri);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->request);
        curl_setopt($this->ch, CURLOPT_CAPATH, $key);
        return $this;
    }

    public function execute() {
        $this->response = curl_exec($this->ch);
        if (curl_errno($this->ch)) {
            throw new BHttpProcessingException(curl_error($this->ch));
        }
        $dom = new BXMLDOMDocument($this->response);
        if($dom->getElementsByTagName('error')->length > 0){
            $code = $dom->getElementsByTagName('code')->item(0)->nodeValue;
            $message = $dom->getElementsByTagName('text')->item(0)->nodeValue;
            throw new BResultErrorHttpProcessingException($code, $message, $this->request, $this->response);
        }
        return $this->response;
    }
}
