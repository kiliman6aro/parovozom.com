<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BXMLDOMDocument
 *
 * @author Павел
 */
class BXMLDOMDocument extends DOMDocument {

    public function __construct($source, $version = '1.0', $encoding = 'utf-8') {
        parent::__construct($version, $encoding);
        $this->loadXML($source);
    }
}
