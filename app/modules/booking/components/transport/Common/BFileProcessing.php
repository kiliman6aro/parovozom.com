<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BFileService
 *
 * @author Павел
 */
class BFileProcessing implements IBProcessing{
    
    protected $file;
    
    protected $request;
    
    protected $response;
    
    protected $map = array(
        'trains extended' => 'trains_extended'
    );
    
    protected $payNames = array(
        'pay-to-electronic',
        'pay-to-noelectronic',
        'transportation-pay-false',
        'transportation-pay-true'
    );


    public function execute() {
        $this->response =  file_get_contents($this->file);
        
        $dom = new BXMLDOMDocument($this->response);
        if($dom->getElementsByTagName('error')->length > 0){
            $code = $dom->getElementsByTagName('code')->item(0)->nodeValue;
            $message = $dom->getElementsByTagName('text')->item(0)->nodeValue;
            throw new BResultErrorHttpProcessingException($code, $message, $this->request, $this->response);
        }
        return $this->response;
    }

    public function prepare($source, $uri, $key) {
        $this->request = $source;
        try{
            $dom = new BXMLDOMDocument($this->request);
            $name = $dom->getElementsByTagName('transaction')->item(0)->attributes->getNamedItem('type')->nodeValue;
        }catch(Exception $e){
            throw new BFileExistException('Unable to create BXMLDOMDocument');
        }
        $path = BOOKING_BASE_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.$uri.DIRECTORY_SEPARATOR.$this->getFileName($name);
        if(file_exists($path)){
            $this->file = $path;
            return $this;
        }
        throw new BFileExistException('File {'.$path.'} is not exist');
    }
    protected function getFileName($name){
        if($name == 'pay'){
            return $this->payNames[array_rand($this->payNames)].'.xml';
        }
        if(isset($this->map[$name])){
            return $this->map[$name].'.xml';
        }
        return $name.'.xml';
    }


}
