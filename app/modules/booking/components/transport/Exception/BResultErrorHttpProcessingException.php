<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BResultProcessingException
 *
 * @author Павел
 */
class BResultErrorHttpProcessingException extends BHttpProcessingException{
    
    protected $request;
    
    protected $response;

    protected $message;

    public function __construct($code, $message, $request, $response, $previous = null) {
        $this->request = $request;
        $this->response = $response;
        parent::__construct($message, $code, $previous);
    }
    public function getRequest(){
        return $this->request;
    }
    public function getResponse(){
        return $this->response;
    }
}
