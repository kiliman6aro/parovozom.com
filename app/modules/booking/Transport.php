<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transport
 *
 * @author Администратор
 */
class Transport extends CApplicationComponent{
    
    public $url;
    
    public $file_key;
    
    
    private $_headers = array(
        'Content-type: text/xml; charset="utf-8"'
    );
    
    public function submit(IRequest $request){
        echo 'dasd'; exit;
        $response = ($this->url == 'localhost') ? $this->_read($request) : $this->_query($request);
        $request->handler($response);
    }
    private function _read(IRequest $request){
        $alias = Settings::Get('data', 'booking');
        $file = Yii::getPathOfAlias($alias).DS.'responses'.DS.$request->getAction();
        return file_get_contents($file);
    }

    private function _query(IRequest $request){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->_headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request->getSource());
        $response = curl_exec($ch);
        if(curl_errno($ch)){
            throw new CHttpException(curl_error($ch));
        }
        return $response;
    }
}
