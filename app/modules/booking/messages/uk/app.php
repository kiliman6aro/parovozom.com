<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return array (
  'Agree' => '',
  'БЛАНК ЗАКАЗА' => 'БЛАНК ЗАМОВЛЕННЯ',
  'Вагон' => 'Вагон',
  'Даный бланк заказа для проезда недействительный.' => 'Даний бланк замовлення для проїзду недійсний.',
  'Дата и время отправления' => 'Дата та час відправлення',
  'Дата и время прибытия' => 'Дата та час прибуття',
  'Дата и номер заказа' => 'Дата та номер замовлення',
  'ИНФОРМАЦИЯ О СТОИМОСТИ ЗАКАЗА' => 'ІНФОРМАЦІЯ ПРО ВАРТІСТЬ ЗАМОВЛЕННЯ',
  'КСБ.' => 'КСБ.',
  'Количество мест' => 'Кількість місць',
  'Места' => 'Місця',
  'Место' => 'Місце',
  'НДС.' => 'ПДВ.',
  'Номер поезда' => 'Номер поїзда',
  'Общая стоимость' => 'Загальна вартість',
  'Перед поездкой необходимо предоставить этот бланк заказа в любую автоматизированную билетную кассу Украины и распечатать проездные и перевозочные документы до отправления поезда. При отсутствии возможности распечатать содержимое данного бланка, пожалуйста, запишите номер заказа с приведенной выше таблицы.' => 'Перед поїздкою необхідно надати цей бланк замовлення у будь-яку автоматизовану квиткову касу материкової частини України і роздрукувати проїзні та перевізні документи до відправлення поїзда. За відсутності можливості роздрукувати вміст даного бланка, будь ласка, запишіть ваш номер замовлення із наведеної нижче таблиці.',
  'Проездные документы' => 'Проїзні документи',
  'СТР.' => 'СТР.',
  'Сбор за оформление ППД без НДС' => 'Збір за оформлення ППД без ПДВ',
  'Станция назначения' => 'Станція призначення',
  'Станция отправления' => 'Станція відправлення',
  'Стоимость проездного документа' => 'Коштовність проїздного документа',
  'Тариф' => 'Тариф',
  '№ доку-<br/>мента' => '№ доку-<br/>мента',
  '-ый класс' => '-ий класс',
  'Email' => 'Email',
  'firm' => 'ФІРМ',
  '{h} ч {i} м' => '{h} г {i} х',
  'Аппаратура' => 'Аппаратура',
  'Багаж' => 'Багаж',
  'Билеты' => 'Квитки',
  'Бланк заказа' => 'Бланк замовлення',
  'Бронировать' => 'Бронювати',
  'Ваш заказ' => 'Ваше замовлення',
  'Введите ваш E-mail' => 'Ввудіть ваш E-mail',
  'Вес' => 'Вага',
  'Взрослый' => 'Дорослий',
  'Время' => 'Час',
  'Время в пути' => 'Час подорожі',
  'Всего' => 'Разом',
  'Вы не ввели дату рождения' => 'Ви не зазначили дату народження',
  'Вы не ввели имя' => 'Ви не зазначили ім`я',
  'Вы не ввели фамилию' => 'Ви не зазначили призвіще',
  'Вы не указали вес' => 'Ви не зазначили вагу',
  'Вы не указали тип багажа' => 'Ви не зазначили тип багажу',
  'Выберете дату рождения ребенка (6-14 лет):' => 'Оберіть дату народження дитин (6-14 років)',
  'Выбрать место' => 'Оберить місце',
  'Выкупить не позднее' => 'Викупити не пызніше',
  'Выкупить не позднее: ' => 'Викупити не пызніше:',
  'Дата отправления' => 'Дата відправлення',
  'Дата рождения' => 'Дата народження',
  'Детский' => 'Дитячий',
  'Документ на услугу' => 'Документ на послугу',
  'Дополнительные сервисы:' => 'Додакові сервіси',
  'ЗН:{server}' => 'ЗН:{server}',
  'Имя' => 'Ім`я',
  'Куда' => 'Куди',
  'Купить' => 'Купити',
  'Направление' => 'Напрямок',
  'Номер вагона' => 'Номер вагону',
  'Обязательно введите фамилию и имя пассажира, который будет осуществлять поездку.' => 'Обов`язково, зазначте ім`я та призвище пасажира, який буде подорожувати',
  'Оплатить' => 'Сплатити',
  'Откуда' => 'Звідки',
  'Отмена' => 'Скасування',
  'Отменить' => 'Скасувати',
  'Отправление' => 'Напрямок',
  'Отправления' => 'Відправлення',
  'Ошибка со стороны платежного шлюза. Некорректная подпись данных' => 'Помилка шлюзу оплати. Некорректний підпис данних',
  'ПН:{tin}' => 'ПН:{tin}',
  'Перевозка багажа' => 'Транспортування багажу',
  'Перевізний документ' => 'Перевізний документ',
  'Поезд' => 'Поїзд',
  'Полный' => 'Повний',
  'Посадочный документ' => 'Посадочний документ',
  'Предзаказ' => 'Передзамовлення',
  'Прибытие' => 'Прибуття',
  'Прибытия' => 'Прибуття',
  'Птицы, животные' => 'Тварини, птахи',
  'Ручная кладь' => 'Надлишок',
  'Сервисы заказываются одновременно на все места.' => 'Сервіси замовлюються одночасно для всіх місць',
  'Сортировать' => 'Сортувати',
  'Стоимость' => 'Коштовність',
  'Стоимость билета' => 'Коштовність квитка',
  'Стоимость билета:' => 'Коштовність квитка',
  'ТЕРМ.№{n}' => 'ТЕРМ.№{n}',
  'Тип' => 'Тип',
  'ФК:{id}' => 'ФК:{id}',
  'ФН:{rro}' => 'ФН:{rro}',
  'Файл {path} не найден' => 'Файл {path} не знайдено',
  'Фамилия' => 'Прізвище',
  'Цена от' => 'Ціна від',
  'вагон' => 'вагони',
  'грн' => 'грн',
  'места' => 'місця',
  'место' => 'місце',
  'свободных' => 'вільник',
);
