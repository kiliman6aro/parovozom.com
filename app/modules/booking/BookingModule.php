<?php

class BookingModule extends CWebModule {

    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'booking.models.*',
            'booking.controllers.*',
            'booking.interfaces.*',
            'booking.components.*',
            'booking.helpers.*',
        ));
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }

    public static function t($dic = 'app', $str = '', $params = array()) {
        return Yii::t(__CLASS__ . "." . $dic, $str, $params);
    }

    /**
     * Возвращает схему вагона, исходя из переданных параметров
     * @param type $type
     * @param type $class
     * @return type
     */
    public static function GetScheme($number, $type, $class = null) {
        $where = array('and', "type = '" . $type . "'");
        if ($class) {
            $where[] = array("class = '" . $class . "'");
        }
        $scheme = Yii::app()->db->createCommand()
                ->select('scheme')
                ->from('{{schemes}}')
                ->where($where)
                ->queryScalar();
        if (!$scheme) {
            //бросить исключение
        }
        return str_replace('{number}', $number, $scheme);
    }
    /**
     * Пересчет стоимости
     * @param type $price
     * @param type $type тип заказа (lugagge, cost, reserve)
     * @return type
     */
    public static function calculatePrice($price, $type = 'cost'){
        $prc = ($price/100)*3;
        return number_format(($price+$prc+25), 2, '.', '');
    }

}
