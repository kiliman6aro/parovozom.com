<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PayTransaction
 *
 * @author Администратор
 */
class PayTransaction extends BaseTransaction{
    
    /**
     *
     * @var Strong тип документа (reserve, booking, transportation)
     */
    public $product_type;
    
    public function getAction() {
        return "pay";
    }
    
    /**
     * Добавляет необходимые узлы в родительный ухел
     * trnsaction
     * @return DOMNode transaction
     */
    public function build() {
        $transaction = parent::build();
        $this->product_type = $this->storage->getParam('type');
        $transaction->appendChild($this->dom->createElement('reserve_id',$this->storage->getParam('transaction_id')));
        $transaction->appendChild($this->dom->createElement('transaction_type',5));
        
        return $transaction;
    }

    
    public function handling($source) {
        $dom = parent::handling($source);
        $documents = array();
        foreach ($dom->getElementsByTagName('order') as $node) {
            $docs = $this->parseOrder($node);
            if(is_array($docs)){
                $documents = array_merge($documents, $docs);
            }else{
                $documents[] = $docs;
            }
        }
        return $documents;
    }
    
    /**
     * Парсит тег <order>. Генерирует документы, если заказ является
     * электронным, то возвращабтся документы, если не электронный и 
     * при этом явялется проездным, то есть заказ на несколько мест
     * то генерируется OrderDocument и в него передаются документы.
     * @param DOMNode $node <order></order>
     * @return \OrderDocument
     */
    protected function parseOrder(DOMNode $node){
        $dom = new DOMDocument;
        $dom->loadXML($node->ownerDocument->saveXML($node));
        $documents = array();
        $electronic =  String::toLower($dom->getElementsByTagName('electronic')->item(0)->nodeValue) == 'true' ? true : false;
        //Если билет электронный то документы находятся в теге documents или ticket
        $tag = $electronic ? 'documents' : 'ticket';
        foreach ($dom->getElementsByTagName($tag) as $node) {
            $document = DocumentFactory::load($node);
            $document->electronic = $electronic;
            $document->paydate = $dom->getElementsByTagName('paydate')->item(0)->nodeValue;
            $document->barcode_image = $dom->getElementsByTagName('barcode_image')->item(0)->nodeValue;
            if(!$document->electronic){
                $document->ordernumber = $dom->getElementsByTagName('ordernumber')->item(0)->nodeValue;
            }
            $documents[] = $document;
        }
        //Если не электронный и это документ проездной,
        //то все документы присвоить одному документу-заказу
        if(!$electronic && $documents[0]->type == 'travel'){
            $order = new OrderDocument;
            $order->barcode_image = $dom->getElementsByTagName('barcode_image')->item(0)->nodeValue;
            $order->ordernumber = $dom->getElementsByTagName('ordernumber')->item(0)->nodeValue;
            $order->paydate = $dom->getElementsByTagName('paydate')->item(0)->nodeValue;
            $order->electronic = $electronic;
            $order->setDocuments($documents);
            return $order;
        }
        return $documents;
    }
}
