<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReserveTransaction
 *
 * @author Администратор
 */
class BookingTransaction extends IssueTransaction{
    
    public function getAction() {
        return 'booking';
    }
    public function handling($source) {
        $products = parent::handling($source);
        $doc = new DOMDocument();
        $doc->loadXML($source);
        $place = $this->_order->getPlace($doc->getElementsByTagName('places')->item(0)->nodeValue);
        $place->booking = $this->parseBooking($doc);
        
        return $products;
    }
    
    /**
     * Пасрит данные на наличие заказа
     * @param DOMDocument $dom
     * @return array
     */
    protected function parseBooking(DOMDocument $dom){
        $booking = array();
        if($dom->getElementsByTagName('reserve')->length <= 0){
            return $booking;
        }
        $childNodes = $dom->getElementsByTagName('reserve')->item(0)->childNodes;
        foreach ($childNodes as $child){
            $booking[$child->nodeName] = $child->nodeValue;
        }
        return $booking;
    }
}
