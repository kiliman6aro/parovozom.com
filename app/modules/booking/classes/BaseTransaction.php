<?php

/**
 * Description of Transaction
 *
 * @author Администратор
 */
abstract class BaseTransaction implements ITransaction {

    protected $storage;
    protected $action;
    protected $id;
    protected $request;
    protected $response;
    protected $dom;
    protected $transport;

    /**
     *
     * @var TransactionError 
     */
    protected $error = array();
    
    public function setStorage(IStorageManager $storage) {
        $this->storage = $storage;
    }
    
    public function setResponse($source) {
        $this->response = $source;
    }
    
    /**
     * Проверяет наличие ошибок в транзакции
     * @return bool
     */
    public function hasError() {
        return $this->error ? true : false;
    }
    
    public function setError($code, $message){
        $this->error = new TransactionError($code, $message);
    }

    /**
     * Возвращает ошибку транзакции
     * @return ErrorTransaction
     */
    public function getError(){
        return $this->error;
    }

    public function setTransport(\ITransport $transport) {
        $this->transport = $transport;
    }

    /**
     * Генерирует транзакцию.
     * @return DOMNode узел транзакции
     */
    public function build() {
        $this->dom = new DOMDocument('1.0', 'utf-8');
        $datetime = $this->dom->createElement('datetime', $this->storage->getParam('datetime'));
        $request = $this->dom->createElement('request');
        $merchant = $this->dom->createElement('merchant_id', $this->storage->getParam('merchant_id'));
        $language = $this->dom->createElement('language', $this->storage->getParam('language'));
        $signature = $this->dom->createElement('xmlsign', ' ');
        $transaction = $this->dom->createElement('transaction');
        $transaction->setAttribute('type', $this->getAction());
        $transaction->setAttribute('id', $this->getID());
        $request->appendChild($datetime);
        $request->appendChild($datetime);
        $request->appendChild($merchant);
        $request->appendChild($language);
        $request->appendChild($signature);
        $request->appendChild($transaction);
        $this->dom->appendChild($request);
        return $transaction;
    }

    public function submit() {
        return $this->transport->request($this);
    }

    /**
     * Возвращает полученный ответ в виде XML DOM
     * @param type $source
     * @return \DOMDocument
     * @throws Exception
     */
    public function handling($source) {
        $this->setResponse($source);
        $dom = new DOMDocument;
        $dom->loadXML($source);
        //Если транзакция с ошибкой
        if($dom->getElementsByTagName('error')->length > 0){
            $this->_parseError($dom);
            //throw new Exception($this->error->message, $this->error->code);
        }
        return $dom;
    }

    public function getRequest() {
        return $this->dom->saveXML();
    }

    public function getResponse() {
        return $this->response;
    }

    public function getID() {
        return $this->id ? $this->id : $this->id = uniqid();
    }

    public function sign() {
        $fp = fopen($this->storage->getParam('key'), 'r');
        $priv_key = fread($fp, 8192);
        fclose($fp);
        $pkeyid = openssl_pkey_get_private($priv_key, '');

        openssl_sign($this->dom->saveXML(), $signature, $pkeyid);
        openssl_free_key($pkeyid);
        $sign = base64_encode($signature);
        $this->dom->getElementsByTagName('xmlsign')->item(0)->nodeValue = $sign;
        return $this;
    }
    /**
     * Парсит документ, на наличие ошибок
     * @param DOMDocument $dom
     */
    private function _parseError(DOMDocument $dom){
        $this->setError($dom->getElementsByTagName('code')->item(0)->nodeValue, $dom->getElementsByTagName('text')->item(0)->nodeValue);
    }

}
