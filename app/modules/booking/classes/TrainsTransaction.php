<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TrainsTransaction
 *
 * @author Администратор
 */
class TrainsTransaction extends BaseTransaction{


    public function getAction() {
        return 'trains extended';
    }
    
    public function build() {
        parent::build();
        $direction = $this->storage->getParam('direction');
        $station_to = $this->dom->createElement('code_station_to', $direction->to_code);
        $station_from = $this->dom->createElement('code_station_from', $direction->from_code);
        $date = $this->dom->createElement('date1', $direction->date1);
        $transaction = $this->dom->getElementsByTagName('transaction')->item(0);
        $transaction->appendChild($station_from);
        $transaction->appendChild($station_to);
        $transaction->appendChild($date);
    }
    public function handling($source) {
        $dom = parent::handling($source);
        $routes = array();
        //Получить поезда
        foreach ($dom->getElementsByTagName('train') as $node) {
            $xml = new DOMDocument;
            $xml->loadXML($dom->saveXML($node));
            foreach ($xml->getElementsByTagName('wagon') as $wagonNode) {
                $route = new Route();
                $route->train = Train::TrainByDOM($xml);
                $route->place_count = $wagonNode->nodeValue;
                $route->wagon_type = $wagonNode->attributes->getNamedItem('type')->nodeValue;
                if($route->wagon_type == 'С'){
                    $route->wagon_class = $wagonNode->attributes->getNamedItem('class')->nodeValue;
                }
                $routes[] = $route;
            }
        }
        return $routes;
    }

}
