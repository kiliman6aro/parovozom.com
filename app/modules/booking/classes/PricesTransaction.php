<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PricesTransaction
 *
 * @author Администратор
 */
class PricesTransaction extends BaseTransaction{
    
    public function getAction() {
        return 'prices';
    }

    public function build() {
        parent::build();
        $transaction = $this->dom->getElementsByTagName('transaction')->item(0);
        $direction = $this->storage->getParam('direction');
        $station_to = $this->dom->createElement('code_station_to', $direction->to_code);
        $station_from = $this->dom->createElement('code_station_from', $direction->from_code);
        $date = $this->dom->createElement('date', $direction->date1);
        $number = $this->dom->createElement('train', $this->storage->getParam('train'));
        
        $transaction->appendChild($station_from);
        $transaction->appendChild($station_to);
        $transaction->appendChild($date);
        $transaction->appendChild($number);
        
    }
    public function handling($source) {
        $dom = parent::handling($source);
        $items = array();
        $wagon_type = $this->storage->getParam('wagon_type', false);
        $wagon_class = $this->storage->getParam('wagon_class', false);
        foreach ($dom->getElementsByTagName('wagon') as $node){
            $xml = new DOMDocument;
            $xml->loadXML($node->ownerDocument->saveXML($node));
            $number = $xml->getElementsByTagName('number')->item(0)->nodeValue;
            if(isset($items[$number])){
                $wagon = $items[$number];
                
                $wagon->prices[$xml->getElementsByTagName('charline')->item(0)->nodeValue] = array(
                    'cost' => BookingModule::calculatePrice($xml->getElementsByTagName('cost')->item(0)->nodeValue, 'cost'),
                    'costreserve' => BookingModule::calculatePrice($xml->getElementsByTagName('costreserve')->item(0)->nodeValue, 'reserve'),
                    'currency' => $wagon->currency
                );
                
                $lower = $xml->getElementsByTagName('lower')->item(0)->nodeValue;
                $lower_side = $xml->getElementsByTagName('lower')->item(0)->attributes->getNamedItem('side')->nodeValue;
                $top = $xml->getElementsByTagName('top')->item(0)->nodeValue;
                $top_side = $xml->getElementsByTagName('top')->item(0)->attributes->getNamedItem('side')->nodeValue;
                $wagon->places += $lower += $lower_side += $top += $top_side;
                
                $wagon->cost = $wagon->cost <= $xml->getElementsByTagName('cost')->item(0)->nodeValue ? $wagon->cost : $xml->getElementsByTagName('cost')->item(0)->nodeValue;
                //$wagon->cost = BookingModule::calculatePrice($wagon->price);
                continue;
            }
            $wagon = new Wagon();
            $wagon->number = $number;
            $wagon->train = $this->storage->getParam('train');
            if($xml->getElementsByTagName('class')->length > 0){
                $wagon->class = $xml->getElementsByTagName('class')->item(0)->nodeValue;
                $wagon->class_code = $xml->getElementsByTagName('class')->item(0)->attributes->getNamedItem('code')->nodeValue;
            }
            $wagon->type = $xml->getElementsByTagName('type')->item(0)->nodeValue;
            $wagon->type_code = $xml->getElementsByTagName('type')->item(0)->attributes->getNamedItem('code')->nodeValue;
            $wagon->cost = BookingModule::calculatePrice($xml->getElementsByTagName('cost')->item(0)->nodeValue, 'cost');
            $wagon->currency = $xml->getElementsByTagName('cost')->item(0)->attributes->getNamedItem('currency')->nodeValue;
            $wagon->sitting = strtolower($xml->getElementsByTagName('sitting')->item(0)->nodeValue) == 'true' ? true : false; 
            $wagon->services = $this->_parseServices($xml);
            
            $lower = $xml->getElementsByTagName('lower')->item(0)->nodeValue;
            $lower_side = $xml->getElementsByTagName('lower')->item(0)->attributes->getNamedItem('side')->nodeValue;
            $top = $xml->getElementsByTagName('top')->item(0)->nodeValue;
            $top_side = $xml->getElementsByTagName('top')->item(0)->attributes->getNamedItem('side')->nodeValue;
            $wagon->places += $lower += $lower_side += $top += $top_side;
            
            $wagon->prices[$xml->getElementsByTagName('charline')->item(0)->nodeValue] = array(
                'cost' => $wagon->cost,
                'costreserve' => BookingModule::calculatePrice($xml->getElementsByTagName('costreserve')->item(0)->nodeValue, 'reserve'),
                'currency' => $wagon->currency
            );
            //Добавлять только те которые соответсвутют параметрам
            if(!empty($wagon_class) && $wagon_class != $wagon->class_code){
                continue;
            }
            if(!empty($wagon_type) && $wagon_type != $wagon->type_code){
                continue;
            }
            $items[$wagon->number] = $wagon;
        }
        return array_values($items);
    }
    /**
     * Парсит документ на наличие сервисов. Создает соответствующие объекты
     * и наполняет ими массив. Возвращает пустой массив, если сервисы
     * отсутствуют
     * @param DOMDocument $dom
     * @return Service[] 
     */
    private function _parseServices(DOMDocument $dom) {
        $services = array();
        if($dom->getElementsByTagName('service')->length <= 0){
            return $services;
        }
        $childNodes = $dom->getElementsByTagName('service')->item(0)->childNodes;
        for ($i = 0; $i < $childNodes->length; $i++) {
            $item = $childNodes->item($i);
            if($item->nodeType != XML_ELEMENT_NODE){
                continue;
            }
            $service = new Service();
            $service->name = $item->nodeValue;
            $service->code = $item->attributes->getNamedItem('code')->nodeValue;
            $services[] = $service;
        }
        return $services;
    }
}
