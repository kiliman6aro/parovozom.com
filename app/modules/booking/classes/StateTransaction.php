<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StateTransaction
 *
 * @author Администратор
 */
class StateTransaction extends BaseTransaction{
    
    public function getAction() {
        return 'state';
    }
    
    public function handling($source) {
        $dom = parent::handling($source);
        return $dom->getElementsByTagName('balance')->item(0)->nodeValue;
    }
}
