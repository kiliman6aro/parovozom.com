<?php


/**
 * Description of IssueTransaction
 *
 * @author Администратор
 */
abstract class IssueTransaction extends BaseTransaction{
    
    public $transportations = array();
    
    protected $_order;
    
    public function setOrder(Order $o){
        $this->_order = $o;
    }

    public function build() {
        $transaction = parent::build();
        $direction = $this->storage->getParam('direction');
        $station_to = $this->dom->createElement('code_station_to', $direction->to_code);
        $station_from = $this->dom->createElement('code_station_from', $direction->from_code);
        $date = $this->dom->createElement('date', $direction->date1);
        $number = $this->dom->createElement('train', $this->storage->getParam('train'));
        $type = $this->dom->createElement('wagon_type', $this->storage->getParam('wagon_type'));
        $wagon_number = $this->dom->createElement('wagon_number', $this->storage->getParam('wagon_number'));
        $transaction->appendChild($station_from);
        $transaction->appendChild($station_to);
        $transaction->appendChild($date);
        $transaction->appendChild($number);
        $transaction->appendChild($type);
        $transaction->appendChild($wagon_number);        
        if($this->storage->getParam('wagon_type') == 'С'){
            $wagon_class = $this->dom->createElement('wagon_class',$this->storage->getParam('wagon_class'));
            $transaction->appendChild($wagon_class);
        }
        
        $transaction->appendChild($this->dom->createElement('places', $this->getPlacesString()));
        $transaction->appendChild($this->createDocuments());
        return $transaction;
    }
    
    /**
     * Если у документа с порядковым номером
     * есть транспортирвка, то выполняется и транспортировка
     * @param String $source xml данные из транзакций reserve, booking, or reserve roundtrip
     * @return Product[] $products
     */
    public function handling($source) {
        $products = array();
        $dom = parent::handling($source);
        $product = new Product();
        $product->uio = $dom->getElementsByTagName('uio')->item(0)->nodeValue;
        $product->transaction_id = $this->getID();
        $product->type = $this->getAction();
        if($dom->getElementsByTagName('electronic')->item(0)->nodeValue == 'True'){
            $product->electronic = true;
            $product->ordernumber = $dom->getElementsByTagName('ordernumber')->item(0)->nodeValue;
        }else{
            $product->electronic = false;
        }
        foreach ($dom->getElementsByTagName('document') as $documentNode) {
            $doc = new DOMDocument;
            $doc->loadXML($dom->saveXML($documentNode));
            $number = $doc->getElementsByTagName('number')->item(0)->nodeValue;
            $place = $this->_order->getPlace($doc->getElementsByTagName('places')->item(0)->nodeValue);
            $place->addServices($this->parseServices($doc));
            $place->setPrices($this->parsePrices($doc));
            $product->price += $place->total;
            if (isset($this->transportations[$number])) {
                $luggage = $this->transportations[$number];
                $luggage['uid'] = $doc->getElementsByTagName('uid')->item(0)->nodeValue;
                $p = $this->transport->transportation($luggage['uid'], $luggage['type'], $luggage['weight'] ? $luggage['weight'] : null);
                $place->setLuggagePrice($p->price);
                $products[] = $p;
            }
        }
        $products[] = $product;
        return $products;
    }
    
    /**
     * Парсит документ на наличие сервисов. Создает соответствующие объекты
     * и наполняет ими массив. Возвращает пустой массив, если сервисы
     * отсутствуют
     * @param DOMDocument $dom
     * @return Service[] 
     */
    protected function parseServices(DOMDocument $dom) {
        $services = array();
        if($dom->getElementsByTagName('services')->length <= 0){
            return $services;
        }
        $childNodes = $dom->getElementsByTagName('services')->item(0)->childNodes;
        if(count($childNodes) <= 0){
            return $services;
        }
        foreach ($dom->getElementsByTagName('services')->item(0)->childNodes as $item){
            if($item->nodeType != XML_ELEMENT_NODE){
                continue;
            }
            $service = new Service();
            $service->name = $item->nodeValue;
            $service->code = $item->attributes->getNamedItem('code')->nodeValue;
            $service->price = $item->attributes->getNamedItem('cost')->nodeValue;
            $services[] = $service;
        }
        return $services;
    }
    /**
     * Возвращает полный список цен (которых может быть несколько). Формирует
     * ассоциативные массивы и формирует вложенный массив
     * @param DOMDocument $dom
     * @return Array массив ассоциативных массивов с ценами
     */
    protected function parsePrices(DOMDocument $dom){
        $prices = array();
        if($dom->getElementsByTagName('costs')->length <= 0){
            return array();
        }
        foreach ($dom->getElementsByTagName('costs')->item(0)->childNodes as $item){
            if($item->nodeType != XML_ELEMENT_NODE){
                continue;
            }
            $price = array();
            foreach ($item->attributes as $attr) {
                $price[$attr->nodeName] = $attr->nodeValue;
            }
            $prices[] = $price;
        }
        return $prices;
    }

    /**
     * Возвращает массив мест
     * @return type
     */
    protected function getPlaces(){
        return $this->storage->getParam('Places', true, false);
    }
    
    /**
     * Возвращает список мест в строку,
     * через запятую
     */
    protected function getPlacesString(){
        $list = array();
        foreach ($this->getPlaces() as $number => $place){
            array_push($list, $number);
        }
        return implode(',', $list); 
    }

    /**
     * Формирует XML тег documents.
     * @return DOMNode
     */
    protected function createDocuments(){
        $n = 0;
        $documents = $this->dom->createElement('documents');
        
        foreach ($this->getPlaces() as $order){
            $documents->appendChild($this->createDocument(++$n, $order));
            if(isset($order['luggage'])){
                $this->transportations[$n] = $order['luggage'];
            }
        }
        return $documents;
    }
    /**
     * формирует один элемент DOMNode <document></document>
     * @param string $number порядковый номер документа
     * @param Array $order набор данных о заказе
     * @return DOMNode
     */
    protected function createDocument($number, $order){
        $document = $this->dom->createElement('document');
        $document->appendChild($this->dom->createElement('number', $number));
        $document->appendChild($this->dom->createElement('kind', $order['kind']));
        if(!empty($order['child'])){
            $document->appendChild($this->dom->createElement('child', $order['child']));
        }
        $document->appendChild($this->dom->createElement('firstname', $order['firstname']));
        $document->appendChild($this->dom->createElement('lastname', $order['lastname']));
        $document->appendChild($this->dom->createElement('count_place', '1'));
        return $document;
    }
}
