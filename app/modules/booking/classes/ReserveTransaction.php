<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReserveTransaction
 *
 * @author Администратор
 */
class ReserveTransaction extends IssueTransaction{
    
    protected $no_bedding = false;
    
    protected $services = array();


    public function getAction() {
        return 'reserve';
    }
    
    /**
     * Дополняет транзакцию наличием свервисов
     * или наличием тега отказа от постели.
     * @return DOMNode
     */
    public function build() {
        $transaction = parent::build();
        $this->createServices();
        
        if($this->no_bedding){
            $transaction->appendChild($this->dom->createElement('no_bedding', 'true'));
        }
        if(!empty($this->services)){
            $transaction->appendChild($this->dom->createElement('services', implode('', $this->services)));
        }
        return $transaction;
    }

    /**
     * Генерирует сервисы. Проверяет нет ли отказа от постельного
     */
    public function createServices() {
        $services = $this->storage->getParam('Services');
        if(empty($services)){
            $this->no_bedding = true;
            return true;
        }
        //Если сервис П - постель, не найден, то отказ от постельного
        $this->no_bedding = (array_search('П', $services) === false) ? true : false; 
        $this->services = $services;
    }

}
