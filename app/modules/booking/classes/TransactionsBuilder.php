<?php

/**
 * Description of TransactionsManager
 *
 * @author Администратор
 */
class TransactionsBuilder {
    
    private static $_aliases = array(
        'trains extended' => 'trains'
    );


    public static function build($action, IStorageManager $storage, ITransport $transport){
        $name = isset(self::$_aliases[$action]) ? self::$_aliases[$action] : $action;
        $tranaction = self::_load($name);
        $tranaction->setStorage($storage);
        $tranaction->setTransport($transport);
        $tranaction->build();
        return $tranaction;
    }
    
    private static function _load($name){
        $class = ucfirst($name).'Transaction';
        return new $class();
    }
}
