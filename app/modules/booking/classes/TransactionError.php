<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TransactionError
 *
 * @author Администратор
 */
class TransactionError {
    
    public $code;
    
    public $message;
    
    public function __construct($code, $message) {
        $this->code = $code;
        $this->message = $message;
    }
}
