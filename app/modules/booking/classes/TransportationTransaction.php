<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TransportationTransaction
 *
 * @author Администратор
 */
class TransportationTransaction extends BaseTransaction{
        
    public function getAction() {
        return 'transportation';
    }
    
    public function build() {
        $transaction = parent::build();
        $transaction->appendChild($this->dom->createElement('kind', $this->storage->getParam('kind')));
        $transaction->appendChild($this->dom->createElement('uid', $this->storage->getParam('uid')));
        //todo разобраться как поведет себя оформление с перегрузом
        if($this->storage->getParam('weight')){
            $transaction->appendChild($this->dom->createElement('weight', $this->storage->getParam('weight')));
        }
        return $transaction;
    }

    public function handling($source) {
        $dom = parent::handling($source);
        $product = new Product();
        $product->uio = $dom->getElementsByTagName('uio')->item(0)->nodeValue;
        $product->transaction_id = $this->getID();
        $product->type = $this->getAction();
        if($dom->getElementsByTagName('electronic')->item(0)->nodeValue == 'True'){
            $product->electronic = true;
            $product->ordernumber = $dom->getElementsByTagName('ordernumber')->item(0)->nodeValue;
        }
        //@todo вытягивает цену из первого документа (подрузумевается что один документв в одном заказе)
        foreach ($dom->getElementsByTagName('costs')->item(0)->childNodes as $item){
            if($item->nodeType != XML_ELEMENT_NODE){
                continue;
            }
            $product->price += BookingModule::calculatePrice($item->attributes->getNamedItem('cost')->nodeValue, 'lugagge');
            return $product;
        }
    }


}
