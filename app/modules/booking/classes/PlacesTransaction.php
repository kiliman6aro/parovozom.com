<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlacesTransaction
 *
 * @author Администратор
 */
class PlacesTransaction extends BaseTransaction{
    
    public function getAction() {
        return 'places';
    }

    public function build() {
        parent::build();
        $transaction = $this->dom->getElementsByTagName('transaction')->item(0);
        
        $direction = $this->storage->getParam('direction');
        $station_to = $this->dom->createElement('code_station_to', $direction->to_code);
        $station_from = $this->dom->createElement('code_station_from', $direction->from_code);
        $date = $this->dom->createElement('date', $direction->date1);
        $number = $this->dom->createElement('train', $this->storage->getParam('train'));
        $type = $this->dom->createElement('wagon_type', $this->storage->getParam('wagon_type'));
        $wagon_number = $this->dom->createElement('wagon_number', $this->storage->getParam('wagon_number'));
        $transaction->appendChild($station_from);
        $transaction->appendChild($station_to);
        $transaction->appendChild($date);
        $transaction->appendChild($number);
        $transaction->appendChild($type);
        $transaction->appendChild($wagon_number);
        if($this->storage->getParam('wagon_type') == 'С'){
            $wagon_class = $this->dom->createElement('wagon_class', $this->storage->getParam('wagon_class'));
            $transaction->appendChild($wagon_class);
        }
        return $transaction;
    }
    public function handling($source) {
        $dom = parent::handling($source);
        $items = array();
        foreach ($dom->getElementsByTagName('wagon') as $node){
            $xml = new DOMDocument;
            $xml->loadXML($node->ownerDocument->saveXML($node));
            $items[$xml->getElementsByTagName('charline')->item(0)->nodeValue] = explode(',',$xml->getElementsByTagName('places')->item(0)->nodeValue);
        }
        return $items;
    }
}
