<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CanselTransaction
 *
 * @author kilim
 */
class CancelTransaction extends BaseTransaction{
    
    public function getAction() {
        return 'cancel';
    }
    
    public function build() {
        $transaction = parent::build();
        $reserve_id = $this->dom->createElement('reserve_id', $this->storage->getParam('reserve_id'));
        $transaction->appendChild($reserve_id);
        return $transaction;
    }


    public function handling($source) {
        $xml = parent::handling($source);
        $ids = array();
        foreach ($xml->getElementsByTagName('uio') as $item){
            $ids[] = $item->nodeValue;
        }
        return $ids;
    }

}
