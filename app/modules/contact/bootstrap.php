<?php

return array(
    'id' => 'contact',
    'class' => 'application.modules.contact.ContactModule',
    'urlManagerRules' => array(
        'send' => 'contact/default/send'
    ),
);