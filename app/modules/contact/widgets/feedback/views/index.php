<div id="dialog" class="support_pp">
    <h5><?php echo ContactModule::t('Связаться с нами'); ?></h5>
    <?php 
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'dialog-contact-form',
        'enableClientValidation'=>true,
        'action' => Yii::app()->createUrl('contact/default/send'),
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'afterValidate' => 'js: function(form, data, hasError){if(hasError){return false;} else {return true}}'
        )
        
    )); 
    ?>
        <div class="field">
            <?php 
                echo $form->dropDownList($contactModel,'subject', CHtml::listData($models, 'id', 'subject'), 
                        array( 
                        'class' => 'price_sort', 'id' => 'pp_sort')
                    ); 
            ?>
            <?php echo $form->error($contactModel,'subject', array('class' => 'error_message top_view', 'id' => 'pp_sort_em_')); ?>
        </div>
        <div class="field">
            <div class="error_wrap">
                <?php echo $form->textArea($contactModel,'body', array('class' => 'area', 'placeholder' => ContactModule::t('Сообщение'))); ?>
                <?php echo $form->error($contactModel,'body', array('class' => 'error_message top_view')); ?>
            </div>     
        </div>
        <div class="field column_layout contact_field">
            <div class="column1">
                <div class="error_wrap">
                    <?php echo $form->emailField($contactModel,'email', array('class' => 'inp', 'placeholder' => ContactModule::t('Почтовый ящик'))); ?>
                    <?php echo $form->error($contactModel,'email', array('class' => 'error_message')); ?>
                </div>
            </div>
            <div class="column2">
                <div class="error_wrap">
                    <?php echo $form->textField($contactModel,'name', array('class' => 'inp', 'placeholder' => ContactModule::t('Имя'))); ?>
                    <?php echo $form->error($contactModel,'name', array('class' => 'error_message')); ?>
               </div>
            </div>
        </div>
        <div class="field btn_block">
            <a href="javascript:void(0);" onclick="$('#dialog-contact-form').submit();" class="btn send_btn"><?php echo ContactModule::t('Отправить') ?></a>
        </div>
    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    $(function() {
        $("#dialog").dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 500
            },
            hide: {
                effect: "explode",
                duration: 500
            },
            width: 679,
            resizable: false,
            modal: true,
            draggable: false
        });
        $(".opener").click(function() {
            $("#dialog").dialog("open");
        });
        $("#pp_sort").select2();
        
    });
</script>
