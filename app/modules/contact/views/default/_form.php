<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'contact-form-sender-form',
    'enableClientValidation' => true,
    'action' => $this->createUrl('index'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'afterValidate' => 'js: function(form, data, hasError){if(hasError){return false;} else {return true}}'
    )
        ));
    echo $form->hiddenField($model, 'subject', array('value' => ContactModule::t('app', 'Общие вопросы и предложения')));
?>
<div class="field">
    <div class="error_wrap">
        <?php echo $form->textArea($model, 'body', array('class' => 'area', 'placeholder' => ContactModule::t('Сообщение'))); ?>
        <?php echo $form->error($model, 'body', array('class' => 'error_message top_view')); ?>
    </div> 
</div>
<div class="field column_layout contact_field">
    <div class="column1">
        <div class="error_wrap">
            <?php echo $form->emailField($model, 'email', array('class' => 'inp', 'placeholder' => ContactModule::t('Почтовый ящик'))); ?>
            <?php echo $form->error($model, 'email', array('class' => 'error_message')); ?>
        </div>
    </div>
    <div class="column2">
        <div class="error_wrap">
            <?php echo $form->textField($model, 'name', array('class' => 'inp', 'placeholder' => ContactModule::t('Имя'))); ?>
            <?php echo $form->error($model, 'name', array('class' => 'error_message')); ?>
        </div>
    </div>
</div>
<div class="field btn_block">
    <a href="javascript:void(0);" onclick="$('#contact-form-sender-form').submit();" class="btn send_btn"><?php echo ContactModule::t('Отправить') ?></a>
</div>
<?php $this->endWidget(); ?>