<?php $this->beginContent('//layouts/main'); ?>
<div class="content contacts_block">
    <div class="container">
        <h1><?php echo ContactModule::t('Контакты'); ?></h1>
        <div class="tbl_layout">
            <div class="l_column">
                <?php echo $content; ?>
            </div>
            <div class="r_column">
                <h5><?php echo ContactModule::t('Контактная информация'); ?></h5>
                <div class="contact_info">
                    <div class="field mail">
                        <i class="icon"></i>
                        <a href="mailto:<?php echo Settings::Get('email', 'contact'); ?>"><?php echo Settings::Get('email', 'contact'); ?></a>
                    </div>
                    <div class="field phone">
                        <i class="icon"></i>
                        <?php echo Settings::Get('phone', 'contact'); ?>
                    </div>
                    <div class="field address">
                        <i class="icon"></i>
                        <address>
                            <?php echo Settings::Get('address', 'contact'); ?>
                        </address>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>