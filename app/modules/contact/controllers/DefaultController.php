<?php

class DefaultController extends Controller {
    
    public $layout = '/layouts/column2';
    
    public function actionIndex() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                Mail::Send(Settings::Get('email', 'contact'), $model->subject, $model->body, $model->email, $model->name);
                $this->render('index', array('model' => $model));
                return true;
            }
        }
        $this->render('index', array('model' => $model));
    }
    public function actionSend(){
        $form = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $form->attributes = $_POST['ContactForm'];
            if ($form->validate()) {
                $suportModel = Contact::model()->findByPk($form->subject);
                Mail::Send($suportModel->email, $suportModel->subject, $form->body, $form->email, $form->name);
                $this->render('send');
                return true;
            }
        }
        $this->redirect('contact');
    }

}
