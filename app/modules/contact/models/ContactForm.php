<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel {

    public $name;
    public $email;
    public $subject;
    public $body;
    //public $verifyCode;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('name, email, subject, body', 'required'),
            array('email', 'email'),

        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'name' => ContactModule::t('Имя'),
            'email' => ContactModule::t('Почтовый ящик'),
            'subject' => ContactModule::t('Тема сообщения'),
            'body' => ContactModule::t('Сообщение'),
        );
    }

    public function getAttributeLabel($attribute) {
        return parent::getAttributeLabel($attribute) . ' <i onClick="$(this).parent(\'.error_message\').fadeOut(\'fast\')" class="close_icon"></i>';
    }
    /**
     * Отправляет почту исходя из данных модели.
     * @param type $from E-mail отправителя
     * @param type $to E-mail получателя
     * @return boolean
     */
    public function send($from, $to){
        if($this->validate()){
            return self::mail($from, $to, $this->subject, $this->body, $this->name, '');
        }
        return false;   
    }
    /**
     * Отправка почты
     * @param string $from E-mail отправителя
     * @param string $to E-mail получателя
     * @param string $subject Тема сообщения
     * @param string $body Текст сообщения
     * @param string $from_name Имя отправителя
     * @param string $to_name Имя получателия
     * @return bool
     */
    public static function mail($from, $to, $subject, $body, $from_name = '', $to_name = ''){
//        CVarDumper::dump(func_get_args(), 10, true);
//        exit;
        $mail = Yii::app()->mail;
        $mail->SetFrom($from, $from_name);
        $mail->Subject = $subject;
        $mail->MsgHTML($body);
        $mail->AddAddress($to, $to_name);
        //$mail->AddReplyTo('kiliman6aro@gmail.com', $from_name);
        return $mail->Send();
    }

}
