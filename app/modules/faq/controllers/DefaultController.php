<?php

class DefaultController extends Controller {
    public $layout = '/layouts/column2';

    public function actionIndex() {
        $models = Faq::model()->findAll();
        $this->render('index', array('models' => $models));
    }

}
