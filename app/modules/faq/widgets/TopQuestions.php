<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TopQuestions
 *
 * @author Администратор
 */
class TopQuestions extends CWidget{
    
    const MODE_WIDGET   = 'widget';
    
    const MODE_MENU     = 'menu';

    public $mode = self::MODE_WIDGET;
    
    public $model;

    public function run(){
        $models = $this->getModel();
        if($this->mode == self::MODE_MENU){
            $this->render('items', array('models' => $models));
        }else{
            $content = $this->render('items', array('models' => $models), true);
            $this->render('index', array('models' => $models, 'items' => $content));
        }
        
        
    }
    
    public function getUrl($key){
        $url = Yii::app()->createUrl('faq');
        return $this->mode == self::MODE_MENU ? '#'.$key : $url.'#'.$key;
    }
    protected function getModel(){
        $criteria = self::MODE_WIDGET ? 'favorite = 1' : '';
        return !$this->model ? Faq::model()->findAll($criteria) : $this->model;
    }
}
