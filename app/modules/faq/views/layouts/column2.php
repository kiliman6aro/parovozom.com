<?php $this->beginContent('//layouts/main'); ?>
<div class="content faq_block">
    <div class="container">
        <div class="tbl_layout">
            <div class="l_column question_list">
                <?php $this->widget('faq.widgets.TopQuestions', array('mode' => 'menu')); ?>
            </div>
            <div class="r_column">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>