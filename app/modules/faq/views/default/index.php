<h1><?php echo Yii::t('app', 'FAQ'); ?></h1>
<?php foreach ($models as $model): ?>
    <div class="info_txt">
        <h5><a name="<?php echo $model->key ?>"><?php echo $model->question; ?></a></h5>
        <p><?php echo $model->answer; ?></p>
    </div>
<?php endforeach; ?>