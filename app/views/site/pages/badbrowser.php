<?php $this->wrapper_class = 'bad_page'; ?>
<div class="info">
    <p>Ваш браузер устарел и не подходит для пользования нашим сервисом.</p>
    <p>Обновите браузер или воспользуйтесь одним из последних версий:</p>
</div>
<div class="browsewrs_list">
    <ul>
        <li>
            <p><a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/media/images/browsers/img1.png" width="110" height="115" alt=""/></a></p>
            <p><a href="">Google Chrome</a></p>
        </li>
        <li>
            <p><a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/media/images/browsers/img2.png" width="110" height="115" alt=""/></a></p>
            <p><a href="">Mozilla Firefox</a></p>
        </li>
        <li>
            <p><a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/media/images/browsers/img3.png" width="110" height="115" alt=""/></a></p>
            <p><a href="">Opera</a></p>
        </li>
        <li>
            <p><a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/media/images/browsers/img4.png" width="110" height="115" alt=""/></a></p>
            <p><a href="">Safari</a></p>
        </li>
        <li>
            <p><a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/media/images/browsers/img5.png" width="110" height="115" alt=""/></a></p>
            <p><a href="">Internet Explorer</a></p>
        </li>
    </ul>
</div>