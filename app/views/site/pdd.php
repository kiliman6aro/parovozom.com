<style>

    .clear {
    display: block;
    clear: both;
    font-size: 0;
    height: 0;
    margin: 0;
    padding: 0;
    overflow: hidden;
    line-height: 0;
}

.center {
    text-align: center;
}

.bold {
    font-weight: bold;
}

.nowrap {
    white-space: nowrap;
}

#middle {
    min-height: 100%;
}

#middle > div {
    min-height: 0;
    background: none;
    border: none;
}

#middle > div > div, #content {
    margin: 0 auto;
}

#middle > div > div {
    width: 1000px;
    margin: 0 auto;
    background: none;
    padding: 0;
}

#content {
    background: none;
    width: 998px;
    min-height: 480px;
    padding: 0;
}


.arial, .uid {
    font-family: arial, sans-serif;
    font-size: 16px;
}

.v-top {
    vertical-align: top;
}

.align-right {
    text-align: right;
}

.w1 {
    width: 1%;
}

.w-right {
    width: 160px;
}

.upper {
    text-transform: uppercase;
}

.uid {
    font-size: 28px;
    white-space: nowrap;
}

.order_num {
    font-size: 21px;
    font-weight: bold;
}

.address {
    font: 13px 'Times New Roman', Times, serif;
}

.ticket_info {
    font-weight: bold;
    font-size: 17px;
}

#content .result h2 {
    font-size: 36px;
    margin: 20px 0;
}

#content .better_print {
    font-size: 16px;
    font-weight: bold;
    margin: 20px 0;
    text-align: center;
}

#content .controls {
    text-align: center;
}

#content .controls button {
    font-size: 22px;
    padding: 5px 20px;
    margin-left: 20px;
}

.cheque {
    padding-bottom: 20px;
}

.cheque .text h3, .cheque .text p, .cheque-text {
    text-indent: 30px;
    text-align: justify;
}

.cheque .text p {
    font-size: 18px;
    margin: 5px 0 10px;
    line-height: 1.5;
}

.cheque .text h3, .cheque-text {
    font-size: 28px;
    margin: 20px 0;
}

.cheque .text b {
    font-size: 24px;
}

.cheque .blank {
    border-top: 1px dashed;
    border-bottom: 1px dashed;
    padding: 10px;
}

.cheque .blank .header_doc {
    font-weight: bold;
    font-size: 34px;
}

.cheque .blank .orderDate {
    font-size: 14px;
}

.cheque .blank .barcode {
    padding-bottom: 5px;
}

.cheque .blank table {
    width: 100%;
    background: #000;
    border-collapse: collapse;
}

.cheque .blank table tr {
    background: #fff;
}

.cheque .blank table td, .cheque .blank table th {
    padding: 2px 5px;
    border: 1px solid;
}

.cheque .blank table th {
    text-align: left;
}

.cheque .blank table.info th {
    font-weight: normal;
    width: 225px;
}

.cheque .blank table.info td, .cheque .blank table.places th {
    font-weight: bold;
    font-size: 15px;
}

.cheque .blank .places {
    margin-top: 10px;
}

.cheque .blank .places span {
    font-weight: normal;
}

.cheque .blank .prices {
    padding: 5px 0 10px;
}

.cheque .blank .price_info {
    border: 0;
    border-collapse: collapse;
}

.cheque .prices h4 {
    font-size: 14px;
}

.cheque .electronic {
    font-family: "Courier New", monospace;
    font-size: 16px;
}

.cheque .electronic .info p {
    font-size: 15px;
    font-weight: bold;
    padding-left: 1em;
}

.cheque .electronic .part1 tr:first-child {
    height: 42px;
}

.cheque .electronic .part2 tr:first-child td {
    border-top: none;
}

.cheque .electronic tr:first-child td:first-child {
    width: 5em;
}

.cheque .electronic .barcode {
    padding: 5px 0 0;
}

.cheque .blank .ch-num-l {
    white-space: nowrap;
    border-right: none;
}

.cheque .blank .ch-num {
    white-space: nowrap;
    border-left: none;
}

.cheque .blank.electronic .ch-num {
    width: 1px;
    text-align: right;
}

#content .return-mrg {
    margin-bottom: 20px;
}

.hostName {
    display: none;
}

@media print {
    @page {
        margin: 1cm .5cm;
    }

    body {
        background: #fff;
        overflow-y: visible;
    }

    #middle > div {
        min-height: 0;
        background: none;
        border: none;
    }

    #middle > div > div, #content {
        margin: 0 auto;
        background: none;
        padding: 0;
    }

    #header, #footer, #helloUser, .header, .footer, .btn_block, .attention_tooltip, .result, .controls, .version, .no_print, #content > div > p {
        display: none;
    }

    .cheque .page-break {
        page-break-after: always;
    }

    .testMode .hostName {
        display: block;
    }
}
</style>
<div class="img_block">
    <div class="cheque">
        <div class="text">
            <!--<h3>Бланк замовлення на друк cплачених<br/>проїзних / перевізних документів</h3>-->
            <p><strong><?php echo Yii::t('app', 'Даный бланк заказа для проезда недействительный.'); ?></strong> <?php echo Yii::t('app', 'Перед поездкой необходимо предоставить этот бланк заказа в любую автоматизированную билетную кассу Украины и распечатать проездные и перевозочные документы до отправления поезда. При отсутствии возможности распечатать содержимое данного бланка, пожалуйста, запишите номер заказа с приведенной ниже таблице.'); ?></p>
        </div>
        <div class="blank">
            <div class="header_doc"><?php echo Yii::t('app', 'БЛАНК ЗАКАЗА'); ?></div>
            <div class="orderDate">
                <?php echo Yii::t('app', 'Дата и номер заказа') ?>: 2015-06-23 12:06:59
            </div>
            <div class="clear"></div>
            <div class="barcode">
                <div class="header_doc">КОК-Т2-0368545-2306</div>
                <img width="500" alt="barcode" src="data:image/gif;base64,R0lGODdhtgE6AHcAACwAAAAAtgE6AIcAAAD///9CQkJCQkJCQkIAAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADpazDpbDHpc6jpztkIBowAAAAAAQAY8HQAAALpeZPpeXUY8HAAAQAY9PRQ7kAY8HAAAQAAAwAAAAAAAIAAgAAAgICAAACAAICAgADAwMDA3MDwyqYzAAAAADMzADMAAAMY8OjpbRAAABgAAAAAAAAAACgAAADprBMY8UQAAADprZ7prSMBClgAAAAAAACF2qAAAAAAACgAAAAAAAAY8SyF2sgY9NjprYAAAAAAAAAAAAAY9EQAACgAAACF2qAAAAC0AAAFBDWF2qAAAAAAmf8AzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACZMzOZAGaZM8yZAP+ZZgCZZjOZM2aZZpmZZsyZM/+ZmTOZmWaZmZmZmcyZmf+ZzACZzDNmzGaZzJmZzMyZzP+Z/wCZ/zOZzGaZ/5mZ/8yZ///MAACZADPMAGbMAJnMAMyZMwDMMzPMM2bMM5nMM8zpXz7pXx0BCgsAAAAAAAAAAbYAADoBClVQ+p2OCEAAAAZRDP4Y9eCOCEAAAAHpWWoBClWF2siFAA+IAAsAAAAAAAHpVOnpUIoAAAAY86zpazDpbDEFBDUAAFQAAAIAABBAfVAY88hAfV8Y88AY9eCOCEAAAAGOCEAY8+Bclz4Y8+xAfQEY8+COCEAI/wABCAQQIIDAggUPIlw4kCDChgwHJmzoUOFEiwYlPoSYMWJEjhcrapz4USNGig5Ddtx4MuVGkgszolTJ0iXHliNligS5kqdPijBpBtV5EKfJnD9v2lyq1GjJpTBRtiwZk6jIkEc9VtVJtSbWq17DYh1q1CnXlzUVlmVK0OzQqC6/qkVKd+7TrjPRJmVrV+zKr3+tCj0LOPDawXuh9pSqdXDhuUANx/W79zHbrmMlP71MWLLSzVTdBoZbVepOzjTL4o0MV3Re13zbxkZcNytay7DptmbtefLr07bfUq5tGXPnyFPTcv4JurdT2KXBboaM+qzqsHl3G/+9ffbh47X7kv9FTjytVurM/Ra3Kn30cLPc3/Md3zy1buXjK0OPKV459e798dbUfZXh11tjgn1X4Hr5HZVbcujd55hgi5GHIIDLWSjfXZoZaF9yDFKo0n7n+cYYePDNh52Ap2EYoIQJzoaijCBSuCBRJcql2ITxnVgjhqt9tqF5HRb24Y42yjhidRiRxp+P5dnH4ZEQRplkd6ApaGV8Df4nn2IR1sjTelBeOCSV7d2oYZcZirmmiFyRqJeJvz3YZZAEbvlmeoBp+eCLzamZXphIjnkle/RtJWiEDbqYqJHWwcglnBDJ6eR0LQ55HZol/pmmpED5yeSeNOqZp44IGtrjpE2eGamQtF3/52aBfB4KaaWjRiecabFmOOWreXra6G0xBmpqcM5l2iuYwIHa6qqkbjUjni/++SilDhZKqn65PrkrlHY6Ry2zwh5I7EyiuvipsdbOieq5OZHJ6mS9jjvsstdmB2yq2xJn6WLR1TlqtSkOeizB/OLK7sDIpvtljuxp+6yGZRIL5IqwTkvkrOXVOi+3WHrrHq8aS0kkp3OWKye6paoscbaOuhtxwnTaVrFw9WLc8MH5suglm0B79W9/7y76accs5hyix6G2zDCgBhrN7Lv3Rt3suooeuzTPRWL7c7JBLxkyxJgqHemvA1bp8tjFWr02vAPG7CTJVcsbbdbhfvx0z7Q6/7szyMPpOjK4DN+ps9pPH/1yUU6ryzfMD8tMt7nGynXtxSiHmxnHBS+u+HPdku2fshpvCizijlMOtsPLYo3b3pKfWPehN7tndtqKx8x5m56HHWfoMpdd8tknn05u4lUfxvqMUL8uN8Azw11zX7W3enu2CEe+O540Eww625eOfr339qaMvOrKN/6l60m2O7fsqtudsfXD4558eI933v3nlw3dXtFaE9fhjpe6lTVtYQXsXf0MRrX40W5e0jLa1tq1PZ3tz3cKC5zIJiawi5nOfuZLINtYhsD15Y90iYHY5EQjv51FMIB6050Cj3RBsAkNeOEDYN5UhDLtmBB93lFf6/9OOL4qNZCFD7xbwApnqwW60EP7kp73+odD6AlPggLsYYXOZ8AMOu+HFZSaCuGHRGjNj15ObNb9ZDXDKAbtO2LToOh0yMQPYo+AYAQeCd3GRQWKMXb6KiPFIIi3OvZLhjWkoRT5F5oqEi16WLQjo0KYxxEekI8iXGQRpxa92ZnxiUv04CG118a4adKGcaSN4DhInh2i7Y6d6qMlvdg+WZ7yj+8LZHVa2Lwn7fCMFCwl5N6opN+Bz4riS6PhtPi1xOROk8tzJhHTyMkV7jKJZ3zhL/8Guwoqkpj1MaYcg5fMSPLQeLHMpCrxE83wsK+Dkctl0tSDTVD60pDA7KYwm1n/qnDSslY5hCQMzwlCHw4RiFkSIvPe2UrYyZNprKQeIUOpqVHiq2u65GfloPhPGAWUZOZ85SQNutD7JbSEBw0j1x7qtyVKVIn3FGU+EXnKb9oQjuJc5xwF6sriFXSL6gTPSTFZyVuuFHrWNA4v2Yc5dAKRjYl0402L2dEfIZOOohSpGimZUj1e8otdrSEukUrGa36yl00FoeZwhFGfobCfHJWI/74Fz5xJcqsklSZC2alQvarUfWTVpVLriVZXqZWJm9vnWzd6K7k68n88NaRWG7vJZwKtnVoS61FfOk8eDRKmaYXlU1XkTalmNq6MO+YjQTrQyWKrsiblK0pL2lZT/16UpRxzKViqh0ZzzpSUUbXtVP3pWNVClrU9zQwz33panbYNrLT9q0MD29nOLPVyhhXtWmGa0cVajbipHedHCZdVn4o2r+6MbZ/6mt7aDpN5YxQsPc/K1OxOcrvZRK13J+ivx9K1oeVVrlPR21yhypaoYTUqYDkL0entdqIxrehvL1pa4Ra4u3vdKXIla96RArWozt0jdP3qx80+WL6eFRJvtYlPbtIUg++9sFszTE6sVtS18pWaZVfHXhnPNoWArK6qPptfitrVovB1r/6MCl7ZGPe/FkYajuepY/U+t5ZBxSxBTyzkeBG2vkXk74tRaVq4Nja8zh1vXUu3ZQ/zU//G29MyY6e6YC43WLe7sWdo74tYtlY4xmb2mpPFe9XIBpity4UtjUWMZRDTeboMbuk9Iw1mZTbxj3/W6Hf1S2M1A/jGHcbrhxMc4q82mtSPfh6lX4bnLerZvlvFrz0xbGD+fo/QqyUvqAX80zcHusFoPnV0S1znPEt6oysupExdDNyalnnOGJ7r4Nbctykb7G2lriqgfYzgJOOW1ZO2c2HDfGmuZXq/5QZcmgu94UPP+LXU3HH6ftxe6apa3LkNt7Er7VtmU1ix2ebmrdeda2pLOdSUjbeVGW1wbo8Yf0G+s75dPW5Lx7DZMNa0ran45Gl/+sjWDpYtAx7shv+6x1v/3je4kQ3hPce6z9x9N7AZecOOR3TbvkI4vKu8aFObHNoof6XKadbqMGGX3Bf/d3ABDXR117rGht41ogc86mHPsrjdJrFmi03xfLMctLAWM8bJHOWm99fmDvb1akKOOkfPPNVuD7p5hy69ol/t6BZHMpDPTXKap/Lpno5yitiOR1S/Xc6bJibXCUX0iRu9ay4Xu9KdXfbEC7rT7NY1yHWe44PJO4j0driwgfztxn+9yBE+8oSTzPen623ggM/8z1dE+HTGPc5y31roU971lW862UZm8+r3DnDX9wv2AJX9x9lce65afZwMX77WFQxpfPv+upAPe7r1uXSNbx/5HlW+N+BzzuvzVn36fYe74XM/9VWb/vct137SWV/8t5Md6wQ/ruaZz3kqe37hPid99UZs1Ud3nvRZAQEAOw=="/>
            </div>
            <table class="info">
                <tr>
                    <th><?php echo Yii::t('app', 'Станция отправления') ?>:</th>
                    <td>23423423</td><td>Днепропетровск-Главный</td>
                </tr>
                <tr>
                    <th ><?php echo Yii::t('app', 'Станция назначения') ?>:</th>
                    <td>423432432423</td><td>Киев-Пассажирский</td>
                </tr>
                <tr>
                    <th colspan="2"><?php echo Yii::t('app', 'Дата и время отправления') ?>:</th>
                    <td>Время отправления</td>
                </tr>
                <tr>
                    <th class="ch-num-l"><?php echo Yii::t('app', 'Дата и время прибытия') ?>:</th>
                    <td class="ch-num">
                    </td>
                    <td>Время прибытия</td>
                </tr>
            </table>
            <table class="places">
                <tr>
                    <th><?php echo Yii::t('app', 'Номер поезда') ?>: 079 ПА</th>
                    <th><?php echo Yii::t('app', 'Вагон') ?>: 12 П/М/ДН</th>
                    <th><?php echo Yii::t('app', 'Место') ?>: 23<span>, полный</span></th>
                </tr>
                <tr>
                    <th>Билык Павел</th>
                    <th colspan="2">000B3A1C-3F5EF26F</th>
                </tr>
            </table>
            <div class="prices">
                <h4><?php echo Yii::t('app', 'ИНФОРМАЦИЯ О СТОИМОСТИ ЗАКАЗА') ?>, грн.</h4>
                <table class="price_info">
                    <tr class="center bold">
                        <td colspan="4"><?php echo Yii::t('app', 'Проездные документы') ?></td>
                    </tr>
                    <tr class="center bold">
                        <td><?php echo Yii::t('app', '№ доку-<br/>мента') ?></td>
                        <td><?php echo Yii::t('app', 'Места') ?></td>
                        <td><?php echo Yii::t('app', 'Стоимость проездного документа') ?>, грн. (<?php echo Yii::t('app', 'Тариф') ?>, <?php echo Yii::t('app', 'КСБ.') ?>, <?php echo Yii::t('app', 'СТР.') ?>, <?php echo Yii::t('app', 'НДС.') ?>)</td>
                        <td><?php echo Yii::t('app', 'Сбор за оформление ППД без НДС') ?>, грн.</td>
                    </tr>
                    <tr class="center">
                        <td>1</td>
                        <td>23</td>
                        <td>145.70 грн</td>
                        <td>25.40 грн</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="nowrap bold"><?php echo Yii::t('app', 'Общая стоимость') ?></td>
                        <td class="center" colspan="2">145.70 грн</td>
                    </tr>
                </table>
            </div>

            <div>ГІOЦ УЗ</div>
        </div>
        <div class="hostName">[<?php echo Yii::app()->getBaseUrl(true) . '/'; ?>]</div>
        <div class="clear"></div>

        <div class="clear"></div>
    </div>
</div>