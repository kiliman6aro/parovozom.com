<div class="content error_page">
    <div class="container">

        <div class="image"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/media/images/error_image.png" alt=""></div>
        <div class="error_title">
            Извините,<br>по вашему запросу ничего не найдено
            <div class="error_notice">Попробуйте еще раз</div>
        </div>
        
        <div class="form_train">
        <?php $this->widget('booking.widgets.search.SearchForm', array('direction' => Yii::app()->session['direction'])); ?>
        </div>
    </div>
</div>

<style rel="stylesheet" type="text/css">
    
.ui-datepicker.ui-datepicker-multi-2::before {
    top: 100%;
    -webkit-transform: rotate(180deg);
            transform: rotate(180deg);
}
</style>