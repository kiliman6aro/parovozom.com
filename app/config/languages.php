<?php
 return array(
    'sourceLanguage'=>'ru',
        'sourcePath'=>dirname(__FILE__).'/../',
        'messagePath'=>dirname(__FILE__).'/../messages',
        'languages'=>array('ru'),
        'fileTypes'=>array('php'),
        'overwrite'=>true,
        'exclude'=>array(
                '.svn',
                '.gitignore',
                'yiilite.php',
                'yiit.php',
                '/i18n/data',
                '/messages',
                '/vendors',
                '/web/js',
                '/yii',
                '/extensions',
                '/migrations',
        ),
        'removeOld' => true,
        'sort' => true,
);