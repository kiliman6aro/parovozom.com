
function BGIntro(m) {
    
    this.stack = {};
    
    this.intro = introJs();
    
    this.isStarted = false;
    
    var self = this;
    
    function _initIntro(s){
        self.intro.onexit(function() {
            self.isStarted = false;
        });
        self.intro.onafterchange(
            function(e){$(e).focus();
        });
        self.intro.setOptions({keyboardNavigation:false, steps:s, scrollToElement:true});
        
    }
    
    function _start(){
        if($.cookie('tour')){
            return true;
        }
       self.intro.start();
       self.isStarted = true; 
       $.cookie('tour', '1', {expires: 31, path: '/' });
    }
    function _updateIntro(steps){
        intro.update(steps);
        if(self.isStarted){    
            intro.nextStep();
        }
    }
    /**
     * Если передан селектор (можно получить объект DOM по содержанию
     * переменной selector), то будет возвращён объект DOM.
     * В противном случае, преподагалется что это литеральная ссылка
     * на объект JavaScript и к переменной будет применена конструкция
     * eval();
     * @param {mixed} selector
     * @returns {startIntro._createObject.el}
     */
    function _createObject(selector){
        var el = $(selector);
        if (!el.length) {
            return eval(selector);
        }
        return el;
    }
    
    function _nextStep(){
        if(self.isStarted){
            self.intro.nextStep();
        }
    }
    for (var i = 0; i < m.length; i++) {
        var item = m[i];
        var obj = _createObject(item.element);
        self.stack[item.event] = item;
        $(obj).on(item.event, function(e) {
            var item = self.stack[e.type];
            var steps = eval(item.steps);
            if(this === document && e.type === 'ready'){
               _initIntro(steps);
               if(item.start_step > 0){
                   self.intro.goToStep(item.start_step);
               }
               _start();
            }else{
                _updateIntro(steps);
            }
        });
    }
    $(Purchase).on('show', function(){
        var item = $('.result_item.open');
        item.on('change', function(){
            self.intro.refresh(); 
        });
        item.on('click', function(){
            self.intro.refresh(); 
        });
    });
    $('#from').on('change', function(){
        _nextStep();
    });
    $('#to').on('change', function(){
        _nextStep();
    });
    $('#startDate').on('change', function(){
        _nextStep();
    });
    return self.intro;
}
var intro = BGIntro(map);
