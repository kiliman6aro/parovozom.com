introJs.fn.update = function(steps){
	 /**
	   * Generate new link item
	   *
	   * @api private
	   * @method _generateElement
	   * @param {int} step
	   * @returns {object} Link element
   */
	function _generateElement(step){
		var self = this;
		var innerLi    = document.createElement('li');
        var anchorLink = document.createElement('a');
		
		anchorLink.onclick = function() {
          self.goToStep(this.getAttribute('data-stepnumber'));
        };
		
		anchorLink.href = 'javascript:void(0);';
        anchorLink.innerHTML = "&nbsp;";
        anchorLink.setAttribute('data-stepnumber', step);
		innerLi.appendChild(anchorLink);
		return innerLi;
	}
        //Обїеденяет массив существующих steps с теми, что присутствуют
	this._options.steps = this._options.steps.concat(steps);
        if(this._introItems === undefined){
            return this;
        }
	var len = steps.length;
	var tempSteps = [];
	var i = 0;
	for(var i = 0; i < len; i++){
		var currentItem = steps[i];
		var step = this._introItems.length+i+1;
		currentItem.element = document.querySelector(currentItem.element);
		document.querySelector('.introjs-bullets ul').appendChild(_generateElement.call(this, step));
		currentItem.step = step;
		tempSteps.push(currentItem);
	}
	this._introItems = this._introItems.concat(tempSteps);
	return this;
};
