<?php
Yii::import('widgets.tour.models.*');
/**
 * Description of Intro
 *
 * @author Администратор
 */
class IntroJS extends CWidget {
    
    protected $items = array();
    
    protected $assets;

    public function init(){        
        $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets', false, -1, YII_DEBUG);
        
        Yii::app()->clientScript->addPackage('introjs', array(
            'baseUrl' => $this->assets,
            'js' => array(YII_DEBUG ? 'intro.js' : 'intro.min.js', YII_DEBUG ? 'intro.update.js' : 'intro.update.min.js', 'jquery.cookie.js'),
            'css' => array(YII_DEBUG ? 'introjs.css' : 'introjs.min.css'),
            'depends' => array('jquery'),
        ));
    }

    public function run() {
        Yii::app()->clientScript->registerPackage('introjs');
        Yii::app()->clientScript->registerScript('intro-index-initialize', 'var map = ' . CJSON::encode($this->_getItems()) . ';', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($this->assets . '/bgintrojs.js', CClientScript::POS_END);
        //Перевод тура. Если файл с переводом найден, то подключаем
        $lang_file = dirname(__FILE__) . '/assets'.DS.'intro_lang_'.Yii::app()->language.'.js';
        if(file_exists($lang_file)){
            Yii::app()->clientScript->registerScriptFile($this->assets . '/intro_lang_'.Yii::app()->language.'.js', CClientScript::POS_END);
        }
    }
    /**
     * Возвращает имя текущего модуля, или null
     * @return String
     */
    private function _getModuleId(){
        return $this->getOwner()->getModule() ? $this->getOwner()->getModule()->getId() : $this->getOwner()->getId();
    }
    /**
     * Получает из БД элементы и наполняет ими
     * массив
     */
    private function _getItems(){
        $criteria = new CDbCriteria();
        $criteria->compare('action', $this->getOwner()->getAction()->getId());
        $criteria->compare('enabled', true);
        $criteria->compare('module_id', $this->_getModuleId());
        $models = IntroMap::model()->findAll($criteria);
        $items = array();
        foreach ($models as $model){
            $item = array();
            $item['element'] = $model->element;
            $item['event'] = $model->event;
            $item['start_step'] = $model->start;
            $item['steps'] = $model->steps;
            array_push($items, $item);
        }
        return $items;
    }

}
