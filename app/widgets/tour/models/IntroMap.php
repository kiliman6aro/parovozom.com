<?php

/**
 * This is the model class for table "{{intro_map}}".
 *
 * The followings are the available columns in table '{{intro_map}}':
 * @property integer $id
 * @property string $element
 * @property string $event
 * @property string $steps
 * @property string $action
 * @property string $lang
 * @property integer $enabled
 * @property integer $start
 * @property string $module_id
 */
class IntroMap extends BaseActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return IntroMap the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{intro_map}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('element, event, steps, action', 'required'),
            array('enabled, start', 'numerical', 'integerOnly' => true),
            array('element, event', 'length', 'max' => 32),
            array('action, module_id', 'length', 'max' => 100),
            array('lang', 'length', 'max' => 3),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, element, event, steps, action, lang, enabled, start, module_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'element' => 'Element',
            'event' => 'Event',
            'steps' => 'Steps',
            'action' => 'Action',
            'lang' => 'Lang',
            'enabled' => 'Enabled',
            'start' => 'Start',
            'module_id' => 'Module',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('element', $this->element, true);
        $criteria->compare('event', $this->event, true);
        $criteria->compare('steps', $this->steps, true);
        $criteria->compare('action', $this->action, true);
        $criteria->compare('lang', $this->lang, true);
        $criteria->compare('enabled', $this->enabled);
        $criteria->compare('start', $this->start);
        $criteria->compare('module_id', $this->module_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
