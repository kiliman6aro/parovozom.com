<?php

class AdvicesWidget extends CWidget{
    
    public $page = 'index';

    public function run(){
        Yii::import('widgets.advices.models.*');
       $assets = Yii::app()->assetManager->publish(Yii::getPathOfAlias('widgets.advices.media'), false, -1, YII_DEBUG);
       $cs = Yii::app()->clientScript;
       Yii::app()->clientScript->registerCoreScript('jquery');
       $cs->registerScriptFile($assets.'/js/jquery.easing.1.3.js');
       $cs->registerScriptFile($assets.'/js/jquery.touchSwipe.min.js');
       $cs->registerScriptFile($assets.'/js/jquery.liquid-slider.min.js');
       $models = Advances::model()->findAll();
       if(!count($models))
           return true;
       $this->render('index', array('models' => $models)); 
    }
}

