<div class="info_block">
    <div class="text_block">
        <div class="liquid-slider" id="main-slider">
            <?php foreach ($models as $models):?>
            <div class="user_content">
                <h3 class="title"><?php echo $models->title ?></h3>
                <p><?php echo $models->advance; ?></p>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
<i class="close_icon" onClick="$(this).parent('.info_block').fadeOut('fast');"></i>
</div>
<?php 
Yii::app()->clientScript->registerScript('slider', 
    '$("#main-slider").liquidSlider({
        slideEaseDuration: 800,
        autoSlide: true,
        autoSlideInterval: 8000
    });'
); 
?>