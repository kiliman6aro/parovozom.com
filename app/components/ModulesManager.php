<?php

class ModulesManager extends CApplicationComponent {

    private $_pathToModules;

    public static function getPathToConfig($module) {
        return ModulesManager::getPathToModules() . DS . $module . DS . 'bootstrap.php';
    }

    public static function getPathToModules() {
        return Yii::getPathOfAlias('application.modules');
    }

    public static function getModule($id) {
        if (Yii::app()->getModule($id) != null) {
            return Yii::app()->getModule($id);
        }
        return null;
    }

    public function start() {
        $dirs = scandir(ModulesManager::getPathToModules());
        foreach ($dirs as $name) {
            if ($name[0] == '.')
                continue;

            $config = require_once (ModulesManager::getPathToConfig($name));
            $this->register($config);
        }
    }

    /**
     * Registers a module
     * This is usally called in the autostart file of the module.
     *
     * - id
     * - class              Module Base Class
     * - import             Global Module Imports
     * - events             Events to catch
     * - urlManagerRules    Rules
     * - modules            Submodules
     *
     * @param Array $definition
     */
    public function register($definition) {
        if (!isset($definition['class']) || !isset($definition['id'])) {
            throw new Exception("Register Module needs module Id and Class!");
        }
        // Handle Submodules
        if (!isset($definition['modules'])) {
            $definition['modules'] = array();
        }
        if(isset($definition['controllerMap'])){
            Yii::app()->controllerMap = array_merge(Yii::app()->controllerMap, $definition['controllerMap']);
        }
        // Append URL Rules
        if (isset($definition['urlManagerRules'])) {
            Yii::app()->urlManager->addRules($definition['urlManagerRules'], false);
        }
        //Append components
        if(isset($definition['components'])){
            Yii::app()->setComponents($definition['components']);
        }
        // Register Yii Module
        Yii::app()->setModules(array(
            $definition['id'] => array(
                'class' => $definition['class'],
                'modules' => $definition['modules']
            ),
        ));
        //Register Params for module
        if (isset($definition['params'])) {
            try {
                ModulesManager::getModule($definition['id']) . setParams($definition['params']);
            } catch (Exception $ex) {
                throw $ex;
            }
        }
        // Set Imports
        if (isset($definition['import'])) {
            Yii::app()->setImport($definition['import']);
        }
        // Register Event Handlers
        if (isset($definition['events'])) {
            foreach ($definition['events'] as $event) {
                Yii::app()->interceptor->preattachEventHandler(
                        $event['class'], $event['event'], $event['callback']
                );
            }
        }
    }

}
