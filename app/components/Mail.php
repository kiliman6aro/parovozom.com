<?php
Yii::import('application.extensions.yii-mail.YiiMailMessage');

class Mail {
    
    public static function Send($to, $subject, $body, $from = null, $from_name = '') {
        if(Yii::app()->mail instanceof YiiMail == false || !class_exists('YiiMailMessage'))
            throw new CHttpException(500, Yii::t('app', 'Не найден компонент https://code.google.com/p/yii-mail'));
        
        $message = new YiiMailMessage;
        $message->setCharset(Yii::app()->charset);
        $message->setSubject($subject);
        $message->setTo($to);
        $message->setBody($body,'text/html');
        $message->setFrom(Yii::app()->mail->transportOptions['username']);
        
        if($from)
            $message->setReplyTo($from, $from_name);
        
        Yii::app()->mail->send($message);
    }
}
