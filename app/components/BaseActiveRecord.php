<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseActiveRecord
 *
 * @author Администратор
 */
class BaseActiveRecord extends CActiveRecord{
    
    public function defaultScope() {
        return array(
            'condition' => 'lang = :l OR lang = "*"',
            'params' => array(':l' => Yii::app()->language)
        );
    }
    
}
