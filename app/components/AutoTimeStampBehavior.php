<?php

/**
 * Description of AutoTimeStampBehavior
 *
 * @author kilim
 */
class AutoTimeStampBehavior extends CActiveRecordBehavior{
    
    public $createdField = 'create_at';
    
    public function beforeValidate($event) {
        if($this->owner->isNewRecord)
            $this->owner->{$this->createdField} = new CDbExpression('NOW()');
            
        return true;
    }
}
