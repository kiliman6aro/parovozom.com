<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RequestHandler
 *
 * @author Администратор
 */
class RequestHandler {

    public static function request($event) {
        Yii::app()->charset = Settings::Get('charset');
        Yii::app()->language = Settings::Get('language');
        Yii::app()->name = Settings::Get('name');
        
        /**
         * Общая почта для транпсортировки почты. Умеет подставлять email
         * для ответа, но не умеет прикреплять файлы в письмо в исходном
         * виде (без физеческого создания на диске)
         */
        Yii::app()->setComponent('mail', array(
            'class' => 'application.extensions.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => Settings::Get('host', 'contact'),
                'port' => Settings::Get('port', 'contact'),
                'username' => Settings::Get('username', 'contact'),
                'password' => Settings::Get('password', 'contact'),
                //'encryption' => 'tls',
            ),
            'charset' => Yii::app()->charset
        ));
        
        /**
         * Не умеет подставлять email для ответа, но умеет
         * прикреплять файлы в source виде (физически несуществующие)
         */
        Yii::app()->setComponent('smtpmail', array(
            'class' => 'contact.extensions.smtpmail.PHPMailer',
            'Host'=>  Settings::Get('host', 'contact'),
            'Username'=> Settings::Get('username', 'contact'),
            'Password'=> Settings::Get('password', 'contact'),
            'Mailer'=>'smtp',
            'Port'=> Settings::Get('port', 'contact'),
            'CharSet' => 'UTF-8',
            'SMTPAuth'=>true, 
            'SMTPSecure' => 'tls'
        ));
        
        if(YII_DEBUG){
            $req = Yii::app()->request;
            Yii::app()->db->createCommand()
                    ->insert('{{http_requests}}', array(
                        'ip' => $req->userHostAddress,
                        'user_agent' => $req->userAgent,
                        'uri' => $req->requestUri
                    ));
            }
    }
    
    /**
     * Обработка ошибок
     * @param CExceptionEvent $event
     */
    public static function exception(CExceptionEvent $event){
        $exception = $event->exception;
        $controller = new CController(null);
        if($exception instanceof CHttpException && $exception->statusCode == 404){
            $controller->layout = '/layouts/error';
            $event->handled = true;
            $controller->render('//errors/404');
            return true;
        }
        //Если исключение сгенерированно ошибкой на укрзализныце
        if($exception instanceof TransactionException){
            if($exception->getCode() == 104 || $exception->getCode()  == 4041 || $exception->getCode()  == 38){
                $event->handled = true;
                $controller->layout = '/layouts/error';
                $controller->render('//errors/400');
                return true;
            }
            $event->handled = true;
            $controller->render('//errors/500', array('message' => Yii::t('app', 'Сервер "Укразалізниці" не отвечает')));
            return true;
        }
        $event->handled = true;
        $controller->render('//errors/500', array('message' => Yii::t('app', 'Пожалуйста, попробуйте воспользоваться сервисом позже')));
    }
    
}
