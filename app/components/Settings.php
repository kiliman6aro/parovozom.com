<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Settings
 *
 * @author Администратор
 */
class Settings extends CApplicationComponent{
    
    public $cache = 0;
    
    public $dependency;
    
    public $table = '{{settings}}';
    
    protected $data = array();
    
    /**
     * Возвращает значение парамтра
     * @param String $name имя параметра
     * @param String $moduleId имя модуля
     * @return String значение
     * @throws CException
     */
    public static function Get($name, $moduleId = null){
        if(Yii::app()->config instanceof Settings){
           return Yii::app()->config->param($name, $moduleId); 
        }
        throw new CException(Yii::t('error', 'Компонент Settings не подключен в конфигурационном файле'));
    }
    
    public function init() {
        $db = $this->getDbConnection();
        $items = $db->createCommand("SELECT * FROM {$this->table}")->queryAll();
        foreach ($items as $item) {
            if($item['module_id'] != null){
                $this->data[$item['module_id']][$item['name']][$item['lang']] = $item;
                continue;
            }
            $this->data[$item['name']][$item['lang']] = $item;
        }
        parent::init();
    }
    
    /**
     * Возвращает значение парамтра
     * @param String $name имя параметра
     * @param String $moduleId имя модуля
     * @return String значение
     * @throws CException
     */
    public function param($name, $moduleId = null){
        $params = $this->getParams($name, $moduleId);
        if(empty($params)){
            throw new CException(Yii::t('error','Не найдены ключ {key}', array('{key}' => $name)));
        }
        return $params['value'] ? $params['value'] : $params['default'];
    }


    /**
     * Возвращает параметры по языку. В первую
     * очередь будет попытка вернуть конфиги с языком
     * выбранным в системе, в другом случае будет возвращен
     * с языковым парамтером * - для всех. Если ничего не найден
     * будет возворащен пустой массив
     * @param String $name
     * @param String $moduleId
     * @return Array
     * @throws CException
     */
    protected function getParams($name, $moduleId = null){
        $data = $moduleId ? $this->getDataModule($moduleId) : $this->data;
        if(!array_key_exists($name, $data)){
            throw new CException(Yii::t('error','Не найдены ключ {key}', array('{key}' => $name)));
        }
        foreach ($data[$name] as $param) {
            if ($param['lang'] == Yii::app()->language) {
                return $param;
            }
            if ($param['lang'] == '*') {
                return $param;
            }
        }
        return array();
    }
    /**
     * Возвращает данные о конфигурации по имени модуля
     * @param String $moduleId имя модуля
     * @return Array 
     * @throws CException
     */
    protected function getDataModule($moduleId){
        if(array_key_exists($moduleId, $this->data)){
            return $this->data[$moduleId];
        }
        throw new CException(Yii::t('error','Не найдены параметры для модуля {module}', array('{module}' => $moduleId)));
    }

    /**
     * Если установлен кэш, то запрос будет кеширован
     * на установленное количество милисекунд
     * @return type
     */
    protected function getDbConnection(){
        if ($this->cache)
            $db = Yii::app()->db->cache($this->cache, $this->dependency);
        else
            $db = Yii::app()->db;
 
        return $db;
    }
}
