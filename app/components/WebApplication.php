<?php

class WebApplication extends CWebApplication {
    protected function init() {
        parent::init();
        $this->modulesManager->start();
        
    }

}
